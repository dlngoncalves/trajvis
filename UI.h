//
//  UI.h
//  TrajVis
//
//  Created by Diego Gonçalves on 22/05/20.
//  Copyright © 2020 Diego Gonçalves. All rights reserved.
//

#ifndef UI_h
#define UI_h

#include <stdio.h>
#include "InputManager.h"
#include "Renderable.h"

class UI : public Renderable
{
    public:
        InputController *input;
        UI();
        ~UI();
        void Render();
        void initializeShader();
    
//    static bool picker = false;
//    static float minValueColor = -50;
//    static float maxValueColor = 50;
//    
//    static float minColor[3] = { 0.0f,0.0f,0.0f};
//    static float maxColor[3] = { 1.0f,1.0f,1.0f};
//    
//    static float minWidth = 1;
//    static float maxWidth = 5;
//    
//    static float minFilter = -50;
//    static float maxFilter = 50;
//    static int filter = 0;
//    static int selected = 0;
//    
//    static char minDate[11] = "2000-01-01";
//    static char maxDate[11] = "2000-01-01";
};

#endif /* UI_h */
