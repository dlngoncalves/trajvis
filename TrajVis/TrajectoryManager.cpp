//
//  Trajectory.cpp
//  TrajVis
//
//  Created by Diego Gonçalves on 15/08/19.
//  Copyright © 2019 Diego Gonçalves. All rights reserved.
//

#include "TrajectoryManager.h"
#include <sqlite3.h>
#include <glm/gtc/type_ptr.hpp>
#include "trajcomp/trajcomp.hpp"
#include "trajcomp/trajcomp_files.hpp"
#include "trajcomp/trajcomp_traclus.hpp"

#include<queue>
#include<set>
#include<unordered_map>
#include <algorithm>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/index/rtree.hpp>

int TrajectoryManager::scaleFactor = 10;

GLSLShader *dummyShader;// shader for use in the sqlite callback

TrajectoryManager::TrajectoryManager(GLSLShader &_shader)
{
    GeoPosition location;
    location = Map::GetLocation(true);
    //trajlist = LoadTrajectories(location,shader);
    shader = _shader;
    trajlist = FastLoadTrajectories(location, shader);
    //TODO - get average speed from db, set min and max for filter based on those values
    std::transform(trajlist.begin(), trajlist.end(), trajlist.begin(),
                   [](TrajParser &curTraj)
                   {
                       curTraj.SetAverageSpeed();
                       return curTraj;
                   });
    std::sort(trajlist.begin(),trajlist.end(),TrajParser::CompareTrajectories);
    SetupData();
}

TrajectoryManager::~TrajectoryManager()
{
    
}

void TrajectoryManager::Render()
{
//    for(auto curTraj : TrajList){
//        glUniform1f(trajectoryShader("averageSpeed"), curTraj.averageSpeed);
//        glUniform3fv(trajectoryShader("minMaxCurrent"), 1, glm::value_ptr(glm::vec3(minValueColor,maxValueColor,curTraj.segList[0].segWeather.temperature)));
//
//        //should put this stuff in an aux function
//        float value;
//        if(selected == 0)
//            value = curTraj.segList[0].segWeather.temperature;
//        if(selected == 1)
//            value = curTraj.averageSpeed;
//        if(selected == 2){
//            value = std::stof(curTraj.segList[0].timeStamp.substr(11,2));
//        }
//        if(selected == 3){
//            value = std::stoi(curTraj.segList[0].timeStamp.substr(0,4) +
//                              curTraj.segList[0].timeStamp.substr(5,2) +
//                              curTraj.segList[0].timeStamp.substr(8,2));
//
//        }
//
//        //gonna have to change those uniforms
//
//        glUniform3fv(trajectoryShader("minMaxCurrentFilter"), 1, //glm::value_ptr(glm::vec3(minFilter,maxFilter,curTraj.segList[0].segWeather.temperature)));
//                     //glm::value_ptr(glm::vec3(minFilter,maxFilter,curTraj.segList[0].segWeather.temperature)));
//                     glm::value_ptr(glm::vec3(minFilter,maxFilter,value)));
//
//        glUniform3fv(trajectoryShader("minColor"),1,minColor);
//        glUniform3fv(trajectoryShader("maxColor"),1,maxColor);
//        glUniform1i(trajectoryShader("time"),std::stoi(curTraj.segList[0].timeStamp.substr(5,2)));//this needs to be mapped to a buffer
//        glUniform1f(trajectoryShader("minWidth"),minWidth);
//        glUniform1f(trajectoryShader("maxWidth"),maxWidth);
//        glBindVertexArray (curTraj.vertexArrayObject);
//        glDrawArrays (GL_LINE_STRIP_ADJACENCY, 0,(int)curTraj.positions.size());
//        //glDrawArrays (GL_POINTS, 0, (int)curTraj.positions.size());
//    }
}

static int trajListCallback(void *Trajectories, int argc, char **argv, char **azColName)
{
    //the callback is called for each row, so no need to iterate
    std::vector<std::string> *trajList = (std::vector<std::string> *)Trajectories;
    
    //no need to check the column because we are only retrieving the trajectory name
    trajList->push_back(argv[0]);
    
    return 0;
}

static int fastTrajCallback(void *Trajectories, int argc, char **argv, char **azColName)
{
    std::vector<TrajParser> *trajList = (std::vector<TrajParser> *)Trajectories;
    TrajSeg auxSeg;
    TrajParser *traj;//{*dummyShader};
    long trajname;
    
    int columnCount = 0;
    
    for(int i=0; i<argc; i++){
        
        //printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        if(strcmp(azColName[i],"latitude") == 0){
            auxSeg.lat = atof(argv[i]);
        }
        if(strcmp(azColName[i],"longitude") == 0 ){
            auxSeg.lon = atof(argv[i]);
        }
        if(strcmp(azColName[i],"temperature") == 0 ){
            auxSeg.segWeather.temperature = atof(argv[i]);
        }
        if(strcmp(azColName[i],"datetime") == 0 ){
            auxSeg.timeStamp = argv[i];
        }
        if(strcmp(azColName[i],"elevation") == 0 ){
            auxSeg.elevation = atof(argv[i]);
        }
        if(strcmp(azColName[i],"instantspeed") == 0 ){
            auxSeg.speed = atof(argv[i]);
        }
        if(strcmp(azColName[i],"transport_mode") == 0 ){
            auxSeg.mode = TrajSeg::GetTransportMode(atoi(argv[i]));
        }
        if(strcmp(azColName[i],"trajectoryname") == 0 ){
            std::string trajnameaux = argv[i];
            trajname = std::stol(trajnameaux);
        }
        //TODO - Change query so we dont load trajectoryname and can remove this last check
        columnCount++;
        if(columnCount == 8){
            
            if(trajList->size() >0 && trajList->back().trajName == std::to_string(trajname)){
                if(trajList->back().trajName == std::to_string(trajname)){
                    traj = &trajList->back();
                }
                else{//is this ever gonna be called?
                    traj = new TrajParser(*dummyShader);
                    traj->trajName = std::to_string(trajname);
                }
            }
            else{
                traj = new TrajParser(*dummyShader);
                traj->trajName = std::to_string(trajname);
            }
            
            if(TrajParser::basePosition.x == 0 && TrajParser::basePosition.y == 0 && TrajParser::basePosition.z == 0){
                //think over here we should get the map lat/lon
                int x = Map::long2tilex(auxSeg.lon,Map::zoom);
                int y = Map::lat2tiley(auxSeg.lat, Map::zoom);
                
                //but im not sure I'm updating this
                //41.5f;
                //1.5f;
                //int x = Map::long2tilex(1.5f,Map::zoom);
                //int y = Map::lat2tiley(41.5f, Map::zoom);
                
                
                double returnedLat = Map::tiley2lat(y, Map::zoom);
                double returnedLon = Map::tilex2long(x, Map::zoom);
                TrajSeg base;
                base.lat = returnedLat;
                base.lon = returnedLon;
                TrajParser::basePosition = traj->latLonToMeters(returnedLat, returnedLon, Map::zoom);
                //TrajParser::basePosition = traj->convertLatLon(base,glm::vec3(0,auxSeg.elevation,0));
                
                int posX = Map::long2tilex(returnedLon, Map::zoom);
                int posY = Map::lat2tiley(returnedLat, Map::zoom);
                
                //TODO - think this should be static
                traj->SetScale(posX, posY, Map::zoom);
            }
            
            
            glm::vec3 auxPosition = traj->convertLatLon(auxSeg,TrajParser::basePosition);
            
            glm::vec3 curPoint = glm::vec3(1.0,0.0,0.0);
            curPoint = Weather::getWeatherColor(auxSeg.segWeather.temperature);
            
            traj->tempColors.push_back(curPoint);
            traj->positions.push_back(auxPosition);
            traj->segList.push_back(auxSeg);
            columnCount = 0;
            
            if(trajList->size() >0 && trajList->back().trajName == std::to_string(trajname)){
                
            }
            else{
                trajList->push_back(*traj);
                delete traj;
            }
        }
    }
    
    return 0;
}

std::vector<TrajParser> TrajectoryManager::LoadTrajectories(GeoPosition location,GLSLShader shader)
{
    std::vector<TrajParser> trajectories;
    //could this be leaking memory?
    sqlite3 *db;
    std::string query;
    char *zErrMsg = 0;
    
    //this minLat maxLat stuff will be based on the limits of the map, at the current zoom level, at the current map size
    float latDelta = 1.0;//this will be more complex later
    float lonDelta = 1.0;
    
    int rc = sqlite3_open("trajectories.db", &db);
    
    std::vector<glm::vec2> corners;//2 cornerss
    
    corners = Map::Corners(location);
    
    //this doesnt work if we are between zones
    std::string minLat = std::to_string(corners[1].x);//should map lat to y and lon to x
    std::string maxLat = std::to_string(corners[0].x);
    
    std::string minLon = std::to_string(corners[0].y);
    std::string maxLon = std::to_string(corners[1].y);
    
    TrajParser::basePosition = TrajParser::latLonToMeters(location.lat, location.lon, Map::zoom);
    
    int x = Map::long2tilex(location.lon,Map::zoom);
    int y = Map::lat2tiley(location.lat, Map::zoom);
    double returnedLat = Map::tiley2lat(y, Map::zoom);
    double returnedLon = Map::tilex2long(x, Map::zoom);
    
    int posX = Map::long2tilex(returnedLon, Map::zoom);
    int posY = Map::lat2tiley(returnedLat, Map::zoom);
    
    TrajParser::SetScale(posX, posY, Map::zoom);
    //TODO - change loading from database to get everything in one big query
    query = "SELECT DISTINCT TRAJECTORYNAME FROM TRAJSEG WHERE LATITUDE BETWEEN " + minLat + " AND " + maxLat + " AND LONGITUDE BETWEEN "
    + minLon + " AND " + maxLon + ";";
    
    std::vector<std::string> trajNames;
    //have to remember that sqlite exec will step over all the rows
    rc = sqlite3_exec(db, query.c_str(),trajListCallback, &trajNames, &zErrMsg);
 
    //look if we have a memory leak
    for(int i = 0; i < trajNames.size(); i++){
        TrajParser curTraj = TrajParser::LoadTrajectory(trajNames[i], shader);
        trajectories.push_back(curTraj);
    }
    
    return trajectories;
}

std::vector<TrajParser> TrajectoryManager::FastLoadTrajectories(GeoPosition location,GLSLShader shader)
{
    std::vector<TrajParser> trajectories;
    //could this be leaking memory?
    sqlite3 *db;
    std::string query;
    char *zErrMsg = 0;
    
    int rc = sqlite3_open("trajectories.db", &db);
    
    std::vector<glm::vec2> corners;//2 cornerss
    
    corners = Map::Corners(location);
    
    //this doesnt work if we are between zones
    std::string minLat = std::to_string(corners[1].x);//should map lat to y and lon to x
    std::string maxLat = std::to_string(corners[0].x);
    
    std::string minLon = std::to_string(corners[0].y);
    std::string maxLon = std::to_string(corners[1].y);
    
    TrajParser::basePosition = TrajParser::latLonToMeters(location.lat, location.lon, Map::zoom);
    
    int x = Map::long2tilex(location.lon,Map::zoom);
    int y = Map::lat2tiley(location.lat, Map::zoom);
    double returnedLat = Map::tiley2lat(y, Map::zoom);
    double returnedLon = Map::tilex2long(x, Map::zoom);
    
    int posX = Map::long2tilex(returnedLon, Map::zoom);
    int posY = Map::lat2tiley(returnedLat, Map::zoom);
    
    TrajParser::SetScale(posX, posY, Map::zoom);
    //new query - load the segments from the trajectories that are on screen in one query
    query = "SELECT * FROM TRAJSEG WHERE TRAJECTORYNAME IN (SELECT DISTINCT TRAJECTORYNAME FROM TRAJSEG WHERE LATITUDE BETWEEN "
    + minLat + " AND " + maxLat + " AND LONGITUDE BETWEEN " + minLon + " AND " + maxLon + " AND DATETIME < '2010-01-01T00:00:00Z') ORDER BY TRAJECTORYNAME;";
    
    dummyShader = &shader;
    rc = sqlite3_exec(db, query.c_str(),fastTrajCallback, &trajectories, &zErrMsg);
    
    return trajectories;
}

void TrajectoryManager::SetupData()
{
    //this is just a test
        std::vector<glm::vec3> positions;
        std::vector<glm::vec3> weather; //those will be generic, so probably should rename them
        std::vector<glm::vec2> speed;
        std::vector<float> temperature;
        std::vector<glm::vec2> datetime;
        std::vector<int> transport_mode;

        for(auto &curTraj : trajlist){
            positions.insert(positions.end(), curTraj.positions.begin(),curTraj.positions.end());
            weather.insert(weather.end(), curTraj.tempColors.begin(),curTraj.tempColors.end());
            //speed.insert(speed.end(), curTraj.speeds)
            //std::vector<float> speeds;
            //TODO - replace this for with stl algorithm ie. transform or for_each
            for(auto &seg : curTraj.segList){
                speed.push_back(glm::vec2(seg.speed,curTraj.averageSpeed));
                //this allows us to have changing temperature along a single trajectory
                temperature.push_back(seg.segWeather.temperature);
                
                std::string datestring = curTraj.segList[0].timeStamp.substr(0,4) +
                                               curTraj.segList[0].timeStamp.substr(5,2) +
                                               curTraj.segList[0].timeStamp.substr(8,2);
                float date = std::stof(datestring);
                float time = std::stof(curTraj.segList[0].timeStamp.substr(11,2));
                datetime.push_back(glm::vec2(date,time));
                transport_mode.push_back(TrajSeg::GetTransportModeValue(seg.mode));
            }
            
        }
        pointCount = positions.size();
        if(!glIsBuffer(this->vertexBufferObject))
            glGenBuffers(1, &this->vertexBufferObject);

        glBindBuffer(GL_ARRAY_BUFFER, this->vertexBufferObject);
        //not sure if should use glm data pointer or vector data pointer
        glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(glm::vec3), NULL, GL_DYNAMIC_DRAW);
        glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(glm::vec3), (float *)glm::value_ptr(positions.front()), GL_DYNAMIC_DRAW);

        //this is not being used
        if(!glIsBuffer(weatherBufferObject))
            glGenBuffers(1, &weatherBufferObject);

        glBindBuffer(GL_ARRAY_BUFFER, weatherBufferObject);
        glBufferData(GL_ARRAY_BUFFER, weather.size() * sizeof(glm::vec3), NULL, GL_DYNAMIC_DRAW);
        glBufferData(GL_ARRAY_BUFFER, weather.size() * sizeof(glm::vec3), (float *)glm::value_ptr(weather.front()), GL_DYNAMIC_DRAW);

        //weird name here
        if(!glIsBuffer(speedArrayObject))
            glGenBuffers(1, &speedArrayObject);

        glBindBuffer(GL_ARRAY_BUFFER, speedArrayObject);
        glBufferData(GL_ARRAY_BUFFER, speed.size() * sizeof(glm::vec2), NULL, GL_DYNAMIC_DRAW);
        glBufferData(GL_ARRAY_BUFFER, speed.size() * sizeof(glm::vec2), speed.data(), GL_DYNAMIC_DRAW);

        if(!glIsBuffer(temperatureBufferObject))
            glGenBuffers(1, &temperatureBufferObject);
    
        glBindBuffer(GL_ARRAY_BUFFER, temperatureBufferObject);
        glBufferData(GL_ARRAY_BUFFER, temperature.size() * sizeof(float), NULL, GL_DYNAMIC_DRAW);
        glBufferData(GL_ARRAY_BUFFER, temperature.size() * sizeof(float), temperature.data(), GL_DYNAMIC_DRAW);
    
        if(!glIsBuffer(datetimeBufferObject))
            glGenBuffers(1, &datetimeBufferObject);
    
        glBindBuffer(GL_ARRAY_BUFFER, datetimeBufferObject);
        glBufferData(GL_ARRAY_BUFFER, datetime.size() * sizeof(glm::vec2), NULL, GL_DYNAMIC_DRAW);
        glBufferData(GL_ARRAY_BUFFER, datetime.size() * sizeof(glm::vec2), (float *)glm::value_ptr(datetime.front()), GL_DYNAMIC_DRAW);
    
        if(!glIsBuffer(transportBufferObject))
            glGenBuffers(1, &transportBufferObject);
    
        glBindBuffer(GL_ARRAY_BUFFER, transportBufferObject);
        glBufferData(GL_ARRAY_BUFFER, transport_mode.size() * sizeof(int), NULL, GL_DYNAMIC_DRAW);
        glBufferData(GL_ARRAY_BUFFER, transport_mode.size() * sizeof(int), transport_mode.data(), GL_DYNAMIC_DRAW);
    
        if(!glIsVertexArray(vertexArrayObject))
            glGenVertexArrays(1, &vertexArrayObject);

        glBindVertexArray(vertexArrayObject);

        glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0 , 0);

        glBindBuffer(GL_ARRAY_BUFFER, weatherBufferObject);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, speedArrayObject);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, temperatureBufferObject);
        glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, 0, 0);
    
        glBindBuffer(GL_ARRAY_BUFFER, datetimeBufferObject);
        glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, 0, 0);
    
        glBindBuffer(GL_ARRAY_BUFFER, transportBufferObject);
        glVertexAttribIPointer(5,1,GL_INT,0,0);
    
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);
        glEnableVertexAttribArray(3);
        glEnableVertexAttribArray(4);
        glEnableVertexAttribArray(5);
    
        //we should probably have a default setupcluster data to load why default values and also use to clear the clustering
        //so we can use some value that wont be mistaken as a cluster, like -5
        if(isClustered){
            SetupClusterData();
        }
        else{
            SetupClearClusterData();
        }
}

void TrajectoryManager::SetupClusterData()
{
    std::vector<float> clusters;
    
    //but what happens to trajectories that were not loaded when we performed the clustering?
    for(auto &curTraj : trajlist){
//        for(auto &seg : curTraj.segList){
//            clusters.push_back((seg.segWeather.temperature));
//        }
        //this was causing a bug because now we actually fill
        if(curTraj.clusterList.size() >0){
            clusters.insert(clusters.end(), curTraj.clusterList.begin(),curTraj.clusterList.end());
        }
        else{
            //gonna use a for, but transform might work better
            for(auto &seg : curTraj.segList){
                clusters.push_back(-5);
            }
        }
    }

    //think using std::transform is faster
    //ok I think this is wrong, we are just using the second for to get the number of items in the list
//    for (auto &curTraj : trajlist){
//        for(auto &curSeg : curTraj.segList){
//            clusters.push_back(curTraj.cluster);//should have a better way of doing this
//        }//remains to be seen whether we need to have this data for EVERY point, or if cluster should be an uniform
//    }
    
    glBindVertexArray(vertexArrayObject);
    
    if(!glIsBuffer(this->clusterBufferObject))
        glGenBuffers(1, &this->clusterBufferObject);
    
    
    glBindBuffer(GL_ARRAY_BUFFER, this->clusterBufferObject);
    glBufferData(GL_ARRAY_BUFFER, clusters.size() * sizeof(float), NULL, GL_DYNAMIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, clusters.size() * sizeof(float), clusters.data(), GL_DYNAMIC_DRAW);
    
    
    glBindBuffer(GL_ARRAY_BUFFER, this->clusterBufferObject);
    glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(6);
}

void TrajectoryManager::SetupClearClusterData()
{
    std::vector<float> clusters;
    
    //but what happens to trajectories that were not loaded when we performed the clustering?
    //TODO - replace with stl algorithm
    for(auto &curTraj : trajlist){
        for(auto &seg : curTraj.segList){
            clusters.push_back(-5);
        }
        //also clear the cluster data from before, in case it exists
        curTraj.clusterList.clear();
    }
    
    //think using std::transform is faster
    //ok I think this is wrong, we are just using the second for to get the number of items in the list
    //    for (auto &curTraj : trajlist){
    //        for(auto &curSeg : curTraj.segList){
    //            clusters.push_back(curTraj.cluster);//should have a better way of doing this
    //        }//remains to be seen whether we need to have this data for EVERY point, or if cluster should be an uniform
    //    }
    
    glBindVertexArray(vertexArrayObject);
    
    if(!glIsBuffer(this->clusterBufferObject))
        glGenBuffers(1, &this->clusterBufferObject);
    
    
    glBindBuffer(GL_ARRAY_BUFFER, this->clusterBufferObject);
    glBufferData(GL_ARRAY_BUFFER, clusters.size() * sizeof(float), NULL, GL_DYNAMIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, clusters.size() * sizeof(float), clusters.data(), GL_DYNAMIC_DRAW);
    
    
    glBindBuffer(GL_ARRAY_BUFFER, this->clusterBufferObject);
    glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(6);
}

void TrajectoryManager::initializeShader()
{
    
}

std::vector<std::vector<std::vector<double>>> TrajectoryManager::PrepareForClustering(std::map<int,int> &indexMapping)
{
    //clustering is done on data formated as "xpos ypos index"
    //from csv files
    //need to convert trajparser vector to this
    using namespace trajcomp;
    using namespace trajcomp::tools;
    
    std::vector<std::vector<std::vector<double>>> db;
    
    //THIS IS NOT BEING USED HERE
//    GeoPosition location = Map::GetLocation(true);
//    std::vector<glm::vec2> corners;//2 cornerss
//
//    corners = Map::Corners(location);
//
//    //this doesnt work if we are between zones
//    std::string minLat = std::to_string(corners[1].x);//should map lat to y and lon to x
//    std::string maxLat = std::to_string(corners[0].x);
//
//    std::string minLon = std::to_string(corners[0].y);
//    std::string maxLon = std::to_string(corners[1].y);
    
    //gonna change here from using the scaled and referenced positions to the absolute meter value
    //this is to ensure constant results at different zoom levels (I hope)
    for(int i = 0; i < trajlist.size(); i++){
        std::vector<std::vector<double>> traj;
        
        std::transform(trajlist[i].segList.begin(),trajlist[i].segList.end(),std::back_inserter(traj),
                       [&](auto const& trajSeg){                           
                           glm::vec3 position = trajlist[i].latLonToMeters(trajSeg.lat, trajSeg.lon, 0);
                        return std::vector<double>{position.x/TrajectoryManager::scaleFactor,position.z/TrajectoryManager::scaleFactor};});
        indexMapping.insert(std::pair(i,i));//indexmapping added to mantain consistency with other mode
//        std::transform(trajlist[i].positions.begin(),trajlist[i].positions.end(),std::back_inserter(traj),
//                       [](auto const& element){return std::vector<double>{element.x,element.z};});
 
                       //[](auto const& tuple){return std::get<1>(tuple);});
        //for(int j = 0; j < trajlist[i].segList.size(); j++){
            //traj.push_back({trajlist[i].positions[j].x,trajlist[i].positions[j].z});
        //}
        
        db.push_back(traj);
        traj.clear();
    }
    
    return db;
    
}

bool TrajectoryManager::FilterCurrent(float min, float max, float current)
{
    if(min > max)//was ==, changed to allow filtering by only one value
        return false;
    
    if(current < min)
        return true;
    if(current > max)
        return true;
    
    return false;
}

bool TrajectoryManager::FilterTransport(int filterValues,int current)
{
    bool flag = bool(filterValues & (1<< current));
    
    return flag;
}

bool TrajectoryManager::FilterOut(float (*minMaxCurrentFilters)[6][2],unsigned int flags, unsigned int modeFlags,TrajParser &trajectory, TrajSeg &segment)
{
    //just copying the filtering from the shader fuck it
    
    bool returnValue = false;
    
    if(bool(flags & 1u)){
        returnValue = FilterCurrent((*minMaxCurrentFilters)[0][0],(*minMaxCurrentFilters)[0][1],segment.segWeather.temperature);
        if(returnValue)
            return true;
    }
    if(bool((flags & 2u)>>1)){
        returnValue = FilterCurrent((*minMaxCurrentFilters)[1][0],(*minMaxCurrentFilters)[1][1],trajectory.averageSpeed);
        if(returnValue)
            return true;
    }
    if(bool((flags & 4u)>>2)){
        float time = std::stof(trajectory.segList[0].timeStamp.substr(11,2));
        returnValue = FilterCurrent((*minMaxCurrentFilters)[2][0],(*minMaxCurrentFilters)[2][1],time);
        if(returnValue)
            return true;
    }
    if(bool((flags & 8u)>>3)){
        std::string datestring = trajectory.segList[0].timeStamp.substr(0,4) +trajectory.segList[0].timeStamp.substr(5,2) + trajectory.segList[0].timeStamp.substr(8,2);
        float date = std::stof(datestring);
        returnValue = FilterCurrent((*minMaxCurrentFilters)[3][0],(*minMaxCurrentFilters)[3][1],date);
        if(returnValue)
            return true;
    }
    if(bool((flags & 16u)>>4)){
        //ignore this because we are not clustering based on previous clusters
        //returnValue = FilterCurrent(*minMaxCurrentFilters[4][0],*minMaxCurrentFilters[4][1],cluster_g[1]);
        //if(returnValue)
        //    return true;
    }
    if(bool((flags & 32u)>>5)){
        returnValue = !FilterTransport((*minMaxCurrentFilters)[5][0],int(TrajSeg::GetTransportModeValue(segment.mode)));
        if(returnValue)
            return true;
    }
    
    return returnValue;
}

//Added indexMapping to remember the indices of the original array
std::vector<std::vector<std::vector<double>>> TrajectoryManager::PrepareForClustering(float (*minMaxCurrentFilters)[6][2],unsigned int flags,unsigned int modeFlags, std::map<int,int> &indexMapping)
{
    //are those being used?
    using namespace trajcomp;
    using namespace trajcomp::tools;

    std::vector<std::vector<std::vector<double>>> db;
    
    //get the bounderies of the current map
    std::vector<glm::vec2> corners;//2 cornerss
    GeoPosition location{Map::lat,Map::lon};//dont need to assign the string and vec2 versions, not used here and have default values
    corners = Map::Corners(location);
    
    //this doesnt work if we are between zones
    std::string minLat = std::to_string(corners[1].x);//should map lat to y and lon to x
    std::string maxLat = std::to_string(corners[0].x);
    
    std::string minLon = std::to_string(corners[0].y);
    std::string maxLon = std::to_string(corners[1].y);
    
    //Are we using db or vector? Think vector will be faster, will see
    //ok, now thinking maybe db is not such a bad idea...
    std::vector<std::vector<double>> traj;
    
    //copying the filtering here
    int currentPosition = 0;
    //keep the difference between the original indices and the ones in the array used for clustering
    int currentTraj = 0;
    int currentFilterTraj = 0;
    
    for(auto &curTraj : trajlist){
        for(auto &curSeg : curTraj.segList){
            if(curSeg.lat > corners[1].x && curSeg.lat < corners[0].x && curSeg.lon > corners[0].y && curSeg.lon < corners[1].y){
                if(!FilterOut(minMaxCurrentFilters, flags, modeFlags, curTraj, curSeg)){
                    double x = (double)curTraj.positions[currentPosition].x;
                    double y = (double)curTraj.positions[currentPosition].z;
                    x /= TrajParser::relativeScale;
                    y /= TrajParser::relativeScale;
                    x += TrajParser::basePosition.x;
                    y += TrajParser::basePosition.z;
                    x /= TrajectoryManager::scaleFactor; //different scale
                    y /= TrajectoryManager::scaleFactor;
                    std::vector<double> pos{curTraj.positions[currentPosition].x,curTraj.positions[currentPosition].z};//just for testing
                    traj.push_back(std::vector<double>{x,y});
                }
            }
            currentPosition++;
        }
        if(traj.size() > 0){
            indexMapping.insert(std::pair(currentFilterTraj,currentTraj));
            db.push_back(traj);
            currentFilterTraj++;
        }
        currentTraj++;
        traj.clear();
        currentPosition = 0;
    }
    return db;
}

std::map<int,std::vector<std::vector<double>>> TrajectoryManager::ResetRepresentPositions(std::map<int,std::vector<std::vector<double>>> &represTraj)
{
    for(auto &curCluster : represTraj){
        std::transform(curCluster.second.begin(),curCluster.second.end(),curCluster.second.begin(),
                       [](std::vector<double> &curPosition)
                       {//we know this internal vector has only two positions
                           double x = (curPosition[0] * TrajectoryManager::scaleFactor);
                           double y = (curPosition[1] * TrajectoryManager::scaleFactor);
                           //think this order is incorrect
                           x -= TrajParser::basePosition.x;
                           y -= TrajParser::basePosition.z;//z is y
                           x *= TrajParser::relativeScale;
                           y *= TrajParser::relativeScale;
                           return std::vector<double>{x,y};
                       });
    }
    
    return represTraj;
}

std::map<int,std::vector<std::vector<double>>> TrajectoryManager::ClusterTrajectories(float (*minMaxCurrentFilters)[6][2],int epsilon,int minLines,int scale,unsigned int flags,unsigned int modeFlags)
{
    using namespace trajcomp;
    
    TrajectoryManager::scaleFactor = scale;
    
    std::map<int,int> indexMapping;
    
    std::vector<std::vector<std::vector<double>>> db;
    if(minMaxCurrentFilters == nullptr){
        db = PrepareForClustering(indexMapping);
    }else{
        db = PrepareForClustering(minMaxCurrentFilters,flags,modeFlags,indexMapping);
    }
    
    std::vector<std::map<int,int>> clusterCount(trajlist.size());
    
    //double eps = 30 * TrajParser::relativeScale;//unsure if using relative scale is good here BUT i think the values are already scaled, so
    //now that we are using the unscaled values, shouldnt need to multiply by scale
    double eps = static_cast<double>(epsilon);//dont know why this has to be double
    auto clusteredTrajectories = traclus(db, eps, minLines, traclus_partition<std::vector<std::vector<double>>,std::vector<int>>, calc_dist<std::vector<double>>);
//    std::ofstream clusterfile("clustering.csv"); //leaving this here for debugging purposes
    
    //when the rep. traj are created they are in the selected scale and meters
    //data structure std::map<int,std::vector<point>>
    auto representative = representative_trajectory(clusteredTrajectories,minLines);
    //this will create a copy I think
    representative = ResetRepresentPositions(representative);
    auto iteratorClusters = clusteredTrajectories.begin();
    
    //problem is that here, even if we have removed the segments that are outside the screen, the filling of the clusterlist
    //per trajectory makes sure that missing elements in the middle are not forgotten.
    //And so far we have no way of knowing if a point is missing because its not a partition point or because it was excluded
    //possible solutions - keep a copy of the original partition
    //instead of deleting the segments, mark them with a different magic number like -4 and ignore the segments in the clustering
    
    for (auto curTraj:clusteredTrajectories){//kind of a bad name, not actually trajectories here
        //clusterfile << curTraj << std::endl;
        //here, if I'm scanninng every clustered line segment, I can add the more precise cluster information to the trajectories...
        //but I cant see a FAST way of doing this...
        //probably an easier way of doing this with iterators
        int originalIndex = indexMapping[(*iteratorClusters).trajectory_index];
        
        if(std::next(iteratorClusters) != clusteredTrajectories.end()){
            for(int i = (*iteratorClusters).trajectory_part; i < (*std::next(iteratorClusters)).trajectory_part; i++){
                trajlist[originalIndex].clusterList.push_back((*iteratorClusters).cluster);
            }
        }
        
        if((*iteratorClusters).trajectory_index != (*std::next(iteratorClusters)).trajectory_index){
            for(int j = (*iteratorClusters).trajectory_part; j < trajlist[originalIndex].segList.size(); j++){
                trajlist[originalIndex].clusterList.push_back((*iteratorClusters).cluster);
            }
        }
        
        
        iteratorClusters++;
        
        
        clusterCount[originalIndex][curTraj.cluster] += 1;
    }
    //if I have another container telling me the points in which each trajectory splits into the line segments...
    //this would have to change somewhat
    //
    
    for(int i = 0; i < indexMapping.size(); i++){
        int maxCluster = 0;
        int originalIndex = indexMapping[i];
        trajlist[originalIndex].cluster = -1;
        for(auto cluster : clusterCount[originalIndex]){
            if(cluster.second > maxCluster){//} && cluster.first >=0){
                maxCluster = cluster.second;
                trajlist[originalIndex].cluster = cluster.first;
                
            }
        }
    }
    
    std::set<int> clusters;

    for(auto &cluster : clusterCount){
        std::transform(begin(cluster),end(cluster),std::inserter(clusters,clusters.begin()),
                       [](const auto &pair){return pair.first;});
    }
    
    //one problem with this is that we are using the number of the clusters before they get deleted,
    //so a segment might have a larger cluster value than the number here. gonna use the last value for now.
    
    //rbegin is a iterator to the last one, not the size
    //not sure why using this here but will keep it
    numberOfClusters = *clusters.rbegin()+1;//first cluster is zero

    //gonna remove this count later
    //this is the dumbest way to debug
    std::vector<int> totalclusters;
    
    
    for(auto &curTraj : trajlist){
        totalclusters.insert(totalclusters.end(), curTraj.clusterList.begin(),curTraj.clusterList.end());
    }

    std::map<int,int> appereances;
    
    for(auto &curCluster : clusters){
        int curAmount = std::count(totalclusters.begin(), totalclusters.end(), curCluster);
        
        appereances.insert(std::make_pair(curCluster, curAmount));
    }

    std::cout << "Number of Clusters : " << clusters.size() << "\n"; //numberOfClusters is in fact, NOT the number of clusters
    
    
    
    isClustered = true;
    canClusterAgain = false;
    //Dont think this should be called here
    //TODO - really need to move this
    SetupClusterData();
    
    return representative;
//    clusterfile.close();
}

void TrajectoryManager::CreateRTree()
{
    
    
//    for(int i = 0; i < 200/*trajlist.size()*/; i++){
//        std::vector<std::vector<double>> traj;
//        for(int j = 0; j < trajlist[i].segList.size(); j++){
//            traj.push_back({trajlist[i].positions[j].x,trajlist[i].positions[j].z});
//        }
//        db.push_back(traj);
//        traj.clear();
//    }
    
    //should we use the r-tree to store the original points?
    //or rather would make more sense to FIRST generate the line segments, then generate the box from those segments
    //in addition, the line segments will store from which trajectory they belong
    
    namespace bg = boost::geometry;
    namespace bgi = boost::geometry::index;
    
    typedef bg::model::point<float, 2, bg::cs::cartesian> point;
    typedef bg::model::box<point> box;
    typedef std::pair<box, unsigned> value;
    
    // create the rtree using default constructor
    bgi::rtree< value, bgi::quadratic<16> > rtree;
    
    for(int i =0; i < trajlist.size(); i++){
        for(int j = 0; j < trajlist[i].positions.size(); j++){
            //trajlist[i].positions[j]
            //trajlist[i].positions[j].x,trajlist[i].positions[j].z
            //box b(point()
            //bg::envelope(box, box);//
            bg::model::box<point> box;
            point curPoint(trajlist[i].positions[j].x,trajlist[i].positions[j].z);
            bg::envelope(curPoint, box);
            rtree.insert(std::make_pair(box,i));
            //curPoint.
            //bg::model::po
        }
        //read well known text
        //bg::read_wkt(<#const std::string &wkt#>, <#Geometry &geometry#>)
    }
    
    std::cout << "Done building r-tree" << " number of elements " << rtree.size() << "\n";
    
    //this lets us search for a specific value in the tree
    point curPoint(trajlist[0].positions[0].x,trajlist[0].positions[0].z);
    box myBox;
    bg::envelope(curPoint, myBox);
    value valueToSearch(myBox,0);
    size_t count = rtree.count(valueToSearch);
    
    
    //here we try to find a number of points inside a query box
    box query_box(point(300, -100), point(400, 100));
    std::vector<value> result_s;
    rtree.query(bgi::intersects(query_box), std::back_inserter(result_s));
    
    //just testing knn query, doesnt make a lot of sense to use a query box
    std::vector<value> result_nearest;
    rtree.query(bgi::nearest(query_box, 5), std::back_inserter(result_nearest));
    
    std::cout << bg::wkt<box>(query_box) << "\n";
}

//This function assumes we already have the trajectories loaded in the database, and updates the trajseg table with the transportation mode information
//this only works as is because we only have data from one user
//TODO - if(when) more users are added to the database, need to take that into account
void TrajectoryManager::ParseGeolifeLabels(std::string file)
{
    std::ifstream labelFile;
    std::string line;
    labelFile.open(file,std::ifstream::in);
    sqlite3 *db;
    char *zErrMsg = 0;
    std::map<std::string,int> transport_modes;
    
    if(labelFile.is_open()){
        transport_modes = TrajectoryManager::GetTransportTypes();
        int rc = sqlite3_open("trajectories.db", &db);
        std::string user;//added user to file
        while(getline(labelFile, line)){

            if(line[0]=='S')
                continue;
            if(line[0]=='U'){
                user = line.substr(2);
                if(user.back()=='\r'){//somehow some files added return char at end of line, others didnt
                    user.pop_back();
                }
                continue;
            }
            int count = 0;
            int curMode = 0;
            std::istringstream labelLine(line);
            std::string token;
            //TODO - remove raw sql statements from code
            std::string query = "UPDATE TRAJSEG SET TRANSPORT_MODE = ";
            std::string start;
            std::string end;
            
            if(user == "")//not happy about this if
                break;
            
            while(std::getline(labelLine, token, '\t')){
                switch(count){//use tokens to assemble query
                    case 0://start date and time
                        start = token;
                        std::replace(start.begin(),start.end(),'/','-');
                        std::replace(start.begin(),start.end(),' ','T');
                        start += 'Z';
                        break;
                    case 1://end date and time
                        end = token;
                        std::replace(end.begin(),end.end(),'/','-');
                        std::replace(end.begin(),end.end(),' ','T');
                        end += 'Z';
                        break;
                    case 2://transport mode
                        token.pop_back();//last char is endln;
                        curMode = transport_modes[token];
                        break;
                }
                count++;
            }

            query += std::to_string(curMode);
            query += " WHERE DATETIME BETWEEN '" + start + "' AND '" + end + "' AND " + "USER = '" + user  + "';";
            
            rc = sqlite3_exec(db, query.c_str(),NULL, NULL, &zErrMsg);
        }
        sqlite3_close_v2(db);
    }
}

static int transportCallback(void *Modes, int argc, char **argv, char **azColName)
{
    std::map<std::string,int> *db_modes = (std::map<std::string,int> *)Modes;
    db_modes->insert(std::make_pair(argv[1], atoi(argv[0])));
    return 0;
}

std::map<std::string,int> TrajectoryManager::GetTransportTypes()
{
    sqlite3 *db;
    std::string query;
    char *zErrMsg = 0;
    
    std::map<std::string,int> modes;//we are reversing from the db- here the id is the text description, to make searching easier
    int rc = sqlite3_open("trajectories.db", &db);
    
    query = "SELECT * FROM TRANSPORTATION_MODE";
    rc = sqlite3_exec(db, query.c_str(),transportCallback, &modes, &zErrMsg);
    sqlite3_close_v2(db);
    return modes;
}
