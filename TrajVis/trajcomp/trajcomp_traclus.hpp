#ifndef TRACLUS_HPP_INC
#define TRACLUS_HPP_INC

#include<queue>
#include<set>
#include<unordered_map>
#include <algorithm>
#include <chrono>
#include <numeric>
#include <iterator>

#include "trajcomp.hpp"

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/index/rtree.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ClusterManager.hpp"
//#include <boost/geometry/geometries/adapted/boost_tuple.hpp>

//BOOST_GEOMETRY_REGISTER_BOOST_TUPLE_CS(cs::cartesian)

#define UNCLASSIFIED -1
#define NOISE -2
#define REMOVED -3
#define IGNORE -4

namespace trajcomp{
	
    namespace bg = boost::geometry;
    namespace bgi = boost::geometry::index;
    
    typedef bg::model::point<double, 2, bg::cs::cartesian> point_boost;
    typedef bg::model::segment<point_boost> seg;
    typedef bg::model::box<point_boost> box;

    typedef std::tuple<seg, size_t,int> value;
    
    bgi::rtree< value, bgi::rstar<30> > rtree;
    //bgi::rtree< value, bgi::rstar<16> > rtree;
    
template<typename point>
class tLineSegment
{
	public:
	point s;  /// the start of the segment
	point e;  /// the end of the segment
	size_t trajectory_index;  /// the index of the trajectory, where this segment has been extracted from
	int cluster; /// the cluster ID of the segment
    size_t trajectory_part;//fro
    //could add the part of the trajectory here
    
	/// The default segment is created as UNCLASSIFIED
	tLineSegment(point &_s, point &_e, size_t index,size_t part):s(_s),e(_e),trajectory_index(index),cluster(UNCLASSIFIED),trajectory_part(part)
	{
	}
	
	friend std::ostream& operator<<(std::ostream& os, const tLineSegment<point>& dt)
	{
		os << trajcomp::tools::make_string(dt.s) << " " << trajcomp::tools::make_string(dt.e) << " " << dt.trajectory_index << " " << dt.cluster;
		return os;
	}
	
};

enum class Side{Left,Right};
    
template<typename point>
class directedPoint{
    public:
    point position;
    Side side;
    //do we really need this? because we only care in relation to each cluster
    //so ok, we need this, but keeping track of it is easier, just add when filling the point vector
    //since we add two by two, we will have both ends of the segment
    //problem is, we save the points, and then want to add the original segment to the list of intersections
    //we then dont know the other end, which is needed to calculate the height position of the intersection
    int segID;//we only care about the original segment, not trajectory
    
    directedPoint(point &_position):position(_position),side(Side::Left){}//default left I guess
    
    directedPoint(point &_position,Side pointSide):position(_position),side(pointSide){}
    
    directedPoint(point &_position,Side pointSide, int _segID):position(_position),side(pointSide),segID(_segID){}
    
    directedPoint() = default;

};

static trajcomp::default_element_distance<std::vector<double>> d_euc;
//~ #define trajcomp::default_element_distance<std::vector<double>> d_euc

/*Projection point calculation*/
template<typename sample_type>
sample_type projection_point(sample_type &u,sample_type &v, sample_type &p) 
  {
	  
	  // the length of the segment
	  trajcomp::default_element_distance_squared<sample_type> d2;
	  
	  double l = d2(u,v);

	   if (fabs(l) < 1E-12)
	   		return u;
	   
		double t = 0;
		for (size_t i=0; i <  u.size(); i++)
		   t += ((v[i]-u[i])*(p[i]-u[i]));
		t/=l;
		
		sample_type proj;
		for (size_t i=0; i <  u.size(); i++)
		   //proj.push_back(v[i]+t*(u[i]-v[i]));
		   proj.push_back(u[i]+t*(v[i]-u[i]));
		   
		return proj;
  }



template<typename point>
double perpen_dist(point &si,point &ei,point &sj,point &ej)
{
	point ps = projection_point(si,ei,sj);
	point pe = projection_point(si,ei,ej);
	
	double dl1 = d_euc(sj,ps);
	double dl2 = d_euc(ej,pe);
	
	if (fabs(dl1+dl2) < 0.0001) 
	    return 0;
	
	return (dl1*dl1 + dl2*dl2)/(dl1 + dl2); //@TODO: use d2 
};

template<typename point>
double angle_dist(point &si,point &ei,point &sj,point &ej)
{
	double alpha1 = atan2(ei[1] - si[1],ei[0] - si[0]);
	double alpha2 = atan2(ej[1] - sj[1],ej[0] - sj[0]);
	
	double l = d_euc(sj,ej);
	
	return l * fabs(sin(alpha2-alpha1));
}

template<typename point>
double par_dist(point &si,point &ei,point &sj,point &ej)
{
	point ps = projection_point(si,ei,sj);
	point pe = projection_point(si,ei,ej);
	
	double l1 = std::min(d_euc(ps,si),d_euc(ps,ei));
	double l2 = std::min(d_euc(pe,si),d_euc(pe,ei));
	
	return std::min(l1,l2);
}


/*Total distance, which is a weighted sum of above*/
template<typename point>
double total_dist(point &si,point &ei,point &sj,point &ej, 
		double w_perpendicular=0.33, double w_parallel=0.33, double w_angle=0.33)
{
	double td =   w_perpendicular * perpen_dist(si,ei,sj,ej)
			    + w_parallel      *    par_dist(si,ei,sj,ej)
				+ w_angle         *  angle_dist(si,ei,sj,ej);
	return td;
}



template<typename iteratortype>
double MDL_PAR(iteratortype st, iteratortype en)
{
	double d1 = d_euc(*st, *en);
		
	double PER_DIST=0, ANG_DIST=0;
	iteratortype it = st;
	iteratortype it2 = it;
	it2 ++;
	
	while (true)
	{
		double d2 = d_euc(*it, *it2);
		if (d1 >= d2)
		{
			PER_DIST += perpen_dist(*st,*en,*it,*it2);
			ANG_DIST += angle_dist(*st,*en,*it,*it2);
		}else{
			PER_DIST += perpen_dist(*it,*it2,*st,*en);
			ANG_DIST += angle_dist(*it,*it2,*st,*en);
		}
		
		if (it2 == en) 		
			break;
		it ++;
		it2 ++;
	}
	
	
	// information calculation
	double LH = log2(d1);
	double LDH = log2(PER_DIST) + log2(ANG_DIST);
	
	return LH + LDH;
}

// Note: this is never (and must not be) called for an empty trajectory.
    
//what if here we were to save the transition points?
//ok i think we have something
template<typename ttraj, typename partindex>
ttraj traclus_partition (ttraj &A, partindex &indices)
{
	ttraj CP;
	
	CP.push_back(A[0]);  
    indices.push_back(0);
    
    //std::vector<int> transitionPoints;//just for testing
    //transitionPoints.push_back(0);
    
	typename ttraj::iterator it,it2,it2_old;
	it =  A.begin();
	it2_old = it2 = it;
	it2 ++;
	
	while (it2 != A.end())
	{
		double cost = MDL_PAR(it, it2);
		double cost2 = log2(d_euc(*it,*it2));//article suggests adding a constant here might improve results
		//cout << cost << "#" << cost2 << endl;
		//cout << "it2:" << make_string(*it2) << endl;
		if (cost > cost2 && !(fabs(cost) < 0.0001)) // right side: skip over equal points
		{
            indices.push_back(it2 - A.begin());
			//cout << "adding" << make_string(*it2_old);
			CP.push_back(*it2_old);
			it = it2_old;			
			it2++;
			it2_old ++;
            
		}else{
			it2 ++;
			it2_old ++;
		}
	}
	CP.push_back(A.back());
	return CP;
}


//double calc_dist(point &li1,point &li2,point &lj1,point &lj2, size_t key = 0)
template<typename point>
double calc_dist(std::vector<tLineSegment<point>> &L,size_t i, size_t j)
{
/*	static std::unordered_map<size_t,double> cache; //@REMARK: magic statics are not thread-safe at Microsoft
	std::unordered_map<size_t,double>::const_iterator it;
	it = cache.find(key);
	if (it != cache.end())
	{ 
		return (*it).second;
	}*/
	//@TODO: refactor
	#define li1 L[i].s
	#define li2 L[i].e
	#define lj1 L[j].s
	#define lj2 L[j].e
    //maybe its easier to change this distance calculation function
    
	double len_i = d_euc(li1,li2);
	double len_j = d_euc(lj1,lj2);
	// total_dist: larger segement first.
	double td = 
	   (len_i >= len_j)?total_dist(li1,li2,lj1,lj2):total_dist(lj1,lj2,li1,li2);

	/*if (cache.size() < 256E3/4)
		cache.insert(std::pair<size_t, double>(key, td));*/
	return td;
}

//template<typename point>
//this function is not the most beautiful
double calc_dist2( const value &first, const value &second)
{
//can maybe keep these defines
//    #define li1 L[i].s
//    #define li2 L[i].e
//    #define lj1 L[j].s
//    #define lj2 L[j].e
        //maybe its easier to change this distance calculation function
    seg firstSeg = std::get<0>(first);
    seg secondSeg = std::get<0>(second);
    
    std::vector<double> firstStart{bg::get<0, 0>(firstSeg),bg::get<0, 1>(firstSeg)};
    std::vector<double> firstEnd{bg::get<1, 0>(firstSeg),bg::get<1, 1>(firstSeg)};

    std::vector<double> secondStart{bg::get<0, 0>(secondSeg),bg::get<0, 1>(secondSeg)};
    std::vector<double> secondEnd{bg::get<1, 0>(secondSeg),bg::get<1, 1>(secondSeg)};
    
    
    double len_i = d_euc(firstStart,firstEnd);
    double len_j = d_euc(secondStart,secondEnd);
        // total_dist: larger segement first.
    //double td = (len_i >= len_j)?total_dist(li1,li2,lj1,lj2):total_dist(lj1,lj2,li1,li2);
    double td = (len_i >= len_j)?total_dist(firstStart,firstEnd,secondStart,secondEnd):total_dist(secondStart,secondEnd,firstStart,firstEnd);
        
        /*if (cache.size() < 256E3/4)
         cache.insert(std::pair<size_t, double>(key, td));*/
    return td;
    
    //return 0;
}
    
template<typename segment>
double secondary_dist(segment first, segment second)
{
//    double len_i =
//    return 0;
}

/* Compute neighborhood containment as vector<bool>
 * 
 * _d can be globally cached functional class and
 *  is called with indizes instead of objects to 
 *  facilitate caching
 * */
 
 
/*template<typename segmentcollection, typename point, typename index_distance_getter>
std::vector<bool> compute_Ne_containement(segmentcollection &D, 
					size_t index, double epsilon, index_distance_getter &_d)
{
	std::vector<bool> ret(D.size());
	
	for(size_t i = 0; i < D.size(); i++)
	{
		double d=0;
		d = _d(i,index);
		if (d <= epsilon)
			ret[index] = true;
	}
	return ret;	
};*/

/* Realize the neighborhood in memory as a segment collection
 * 
 * 
 * */

/*template<typename segmentcollection, typename point, typename index_distance_getter>
segmentcollection compute_Ne(segmentcollection &D, 
		size_t index, double epsilon, index_distance_getter &_d)
{
	std::vector<bool> result = compute_Ne_containment(D,index,epsilon,_d);

	segmentcollection ret;
	for(size_t i = 0; i < result.size(); i++)
	  if(result[i])
		ret.push_back(D[i]);
	return ret;	
};
*/

/*
 * Traclus Implementation of libTrajcomp
 * 
 * The Traclus algorithm is a classical algorithm for trajectory clustering.
 * In this C++ header, the two most important steps are implemented:
 * 		1) Creating a trajectory segment set
 * 		2) Clustering of these segments
 *      3) Removement of cluster segments, which have enoguh different 
 *         input segments but not enough different trajectories
 * 
 * Basically, traclus has two parameters and follows the density clustering
 * approach. 
 * 	 epsilon:  threshold defining nearness of trajectories
 *             Note that this epsilon is relative to a non-straightforward definition
 *             of similarity given in total_distance and calc_dist
 *
 *   minLines: Minimum number of distinct lines to make a segment a core
 *             line segment 
 * 
 * */


/*CONSTANTS FOR SPECIAL CLUSTERS*/



/**
 * Compute the epsilon-neighborhood as a vector of indizes given a line segment
 * as an index into the collection of line segments L
 * 
 * Note that the line segment idx is *not* added 
 * */
 //std::vector<tLineSegment<point>> &L,size_t i, size_t j
 
//I think this is the fundamental point of change -
//instead of searching the vector, we use the rtree search
//might need to do that in other places
//problem is distance function is more complex
    
template<typename point, typename distance_functor,typename progress_visitor>
std::vector<size_t> compute_NeIndizes(std::vector<tLineSegment<point>> &L,size_t idx,
		double eps, distance_functor &_d, progress_visitor pvis)
{
	std::vector<size_t> ret;
	#pragma omp parallel for
	for (size_t i=0; i < L.size(); i++)
	{
		#pragma omp critical
		pvis(0,0,"Phase 2: Expanding a single cluster");
	  if (idx != i)
//		if (_d(L[i].s,L[i].e,L[idx].s,L[idx].e,i*L.size() + idx) <= eps)
		if (_d(L,i,idx) <= eps)
		{
		  #pragma omp critical
	      ret.push_back(i);
	    }
	}
	return ret;	
}
    
template<typename point, typename distance_functor,typename progress_visitor>
std::vector<size_t> compute_NeIndizes2(std::vector<tLineSegment<point>> &L,size_t idx,
                                      double eps, distance_functor &_d, progress_visitor pvis)
{
    std::vector<size_t> ret;
    
//commenting out old code
//#pragma omp parallel for
//    for (size_t i=0; i < L.size(); i++)
//    {
//#pragma omp critical
//        pvis(0,0,"Phase 2: Expanding a single cluster");
//
//        //this here should be substituted by a query to the rtree
//        if (idx != i)
//            //        if (_d(L[i].s,L[i].e,L[idx].s,L[idx].e,i*L.size() + idx) <= eps)
//            if (_d(L,i,idx) <= eps)// this is the critical line
//            {
//#pragma omp critical
//                ret.push_back(i);
//            }
//    }
    
    
    //starting with first point
    seg curSeg(point_boost(L[idx].s[0],L[idx].s[1]),point_boost(L[idx].e[0],L[idx].e[1]));
    value curValue(std::make_tuple(curSeg,0,-1));
    
    //oh god this is horrifying
    //and redundant
    box searchBox;
    searchBox.min_corner().set<0>(bg::get<0, 0>(curSeg) < bg::get<1, 0>(curSeg) ? bg::get<0, 0>(curSeg) : bg::get<1, 0>(curSeg));
    searchBox.min_corner().set<1>(bg::get<0, 1>(curSeg) < bg::get<1, 1>(curSeg) ? bg::get<0, 1>(curSeg) : bg::get<1, 1>(curSeg));
    searchBox.max_corner().set<0>(bg::get<0, 0>(curSeg) < bg::get<1, 0>(curSeg) ? bg::get<1, 0>(curSeg) : bg::get<0, 0>(curSeg));
    searchBox.max_corner().set<1>(bg::get<0, 1>(curSeg) < bg::get<1, 1>(curSeg) ? bg::get<1, 1>(curSeg) : bg::get<0, 1>(curSeg));
    
    //ok I get what is happening
    //bg::expand(searchBox,point_boost(bg::get<0, 0>(curSeg)*2,bg::get<0, 1>(curSeg)*2));
    
    //dont think not expanding is good, but gonna test it
    //bg::expand(searchBox,point_boost(searchBox.min_corner().get<0>()-(eps),searchBox.min_corner().get<1>()-(eps)));
    //bg::expand(searchBox,point_boost(searchBox.max_corner().get<0>()+(eps),searchBox.max_corner().get<1>()+(eps)));
    
//    fabs(eps);
    //searchBox.min_corner().
    std::vector<value> returnedValues;
    //rtree.query(bgi::nearest(curSeg, 500) && bgi::satisfies(),std::back_inserter(returnedValues));
    //this unarypredicate alow us to only get unclassified segments
    //this is just a test query
//    rtree.query(
//                bgi::nearest(curSeg, 500) &&
//                bgi::satisfies([](value const& v) {return (std::get<2>(v)>0);}),
//                std::back_inserter(returnedValues));
    
    
    //rtree.query(bgi::nearest(curSeg, 300)
    //could use the rtree query to limit to values on screen maybe
    rtree.query(!bgi::disjoint(searchBox)
                && bgi::satisfies([&](value const& v) {return calc_dist2(curValue,v) <= eps;})
                && bgi::satisfies([&](value const& v){return (std::get<1>(v) != idx);})
                && bgi::satisfies([&](value const& v){return (std::get<2>(v) != -4); }),
                std::back_inserter(returnedValues));
    
    std::transform(begin(returnedValues),end(returnedValues),std::back_inserter(ret),
                   [](auto const& tuple){return std::get<1>(tuple);});
    //std::cout << ret.size() << "\n";
    return ret;
}
    
/**
 * Expand the epsilon-connected neighborhood of a cluster
 * using the line segment collection L, the queue of unprocessed elements
 * to be looked at, epsilon, minlines, the clusterID to be assigned and 
 * the distance functional.
 * 
 */

template<typename point,typename distance_functor, typename visitor>
void expandCluster(std::vector<tLineSegment<point>> &L,
				   std::queue<size_t> &Q,
					double eps,	size_t minLines, size_t clusterID,distance_functor &_d, visitor &pvis)
{
     while (!Q.empty())
     {
		 pvis(0,0,"Phase 2: Ticking Q");
		  size_t m = Q.front();
		 auto Ne = compute_NeIndizes2(L,m,eps,_d, pvis);
		 Ne.push_back(m);		 		 
		 if (Ne.size() >= minLines)
		 {
			for (auto it = Ne.begin(); it != Ne.end(); it++)
			{
		
				if (L[*it].cluster == UNCLASSIFIED)	
					Q.push(*it);
				if (L[*it].cluster < 0 && L[*it].cluster > -4)
				    	L[*it].cluster = clusterID;
			}
		 }
		 // WAR IN l. 310, hat lorenz wieder anders gemacht
//		 if (L[m].cluster < 0)
	//					L[m].cluster = clusterID;

		 Q.pop();
	 }
}

/**
 * Perform the grouping. This is the central clustering work:
 * 
 * Finds the next UNCLASSIFIED segment, calculates the epsilon-neighborhood,
 * if it is large enough (minLines) expands the Cluster using density-connectedness and
 * the current clusterID, which is then incremented, 
 * otherwise it marks the eps-neighborhood as NOISE segments
 * 
 * @Question: something, that is in eps-distance to some segment which
 * is not a core segment will never become a core segment?
 * */
template<typename point,typename distance_functor, typename progress_visitor>
    void grouping(std::vector<tLineSegment<point>> &L, double eps, size_t minLines,
				distance_functor &_d, progress_visitor &pvis)
{
    
	size_t clusterID=0;
	std::queue<size_t> Q;
	pvis.init(L.size());
	for (size_t i=0; i < L.size(); i++)
	{
		
		pvis(i,L.size(),"Phase 2: Clustering Segments");
		if (L[i].cluster == UNCLASSIFIED)
		{
			std::vector<size_t> Ne = compute_NeIndizes2<point>(L,i,eps,_d,pvis);
			//cout << "Ne[" << i << "]="<< Ne.size() << endl;
            //std::cout << Ne.size() << "\n";
			if (Ne.size()+1 >= minLines)
			{
				L[i].cluster = clusterID;
				for (auto it = Ne.begin(); it != Ne.end(); it++)
				{
					L[*it].cluster = clusterID;
					Q.push(*it);
				}
				expandCluster(L,Q, eps,minLines,clusterID,_d,pvis);
				clusterID++;
			}else  // not minLines
			{
				L[i].cluster = NOISE; 
			}
		}		
		
	}
	/*Step 3: check that clusters come from more than minLines 
	 * 		  different trajectories*/
	for (int i=0; i < (int)clusterID; i++)
	{
		std::set<size_t> sources;
		for (size_t j = 0; j < L.size(); j++)
		  if (L[j].cluster == i)
		    sources.insert (L[j].trajectory_index);
		    
		if (sources.size() < minLines)
		{
		   for (size_t j = 0; j < L.size(); j++)
		      if (L[j].cluster == i)
			     L[j].cluster = REMOVED;
		}
	}
	/*Step 3a: ClusterID compression into a range*/
    //TODO - this is probably not very efficient, replace the double for with algorithm
    std::map<int,std::vector<int>> clusters;
    
    //pairing the cluster ids with the segment indexes
    for(int i = 0; i < L.size(); i++){
        clusters[L[i].cluster].push_back(i);
    }
    
	//renaming the clusters in the segment list
    //changed here to use a global clusterID system
    for(auto &curCluster : clusters){
        if(curCluster.first <0)//we only care about the one >0, smaller are error codes basically
            continue;
        
        for(auto &curPos : curCluster.second){
            L[curPos].cluster = ClusterManager::clusterID;
        }
        ClusterManager::clusterID++;
    }

    std::cout << clusters.size();
    /* Step 4: Representation*/	
    // left out, will do that later. This step creates a representative
    // trajectory for a cluster.
    	
};


template<typename point, typename segment>
point averagePoint(std::unordered_map<int,segment> &intersectingSegments,point &intersectionPoint )
{
    typedef typename point::value_type pointPos;

    //segment from (X0,Y0) to (X2,Y2)
    //we know point X1 = intersectionPoint.X is on the line segment
    //must find point Y1, the other coordinate of the intersection
    //considering the form y = mx + b
    // m = Y2-Y0/X2-X0
    // b = Y2-m*X2
    // b = Y0-m*X0
    // Y1 = m * (X1) + B
    //Y1 = (Y2-Y0/X2-X0) * X1 + (Y2-(Y2-Y0/X2-X0))*X2
    //easier pre calculating m -> Y1 = m * X1 + Y2 - m * X2 -> Y1 = m * (X1 -X2) + Y2
    //Y1 = m * X1 + Y0 - m * X0 -> Y1 = m * ( X1 - X0) + Y0
    //x1 - x2 or x1 - x0 give the same result? kinda weird
    
    pointPos position = 0;
    pointPos x1 = intersectionPoint[0];
    //will keep accumulator here, but diff value has to be smaller
    //might not need accumulator after all
    long double accumulator = 0;
    
    //a few problem cases
    //segments where X1 IS either X0 or X2
    //vertical segments
    
    for(auto curSegment : intersectingSegments){
        point start = curSegment.second[0];
        point end = curSegment.second[1];
        
        pointPos x0 = start[0];
        pointPos y0 = start[1];
        
        pointPos x2 = end[0];
        pointPos y2 = end[1];
        
        pointPos y1;
        //check for equalities to deal with vertical lines and segments that intersect at an end point
        
        if(x0 == x2){//vertical line - for now just using the first height, might ignore those lines later
            y1 = y0;
        }
        else{
            if(x0 == x1){//we are at the start of the line
                y1 == y0;
            }
            else if(x2 == x1){//or the end
                y1 == y2;
            }
            else{//or any other point
                //having problems when x2 is too close to x0
                //might ignore and consider m = 0, so the line has no slope
                //in that case maybe just average the heights?
                //of course if x2-x0 is so close to 0 the segment is really small, so might make sense to just ignore it
                
                pointPos x2x0 = x2 - x0;
                if(fabs(x2x0) < 1){//assume vertical line
                    y1 = y0;//same case as above
                }
                else{
                    pointPos m = (y2-y0)/x2x0;
                    y1 = m * (x1-x0) +y0;
                }
                //pointPos y12 = m * (x1-x2) +y2;
            }
        }
        accumulator += y1;
    }
    
    
    accumulator /= intersectingSegments.size();
    position = accumulator;
    point finalPoint{x1,position};
    
    return finalPoint;
}
    
//really need to clean up this function
//Trajectory Manager should hold the representative trajectories, so we'll be calling this function from there
template<typename point>
std::map<int,std::vector<point>> representative_trajectory(std::vector<tLineSegment<point>> &L, int minLines)
{
    //ok this is the thing
    //compute average direction vector
    std::vector<std::vector<double>> average_cluster_direction;
    
    typedef point direction;
    typedef std::vector<point> segment;
    //typedef std::vector<direction> segment;
    
    std::map<int,std::vector<direction>> clusterDirections;
    std::map<int,std::vector<direction>> clusterSegments;
    std::map<int,std::vector<segment>> clusterSegmentsOutro; //this one might be the most useful because we can separate in start and end
    std::map<int,std::vector<directedPoint<point>>> clusterPoints;
    std::map<int,std::vector<point>> representativeTrajectories;//return this later
    
    for(int i = 0; i < L.size(); i++){
        //need to get the sum of vectors from each cluster
        //but how to iterate by cluster here?
        //transform?
        //cluster direction is ok because its one vector per segment
        direction curDirection{L[i].e[0]-L[i].s[0],L[i].e[1]-L[i].s[1]};
        clusterDirections[L[i].cluster].push_back(curDirection);
        
        //this one is the problematic one, to preserve order
        //just realized that in a given trajectory, segments have the same start as the end of the previous segments
        //so we end up, in the sweep phase, with a bunch of left sides with the same coordinates as a right side
        
        clusterSegments[L[i].cluster].push_back(L[i].s);
        clusterSegments[L[i].cluster].push_back(L[i].e);
        
        //but still dont think we can sort by x AND remember if it is start or end...
        clusterSegmentsOutro[L[i].cluster].push_back(std::vector<point>{L[i].s,L[i].e});
        
        //could fill the directedPoints here
        //how can we be sure that the start point is always left?
        //because once we order by x coordinate, we cant "enter" a segment from the right
        //so we can have a startpoint, marked as left, sorted after the endpoint, sorted as right (of the same segment)
        //so this line segment will be deleted from the intersections before it can even be inserted
        //the partition function only follows the trajectory data,
        //changing so leftmost is left, not start
//        if(L[i].s[0] <= L[i].e[0]){//kinda assuming left grows right, if weird errors occur check axis direction
//            clusterPoints[L[i].cluster].push_back(directedPoint<point>{L[i].s,Side::Left});
//            clusterPoints[L[i].cluster].push_back(directedPoint<point>{L[i].e,Side::Right});
//        }
//        else{//should this be done here or AFTER the rotation ? fuck
//            clusterPoints[L[i].cluster].push_back(directedPoint<point>{L[i].e,Side::Left});
//            clusterPoints[L[i].cluster].push_back(directedPoint<point>{L[i].s,Side::Right});
//        }
    }
//    std::vector<direction> init;
//    std::accumulate(clusterDirections.begin(), clusterDirections.end(), init,
//                    [&](std::vector<direction> ){
//
//                    });

    //std::vector<direction> clusterDirections;
    //auto clusterIt = clusterDirection
    std::vector<direction> directionsSum; //direction is a point which is a std::vector<double>
    auto directionIt = directionsSum.begin();
    
    //dont think will use this
    //std::vector<direction> representativeTrajectory;// will have to save a vector of these
    
    //just realized this for is GIGANTIC, have to break this up
    for(auto &cluster : clusterDirections){ //each cluster is a vector of directions which is a vector of double
        std::cout << cluster.first << "\n";
        if(cluster.first < 0)//was 1 here, but cant forget cluster 0. not calculating the repr. trajs of the outliers
            continue;
        
        direction clusterSum{0,0};//
        //double x=accumulate(v.begin(),v.end(),0.0,[&](double x,double y) {return x+y/v.size();});
        //direction result;
        //why error???? vector of vectors instead of just vector?
        auto result = std::accumulate(cluster.second.begin(),cluster.second.end(),clusterSum,
                        [&](direction currentDirection, direction curSum){
                            return direction{currentDirection[0] + curSum[0],currentDirection[1] + curSum[1]};
                            //return std::vector<double>{clusterSum[0] + currentDirection[0],clusterSum[1] + currentDirection[1]};
                        });
        //ugh
        //value for cluster 0 seems too small due to large number of elements, don't know if will cause problems
        
        //this is a vector for all clusters dont forget
        directionsSum.push_back(direction{result[0]/cluster.second.size(),result[1]/cluster.second.size()});
        //double cos_theta = rep_point->dot(*zero_point) / rep_point->dist(Point(0,0,-1)); // cos(theta)
        //glm::vec2
        //double dot = glm::
        float dot = glm::dot(glm::vec2(directionsSum.back()[0],directionsSum.back()[1]),glm::vec2(1,0));
        float length = glm::length(glm::vec2(directionsSum.back()[0],directionsSum.back()[1]));
        float cos_theta = dot/length;
        float sin_theta = sqrt(1 - pow(cos_theta , 2));//this works, but need to look why
        float mat[4] = {cos_theta,sin_theta,-sin_theta,cos_theta};
        glm::mat2 rotation = glm::make_mat2(mat);
        //oh god
        //std::cout << clusterSegments[cluster.first] << "\"
        
        //clusterSegments[cluster.first] already returns the vector, no need to use the second keyword
        //auto test = clusterSegments[cluster.first].begin();
        
        
        //keeping the vectory copy in case we need it
        std::vector<direction> copyVector = clusterSegments[cluster.first];
        
        //rotation is fine
        //cluster segments is always start end start end
        
        std::transform(clusterSegments[cluster.first].begin(),clusterSegments[cluster.first].end(),clusterSegments[cluster.first].begin(),
                      [&](direction &currentPoint){
                          glm::vec2 rotatedPoint = rotation *glm::vec2(currentPoint[0],currentPoint[1]);
                          return direction{rotatedPoint.x,rotatedPoint.y};
                          //currentPoint[0] = rotatedPoint.x;
                          //currentPoint[1] = rotatedPoint.y;
                          //return currentPoint;
                      });
        
       //try to assign the values to the points here
//       std::transform(clusterSegments[cluster.first].begin(),clusterSegments[cluster.first].end(),clusterPoints[cluster.first].begin(),
//                      [&](direction &currentPoint){
//
//                      });

        //fuck it just use a for
        std::unordered_map<int,segment> segments;//maybe can iterate over clusterpoints and search here
        
        for(int i =0; i < clusterSegments[cluster.first].size()-1; i+=2){
            //was inserted start end but rotated now
            //what about vertical lines?
            //should vertical lines even matter in the sweep, since we are going along the direction of the cluster?
            //also what is the vertical coordinate of the vertical segment? the whole segment? would have to pick a point
            if(clusterSegments[cluster.first][i][0] < clusterSegments[cluster.first][i+1][0]){
                clusterPoints[cluster.first].push_back(directedPoint<point>{clusterSegments[cluster.first][i],Side::Left,i>>1});
                clusterPoints[cluster.first].push_back(directedPoint<point>{clusterSegments[cluster.first][i+1],Side::Right,i>>1});
                segments.insert(std::make_pair(i>>1,segment{clusterSegments[cluster.first][i],clusterSegments[cluster.first][i+1]}));
            }
            else{
                clusterPoints[cluster.first].push_back(directedPoint<point>{clusterSegments[cluster.first][i+1],Side::Left,i>>1});
                clusterPoints[cluster.first].push_back(directedPoint<point>{clusterSegments[cluster.first][i],Side::Right,i>>1});
                //segments.insert(std::make_pair(i>>1,segment{clusterSegments[cluster.first][i],clusterSegments[cluster.first][i+1]}));
                //testing if changing the order matters
                segments.insert(std::make_pair(i>>1,segment{clusterSegments[cluster.first][i+1],clusterSegments[cluster.first][i]}));
            }
        }
        std::transform(clusterSegmentsOutro[cluster.first].begin(),clusterSegmentsOutro[cluster.first].end(),clusterSegmentsOutro[cluster.first].begin(),
                       [&](segment &currentSegment){
                           glm::vec2 rotatedStart = rotation *glm::vec2(currentSegment[0][0],currentSegment[0][1]);
                           glm::vec2 rotatedEnd = rotation *glm::vec2(currentSegment[1][0],currentSegment[1][1]);
                           return segment{direction{rotatedStart.x,rotatedStart.y},direction{rotatedEnd.x,rotatedEnd.y}};
                           //return direction{rotatedPoint.x,rotatedPoint.y};
                           //currentPoint[0] = rotatedPoint.x;
                           //currentPoint[1] = rotatedPoint.y;
                           //return currentPoint;
                       });
        
        //and rotate the other one why not - I'm assuming that the side property is mantained
        //dont need to rotate this anymore
//        std::transform(clusterPoints[cluster.first].begin(),clusterPoints[cluster.first].end(),clusterPoints[cluster.first].begin(),
//                       [&](directedPoint<point> &currentPoint){
//                           point originalPos = currentPoint.position;
//                           Side originalSide = currentPoint.side;//just to be sure
//                           glm::vec2 rotatedPos = rotation *glm::vec2(originalPos[0],originalPos[1]);
//                           point newPos{rotatedPos.x,rotatedPos.y};//why doesnt work with constructor inside constructor?
//                           return directedPoint<point>{newPos,originalSide};//are we sure its still the original side? cant change here
//                       });

        //if transform this vector also remember to use the original values
//        std::transform(clusterSegments[cluster.first].begin(),clusterSegments[cluster.first].end(),copyVector.begin(),
//                       [&](direction &currentPoint){
//                           glm::vec2 rotatedPoint = rotation *glm::vec2(currentPoint[0],currentPoint[1]);
//                           return direction{rotatedPoint.x,rotatedPoint.y};
//                       });

    //now need to sort the points based on the x' coordinate
    //but do we know based on the direction structure which is a first or second? dont think so
        std::sort(clusterSegments[cluster.first].begin(),clusterSegments[cluster.first].end(),
                  [](direction &firstPoint,direction &secondPoint){
                      return firstPoint[0] < secondPoint[0];
                  });

        //though about using tLineSegment, but the CLASS stores which point is the start or end
        //gonna create a struct that adds that information to the point itself
        //do a transform from clusterSegmentsOutro to a vector of this struct and THEN sort
        
        //how to sort the POINTS and remember if it is a start or end? think will need a structure
        std::sort(clusterSegmentsOutro[cluster.first].begin(),clusterSegmentsOutro[cluster.first].end(),
                  [](segment &firstPoint,segment &secondPoint){
                      return firstPoint[0] < secondPoint[0];
                  });
        //will need to keep the original segment saved
        //the assignment of the left or right side must occur after rotation
        //now finally sort the points taking into account the side
        std::sort(clusterPoints[cluster.first].begin(),clusterPoints[cluster.first].end(),
                  [](directedPoint<point> &firstPoint,directedPoint<point> &secondPoint){
                      float diff = fabs(firstPoint.position[0]-secondPoint.position[0]);
                      if(diff < 0.01){
                      //<< Effective STL>>, Item21: Always have comparison functions return false for equal values.
                          if(firstPoint.side == Side::Left && secondPoint.side == Side::Left){
                              return false;
                          }
                          
                          if(firstPoint.side == Side::Right && secondPoint.side == Side::Right){
                              return false;
                          }
                          //this seems to work?
                          if(firstPoint.side == Side::Left){
                              return true;
                          }
                          
                          if(secondPoint.side == Side::Left){
                              return false;//this means second < first
                          }
                      }
                      
                      return firstPoint.position[0] < secondPoint.position[0];//hmm maybe problem - not deterministic?
                  });
        
        int intersections = 0;
        int lastGeneratedPoint = 0;
        //std::vector<tLineSegment<point>> representativeTrajectory;// will have to save a vector of these
        
        
        //now we need to sweep to generate the points
        //for(auto &curDirection : clusterSegments[cluster.first]){
        
        //ok now the idea is that we wont need all the mess down there (most of it anyway)
        //note that this for is STILL inside the for that iterates the maps of the clusters
        
        //need to insert the segments into the list
        
        std::unordered_map<int,segment> intersectingSegments;
        
        
        for(auto &curPoint : clusterPoints[cluster.first]){
            if(curPoint.side == Side::Left){
                //add segment to list of segments
                intersections++;
                intersectingSegments.insert(std::make_pair(curPoint.segID, segments[curPoint.segID]));
            }
            if(intersections >= minLines){
                //generate the point P here, think we should do it before deleting a right side
                //being inside this if sorta guarantees we wont call it on the first element
                
                point previous = (*std::prev(&curPoint)).position;
                double diff = fabs(previous[0]-curPoint.position[0]);
                if(diff < 1)//testing with smaller value - might make sense to keep track of how many have been added and force after some
                    continue;
                
                point newPoint = averagePoint(intersectingSegments,curPoint.position);
                representativeTrajectories[cluster.first].push_back(newPoint);
                
            }
            //when we reach a right side we remove the segment, but only after adding another point to the trajectory
            if(curPoint.side == Side::Right){
                //remove segment of list of segments
                intersections--;
                intersectingSegments.erase(curPoint.segID);
            }
        }
        
        //unrotate the points
        glm::mat2 unrotate = glm::transpose(rotation);
        std::transform(representativeTrajectories[cluster.first].begin(),representativeTrajectories[cluster.first].end(),
                       representativeTrajectories[cluster.first].begin(),
                       [&](point &currentPoint){
                           glm::vec2 unrotatedPoint = unrotate * glm::vec2(currentPoint[0],currentPoint[1]);
                           return point{unrotatedPoint.x,unrotatedPoint.y};
                       });

        //ok so the idea is, we start at the leftmost point, and for each X' point we count the number of line segments that intersect it
        //how do we do it - well, this number changes only when the sweep line passes a starting point or an ending point
        //so we start at zero for the first point
        //we check the second point - is it at the same X' coord? is it a start point? we add it - actually we also add it if is end point
        //and so on and on for every point
        //also dont think will be using this
        
        //OK I'm FAIRLY certain this is doing nothing, just wasting time and was only one of the old ways to get the repr. traj
        //BUT, who knows, maybe I left it here for a reason?
        //Only thing is certain is it is impossible to use with the 0 cluster, just too slow
        //TODO - remove this on a later commit
//        for(int p = 0; p < clusterSegments[cluster.first].size(); p++){
//            std::vector<direction> points;
//            int intersections = 0;
//            for(int q = 0; q < clusterSegments[cluster.first].size()-1; q+=2){
//                if(clusterSegments[cluster.first][p][0] >= clusterSegments[cluster.first][q][0] &&
//                   clusterSegments[cluster.first][p][0] <= clusterSegments[cluster.first][q+1][0]){
//                    //point between a line segment
//                    //ignore if the line segments x coords are the same
//                    if(clusterSegments[cluster.first][q][0] == clusterSegments[cluster.first][q+1][0]){
//                        continue;
//                    }//vertical line
//                    else if(clusterSegments[cluster.first][q][1] == clusterSegments[cluster.first][q+1][1]){
//                        intersections++;
//                        points.push_back(direction{clusterSegments[cluster.first][p][0],clusterSegments[cluster.first][q][1]});
//                    }
//                    else{//have to calculate where in the line horizontally we landed
//                        intersections++;
//                        points.push_back(direction{clusterSegments[cluster.first][p][0],(clusterSegments[cluster.first][q+1][1] - clusterSegments[cluster.first][q][1]) /
//                        (clusterSegments[cluster.first][q+1][0] - clusterSegments[cluster.first][q][0]) *
//                            (clusterSegments[cluster.first][p][0] - clusterSegments[cluster.first][q][0] + clusterSegments[cluster.first][q][1])});
//                    }
//                }
//            }
//        }
        
        //really dont think will be using this
        //will find a better way later for the sweeping without two for loops, for now just to see if it works
        //here we start with the sorted points
//        for(int i = 0; i< clusterSegments[cluster.first].size(); i++){
//            int intersections = 0;
//            glm::vec2 trajpoint(0,0);
//            //here we go over the same points, for each one (yeah, bad)
//            for(int j = 0; j < clusterSegments[cluster.first].size()-1; j++){
//                //Point s = seg_global[i][q]->getSegment().first,e = seg_global[i][q]->getSegment().second;
//                //if(sort_point[p].getX() <= e.getX() && sort_point[p].getX() >= s.getX()){ // 在一个线段的中间
//                //this is an ugly if
//                //fuck forgot to limit this
//                //if the x coordinate of the original point is between a line segment
//                if(clusterSegments[cluster.first][i][0] <= clusterSegments[cluster.first][j+1][0]
//                   && clusterSegments[cluster.first][i][0] >= clusterSegments[cluster.first][j][0]){
//                    //ignore if the line segments x coords are the same
//                    if(clusterSegments[cluster.first][j][0] == clusterSegments[cluster.first][j+1][0]){
//                        continue;
//                    }
//                    //if the line segment is vertical, we add as an intersection
//                    //the coordinate of the point gets the x from the original and just the one y from the line segment
//                    else if(clusterSegments[cluster.first][j][1] == clusterSegments[cluster.first][j+1][1]){
//                        intersections++;
//                        trajpoint += glm::vec2(clusterSegments[cluster.first][i][0],clusterSegments[cluster.first][j][1]);
//                    }
//                    //but if the y coordinates are different,
//                    else{
//                        intersections++;
//                        //Point(sort_point[p].getX() ,(e.getY() - s.getY()) / (e.getX() - s.getX()) * (sort_point[p].getX() - s.getX()) + s.getY(), -1);
//                        //I dont even know what I'm doing here
//                        //we get the difference between the ys of the line segment, divide by the difference of the xs
//                        //multiply by the original points x - the start line segment ex + the start segment y
//                        //one difference from my implementation to theirs is that they are using both the sorted AND unsorted
//                        //sets of points - sorted for the start p point, unsorted for the rest
//                        //so I dont know what is right or wrong here - the paper doesnt make it clear
//                        trajpoint += glm::vec2(clusterSegments[cluster.first][i][0],
//                                           (clusterSegments[cluster.first][j+1][1] - clusterSegments[cluster.first][j][1]) /
//                                           (clusterSegments[cluster.first][j+1][0] - clusterSegments[cluster.first][j][0]) *
//                                           (clusterSegments[cluster.first][i][0] - clusterSegments[cluster.first][j][0] + clusterSegments[cluster.first][j][1]));
//                    }
//                }
//            }
//            if(intersections >= minLins){
//                trajpoint /= intersections;
//                glm::mat2 reverse = glm::transpose(rotation);
//                trajpoint = reverse * trajpoint;
//
//                //int size_point;
//                //if(representativeTrajectory.size() >= 0){
//                int size_point = representativeTrajectory.size() -1;
//                glm::vec2 lastPoint = trajpoint;
//                if(size_point >= 0){
//                    lastPoint = glm::vec2(representativeTrajectory[size_point][0],representativeTrajectory[size_point][1]);
//                }
//                if(size_point <0 || ( size_point >=0 && glm::distance(trajpoint,lastPoint) > 10)){//magic number
//                    representativeTrajectory.push_back(direction{trajpoint.x,trajpoint.y});
//                }
//            }
//        }
        
    }
    //now need to average the direction of each cluster by adding all directions and dividing by the number of elements
    //then rotate the points of the cluster around this axis
    //and do the rest of the things on the paper (easy!)
    
    //why was the number different?
    std::cout << clusterSegments[0].size() << "\n";
    
    return representativeTrajectories;
}
    
class traclus_progress_visitor
{
	public:
	void init(size_t count)
	{
		(void)count;
	}
	void operator() (size_t step, size_t count, std::string phase)
	{
	    (void)step;(void)count;(void)phase;	// suppresses unused warning
	}
	void finish()
	{
	}
	
};

    
/*Traclus Main Function with progress_visitor */

template<typename TrajectoryCollection, typename partitioning_functional,
typename distance_functional, typename progress_visitor>
std::vector<tLineSegment<typename TrajectoryCollection::value_type::value_type>> 
traclus(TrajectoryCollection &A, double eps, size_t minLines, partitioning_functional &part,
distance_functional &_d, progress_visitor  &pvis)
{
	
	typedef typename TrajectoryCollection::value_type TrajectoryType;
	std::vector<size_t> index_map;
	
    
	std::vector<tLineSegment<typename TrajectoryType::value_type>> segments;
    
    
	TrajectoryType L;
	size_t i=0;

	pvis.init(A.size());
	auto it = A.begin();
	while (it != A.end())
	{
		if((*it).size() == 0)		// ignore empty
			continue;
		TrajectoryType CPi;
        std::vector<int> indices;
		 CPi = part(*it,indices);
		 for (size_t k=0; k < CPi.size() -1; k++)
		 {
             //basically here we are saving from which trajectory the segment came from, and where in the point list it starts
            segments.push_back(tLineSegment<typename TrajectoryType::value_type>(CPi[k],CPi[k+1],i,indices[k]));
            //segments.push_back(tLineSegment<typename TrajectoryType::value_type>(CPi[k],CPi[k+1],i,indices[k]));
		 }
		it++;i++;
		pvis(i,A.size(),"Phase 1: Segment Creation");
	}
    
    //need to create rtree here with the sub trajectory segments
    //but still need to pass it to other functions
    

    // create the rtree using default constructor
    //gonna try to use the line segments directly
    
    auto itseg =  segments.begin();
    std::vector<value> values;
    
    
//    for (auto segment : segments){
//        point_boost start;
//        bg::set<0>(start, segment.s[0]);
//        bg::set<1>(start, segment.s[1]);
//        point_boost end;
//        bg::set<0>(end, segment.e[0]);
//        bg::set<1>(end, segment.e[1]);
//
//        seg curSeg(start,end);
//
//        size_t dist = itseg - segments.begin();
//        itseg++;
//
//        //values.push_back(std::make_tuple(curSeg,dist,segment.cluster));
//        rtree.insert(std::make_tuple(curSeg,segment.trajectory_index,segment.cluster));
//        //trying something different here
////        rtree.insert(std::make_tuple(curSeg,dist,segment.cluster));
//    }
    
    point_boost start;
    point_boost end;
    
    //auto itseg =  segments.begin();
    
//    std::transform(segments.begin(),segments.end(),std::back_inserter(segments),[&](tLineSegment<typename TrajectoryType::value_type>& segment){});
    
    //should MOST DEFINATELY move this stuff into a method
    std::vector<glm::vec2> corners;//2 cornerss
    GeoPosition location{Map::lat,Map::lon};
    corners = Map::Corners(location);
    
    glm::vec3 minC = TrajParser::latLonToMeters(corners[1].x, corners[0].y, 0);
    glm::vec3 maxC = TrajParser::latLonToMeters(corners[0].x, corners[1].y, 0);
    //this doesnt work if we are between zones
    
    std::vector<double> minCorner{minC.x/TrajectoryManager::scaleFactor,minC.z/TrajectoryManager::scaleFactor};
    std::vector<double> maxCorner{maxC.x/TrajectoryManager::scaleFactor,maxC.z/TrajectoryManager::scaleFactor};
    
    std::string minLat = std::to_string(corners[1].x);//should map lat to y and lon to x
    std::string maxLat = std::to_string(corners[0].x);
    
    std::string minLon = std::to_string(corners[0].y);
    std::string maxLon = std::to_string(corners[1].y);
    
    std::transform(segments.begin(),segments.end(),segments.begin(),
                     [&](tLineSegment<typename TrajectoryType::value_type>& segment){
                         //segment.s (x,y) must be between minCorner and MaxCorner
                         //segment.e (x,y) must be between minCorner and MaxCorner
                         
                         if(!(segment.s[0] >= minCorner[0] && segment.s[0] <= maxCorner[0])){
                             segment.cluster = -4;}
                         if(!(segment.s[1] >= minCorner[1] && segment.s[1] <= maxCorner[1])){
                             segment.cluster = -4;}
                         if(!(segment.e[0] >= minCorner[0] && segment.e[0] <= maxCorner[0])){
                             segment.cluster = -4;}
                         if(!(segment.e[1] >= minCorner[1] && segment.e[1] <= maxCorner[1])){
                             segment.cluster = -4;}
                         //return std::make_tuple(seg(start,end),0,0);//});
                         //return false;
                         return segment;
                     });
    
    //oh god this is horrible
//    auto removeIter = std::remove_if(segments.begin(),segments.end(),
//                   [&](tLineSegment<typename TrajectoryType::value_type>& segment){
//                       //segment.s (x,y) must be between minCorner and MaxCorner
//                       //segment.e (x,y) must be between minCorner and MaxCorner
//                       
//                       if(!(segment.s[0] >= minCorner[0] && segment.s[0] <= maxCorner[0])){
//                           return true;}
//                       if(!(segment.s[1] >= minCorner[1] && segment.s[1] <= maxCorner[1])){
//                           return true;}
//                       if(!(segment.e[0] >= minCorner[0] && segment.e[0] <= maxCorner[0])){
//                           return true;}
//                       if(!(segment.e[1] >= minCorner[1] && segment.e[1] <= maxCorner[1])){
//                           return true;}
//                       
//                       return false;
//                   });
//    
//    segments.erase(removeIter,segments.end());
    
    std::transform(segments.begin(),segments.end(),std::back_inserter(values),
                   [&](tLineSegment<typename TrajectoryType::value_type>& segment){
                       bg::set<0>(start, segment.s[0]);
                       bg::set<1>(start, segment.s[1]);
                       bg::set<0>(end, segment.e[0]);
                       bg::set<1>(end, segment.e[1]);
                       size_t dist = itseg - segments.begin();
                       itseg++;
                       return std::make_tuple(seg(start,end),dist,segment.cluster);});
    
    //create tree using packing algorithm
    bgi::rtree< value, bgi::rstar<30> > rtree2(values);
    rtree = std::move(rtree2);
    
    //rtree.insert(values.begin(),values.end());
    
    


    std::cout << rtree.size() << "\n";
    
//commenting this out for testing
//    seg curSeg(point(segments[0].s[0],segments[0].s[1]),point(segments[0].e[0],segments[0].e[1]));
//
//    value curValue(std::make_tuple(curSeg,0,-1));
////   bg::set<0>(start, segment.s[0]);
////   bg::set<1>(start, segment.s[1]);
////   point end;
////   bg::set<0>(end, segment.e[0]);
////   bg::set<1>(end, segment.e[1]);
//
//    std::vector<value> returnedValues;
//    //rtree.query(bgi::nearest(curSeg, 500) && bgi::satisfies(),std::back_inserter(returnedValues));
//    //this unarypredicate alow us to only get unclassified segments
//    rtree.query(//bgi::nearest(curSeg, 500) &&
//                bgi::satisfies([&](value const& v) {return calc_dist2(curValue,v) < eps;}), /*(std::get<2>(v)>0);}),*/
//                //bgi::satisfies([](value const& v) {return (std::get<2>(v)>0);}),
//                std::back_inserter(returnedValues));
//
//    std::vector<size_t> ret;
//    std::transform(begin(returnedValues),end(returnedValues),std::back_inserter(ret),
//                   [](auto const& tuple){return std::get<1>(tuple);});
    
     auto startTime = std::chrono::steady_clock::now();
    
	 grouping(segments,eps,minLines,_d, pvis);
    
     auto endTime = std::chrono::steady_clock::now();
     std::cout << "elapsed time: " << std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count() << "ms \n";

     pvis.finish();
	 return segments;
};


// and without a progress bar (i.e. an empty one)
template<typename TrajectoryCollection, typename partitioning_functional,
typename distance_functional>
std::vector<tLineSegment<typename TrajectoryCollection::value_type::value_type>>  traclus(TrajectoryCollection &A, double eps, size_t minLines, partitioning_functional &part,
distance_functional &_d)
{
   traclus_progress_visitor v;
   return traclus(A,eps,minLines,part,_d,v);
};




}//namespace

#endif
