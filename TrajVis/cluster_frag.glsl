#version 410

//just a very basic shader for this
in vec3 vertColor;

layout(location= 0) out vec4 fragment_colour;

void main () {

    fragment_colour = vec4(vertColor,1.0);
}
