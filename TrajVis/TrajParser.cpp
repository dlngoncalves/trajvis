//
//  TrajParser.cpp
//  Waves
//
//  Created by Diego Gonçalves on 21/05/19.
//  Copyright © 2019 Diego Gonçalves. All rights reserved.
//
#define GL_SILENCE_DEPRECATION
#include "TrajParser.h"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
#include <curl/curl.h>
#include <vector>
#include <sqlite3.h>
#include "Map.hpp"
#include <ctime>
#include <iomanip>
#include <numeric>

glm::vec3 TrajParser::basePosition;
float TrajParser::relativeScale = 1;
GLSLShader *dummyShaderTParser;// shader for use in the sqlite callback

//geolife format
//lat
//lon
//0
//altitude in feet
//date as days
//date
//time

//TODO - get timezone from mapbox, use when loading new trajectories
//https://api.mapbox.com/v4/examples.4ze9z6tv/tilequery/-77.033835,38.89975.json?access_token=pk.eyJ1Ijoic2Vub3JzcGFya2xlIiwiYSI6ImNqdXU4ODQ2NTBnMDk0ZG1obDA4bWUzbmUifQ.gviggw2S34VwFVxshcbj_A

static int trajCallback(void *TrajectoryName, int argc, char **argv, char **azColName)
{
    //TrajParser *traj = (TrajParser *)Trajectory;
    //int i;
    //std::string query,trajname;
    //int rc;
    
    //trajname = argv[0];
    std::string name = argv[0];
    *(std::string *)TrajectoryName = name;
    //query = "SELECT * FROM TRAJSEC WHERE TRAJECTORYNAME IS " + trajname;
    
//    rc = sqlite3_exec(db, query.c_str(),trajCallback, 0, &zErrMsg);
//
//    for(i=0; i<argc; i++){
//      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
//
//    }
//    printf("\n");
    return 0;
}

TransportMode TrajSeg::GetTransportMode(int modeValue)
{
    switch (modeValue) {
        case 0: return TransportMode::Undefined;
        case 1: return TransportMode::Walk;
        case 2: return TransportMode::Bike;
        case 3: return TransportMode::Bus;
        case 4: return TransportMode::Car;
        case 5: return TransportMode::Taxi;
        case 6: return TransportMode::Airplane;
        case 7: return TransportMode::Other;
        case 8: return TransportMode::Boat;
        case 9: return TransportMode::Subway;
        case 10: return TransportMode::Train;
        case 11: return TransportMode::Run;
        case 12: return TransportMode::Motorcycle;
    }
    
    return TransportMode::Undefined;
}

int TrajSeg::GetTransportModeValue(TransportMode mode)
{
    switch (mode) {
        case TransportMode::Undefined: return 0;
        case TransportMode::Walk: return 1;
        case TransportMode::Bike: return 2;
        case TransportMode::Bus: return 3;
        case TransportMode::Car: return 4;
        case TransportMode::Taxi: return 5;
        case TransportMode::Airplane: return 6;
        case TransportMode::Other: return 7;
        case TransportMode::Boat: return 8;
        case TransportMode::Subway: return 9;
        case TransportMode::Train: return 10;
        case TransportMode::Run: return 11;
        case TransportMode::Motorcycle: return 12;
    }
    
    return 0;
}

static int trajSegCallback(void *Trajectory, int argc, char **argv, char **azColName)
{
    TrajParser *traj = (TrajParser *)Trajectory;
    TrajSeg auxSeg;
    int columnCount = 0;
    
    for(int i=0; i<argc; i++){
        
        //printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        if(strcmp(azColName[i],"latitude") == 0){
            auxSeg.lat = atof(argv[i]);
        }
        if(strcmp(azColName[i],"longitude") == 0 ){
            auxSeg.lon = atof(argv[i]);
        }
        if(strcmp(azColName[i],"temperature") == 0 ){
            auxSeg.segWeather.temperature = atof(argv[i]);
        }
        if(strcmp(azColName[i],"datetime") == 0 ){
            auxSeg.timeStamp = argv[i];
        }
        if(strcmp(azColName[i],"elevation") == 0 ){
            auxSeg.elevation = atof(argv[i]);
        }
        if(strcmp(azColName[i],"instantspeed") == 0 ){
            auxSeg.speed = atof(argv[i]);
        }
        if(strcmp(azColName[i],"transport_mode") == 0 ){
            auxSeg.mode = TrajSeg::GetTransportMode(atoi(argv[i]));
        }
        //TODO - Change query so we dont load trajectoryname and can remove this last check
        columnCount++;
        if(columnCount == 8){
            
            if(TrajParser::basePosition.x == 0 && TrajParser::basePosition.y == 0 && TrajParser::basePosition.z == 0){
                //think over here we should get the map lat/lon
                int x = Map::long2tilex(auxSeg.lon,Map::zoom);
                int y = Map::lat2tiley(auxSeg.lat, Map::zoom);
                
                //but im not sure I'm updating this
                //41.5f;
                //1.5f;
                //int x = Map::long2tilex(1.5f,Map::zoom);
                //int y = Map::lat2tiley(41.5f, Map::zoom);

                
                double returnedLat = Map::tiley2lat(y, Map::zoom);
                double returnedLon = Map::tilex2long(x, Map::zoom);
                TrajSeg base;
                base.lat = returnedLat;
                base.lon = returnedLon;
                TrajParser::basePosition = traj->latLonToMeters(returnedLat, returnedLon, Map::zoom);
                //TrajParser::basePosition = traj->convertLatLon(base,glm::vec3(0,auxSeg.elevation,0));

                int posX = Map::long2tilex(returnedLon, Map::zoom);
                int posY = Map::lat2tiley(returnedLat, Map::zoom);

                traj->SetScale(posX, posY, Map::zoom);
            }
            glm::vec3 auxPosition = traj->convertLatLon(auxSeg,TrajParser::basePosition);
            
            glm::vec3 curPoint = glm::vec3(1.0,0.0,0.0);
            curPoint = Weather::getWeatherColor(auxSeg.segWeather.temperature);
            
            traj->tempColors.push_back(curPoint);
            traj->positions.push_back(auxPosition);
            traj->segList.push_back(auxSeg);
            columnCount = 0;
        }
    }
    //TODO replace this with data from the database
    traj->SetAverageSpeed();
    
    return 0;
}

//this is certainly a name
static int trajSegListCallback(void *Trajectories, int argc, char **argv, char **azColName)
{
    std::vector<TrajParser> *trajList = (std::vector<TrajParser> *)Trajectories;
    TrajSeg auxSeg;
    TrajParser *traj;
    long trajname = 0;
    
    int columnCount = 0;
    
    for(int i=0; i<argc; i++){
        
        //printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        if(strcmp(azColName[i],"latitude") == 0){
            auxSeg.lat = atof(argv[i]);
        }
        if(strcmp(azColName[i],"longitude") == 0 ){
            auxSeg.lon = atof(argv[i]);
        }
        if(strcmp(azColName[i],"temperature") == 0 ){
            auxSeg.segWeather.temperature = atof(argv[i]);
        }
        if(strcmp(azColName[i],"datetime") == 0 ){
            auxSeg.timeStamp = argv[i];
        }
        if(strcmp(azColName[i],"elevation") == 0 ){
            auxSeg.elevation = atof(argv[i]);
        }
        if(strcmp(azColName[i],"instantspeed") == 0 ){
            auxSeg.speed = atof(argv[i]);
        }
        if(strcmp(azColName[i],"transport_mode") == 0 ){
            auxSeg.mode = TrajSeg::GetTransportMode(atoi(argv[i]));
        }
        if(strcmp(azColName[i],"trajectoryname") == 0 ){
            std::string trajnameaux = argv[i];
            trajname = std::stol(trajnameaux);
        }
        //TODO - Change query so we dont load trajectoryname and can remove this last check
        columnCount++;
        if(columnCount == 8){
            
            if(trajList->size() >0 && trajList->back().trajName == std::to_string(trajname)){
                if(trajList->back().trajName == std::to_string(trajname)){
                    traj = &trajList->back();
                }
                else{
                    traj = new TrajParser(*dummyShaderTParser);
                    traj->trajName = std::to_string(trajname);
                }
            }
            else{
                traj = new TrajParser(*dummyShaderTParser);
                traj->trajName = std::to_string(trajname);
            }
            
            glm::vec3 auxPosition = traj->convertLatLon(auxSeg,TrajParser::basePosition);
            
            glm::vec3 curPoint = glm::vec3(1.0,0.0,0.0);
            curPoint = Weather::getWeatherColor(auxSeg.segWeather.temperature);
            
            traj->tempColors.push_back(curPoint);
            traj->positions.push_back(auxPosition);
            traj->segList.push_back(auxSeg);
            columnCount = 0;
            
            if(trajList->size() >0 && trajList->back().trajName == std::to_string(trajname)){
                
            }
            else{
                trajList->push_back(*traj);
                delete traj;
            }
        }
    }
    return 0;
}

//creating a new callback because we need to receive an array of trajectory names;
static int trajListCallback(void *Trajectories, int argc, char **argv, char **azColName)
{
    //the callback is called for each row, so no need to iterate
    std::vector<std::string> *trajList = (std::vector<std::string> *)Trajectories;
    
    //no need to check the column because we are only retrieving the trajectory name
    trajList->push_back(argv[0]);
    
    return 0;
}

//should abstract away sql stuff
void insertTrajectory(TrajParser &curTraj,std::string trajName, std::string user,sqlite3 *db)
{
    std::string query;
    int rc;
    char *zErrMsg = 0;
    
    query = "INSERT INTO TRAJECTORY (trajectoryname,user,averagespeed,transport_mode) VALUES(" + trajName + "," + user + "," + std::to_string(curTraj.averageSpeed) + ",0);";//TODO load transport_mode from files
    rc = sqlite3_exec(db, query.c_str(),0, 0, &zErrMsg);
    
    for(auto curSeg : curTraj.segList){
        query = "INSERT INTO TRAJSEG(latitude, longitude, temperature, datetime, elevation, trajectoryname,instantspeed,user) VALUES (";
        query = query + std::to_string(curSeg.lat) + "," + std::to_string(curSeg.lon) + "," + std::to_string(curSeg.segWeather.temperature);
        query = query + ", '" + curSeg.timeStamp + "' ," + std::to_string(curSeg.elevation) + " ," + trajName + " ," + std::to_string(curSeg.speed) + "," + user + ");";
        rc = sqlite3_exec(db, query.c_str(),0, 0, &zErrMsg);
    }
}

std::vector<TrajParser> TrajParser::LoadTrajDescription(std::string file, GLSLShader &shader)
{
    std::vector<TrajParser> trajectories;
    
    std::ifstream trajFile;
    std::string line;
    std::string path;
    trajFile.open(file,std::ifstream::in);
    sqlite3 *db;
    std::string query;
    std::string trajName;
    std::string userID;
    char *zErrMsg = 0;
    
    
    int rc = sqlite3_open("trajectories.db", &db);
    //if(rc){
    int lineNumber = 1;
        //going to use this function now to just load new trajectories from a text file
        //probably wont need to do the first query, since we know already its not on the database
        //just found out a case of two trajectories with same start time- might have to change something
        //TODO check in files time between points in trajectory file - might be two different trajectories.
        //trajectories with transport data have this info on labels file
        if(trajFile.is_open()){
            getline(trajFile,userID);//first line is userID
            while (getline(trajFile,line)) {
                if(line=="")
                    continue;
            
                query = "SELECT * FROM TRAJECTORY WHERE TRAJECTORYNAME IS " + line.substr(0,line.size()-4) + ";";
                rc = sqlite3_exec(db, query.c_str(),trajCallback, &trajName, &zErrMsg);
                std::cout << "Current Line : " << lineNumber << " Current trajectory : " << line.substr(0,line.size()-4) << "\n";
                lineNumber++;
                //if(!(rc ==0 && zErrMsg == NULL)){//in this case the trajectory doesnt exists in the db
                if(trajName == ""){//find a better way to do this
                    path = "trajectories/" + line;
                    TrajParser curTraj(path,shader);
                    trajectories.push_back(curTraj);//TODO remove this later
                    insertTrajectory(curTraj, line.substr(0,line.size()-4), userID, db);
                }
                else{
                    std::cout << "Trajectory " + line.substr(0,line.size()-4) + " was not added to database\n";
                }
                
                trajName = "";
            }
        }
    //}
    
    return trajectories;
}

template < typename T>
std::pair<bool, int > findInVector(const std::vector<T>  & vecOfElements, const T  & element)
{
    std::pair<bool, int > result;
    
    // Find given element in vector
    auto it = std::find(vecOfElements.begin(), vecOfElements.end(), element);
    
    if (it != vecOfElements.end())
    {
        result.second = distance(vecOfElements.begin(), it);
        result.first = true;
    }
    else
    {
        result.first = false;
        result.second = -1;
    }
    
    return result;
}

bool TrajParser::CompareTrajectories(const TrajParser &first,const TrajParser &second)
{
    return first.trajName < second.trajName;
}

std::vector<TrajParser> TrajParser::LoadRow(GLSLShader &shader, int row, std::vector<TrajParser>* baseTrajectories)
{
    std::vector<TrajParser> trajectories;
    sqlite3 *db;
    std::string query;
    char *zErrMsg = 0;

    
    int rc = sqlite3_open("trajectories.db", &db);
    
    std::vector<glm::vec2> corners;//2 cornerss
    corners = Map::RowCorners(row);
    
    //this doesnt work if we are between zones
    std::string minLat = std::to_string(corners[1].x);//should map lat to y and lon to x
    std::string maxLat = std::to_string(corners[0].x);
    
    std::string minLon = std::to_string(corners[0].y);
    std::string maxLon = std::to_string(corners[1].y);
    
    //std::string minLat = std::to_string(location.lat-latDelta);
    //std::string maxLat = std::to_string(location.lat+latDelta);
    //std::string minLon = std::to_string(location.lon-lonDelta);
    //std::string maxLon = std::to_string(location.lon+lonDelta);
    
    //TODO - change here to load everything on one query- or maybe not depending on number of trajectories
    query = "SELECT DISTINCT TRAJECTORYNAME FROM TRAJSEG WHERE LATITUDE BETWEEN " + minLat + " AND " + maxLat + " AND LONGITUDE BETWEEN "
    + minLon + " AND " + maxLon + ";";
    
    std::vector<std::string> trajNames;
    //have to remember that sqlite exec will step over all the rows
    rc = sqlite3_exec(db, query.c_str(),trajListCallback, &trajNames, &zErrMsg);
    
    if(trajNames.size() > 100){
        //TODO - accumulate for strings might be O(n^2), have to look into it.
        std::string query2 = "(";
        query2 = std::accumulate(baseTrajectories->begin(), baseTrajectories->end(), query2,
                                 [&](std::string partialQuery, TrajParser& traj)
                                 {
                                     return partialQuery + traj.trajName + ",";
                                 });
        query2.pop_back();
        query2 += ")";
        
        //could replace this with coordinates I think
        std::string query3 = "(";
        query3 = std::accumulate(trajNames.begin(), trajNames.end(), query3,
                                 [&](std::string partialQuery, std::string trajName)
                                 {
                                     return partialQuery + trajName + ",";
                                 });
        query3.pop_back();
        query3 += ")";
        
        query = "SELECT * FROM TRAJSEG WHERE TRAJECTORYNAME IN " + query3 + " AND TRAJECTORYNAME NOT IN " + query2 + " ORDER BY TRAJECTORYNAME;";
        dummyShaderTParser = &shader;
        rc = sqlite3_exec(db, query.c_str(),trajSegListCallback, &trajectories, &zErrMsg);
        
    }
    else{
        for(int i = 0; i < trajNames.size(); i++){
            //iterate over the trajectory names and get their segments
            TrajParser curTraj{shader};
            curTraj.trajName = trajNames[i];//doing this to allow binary searching, but not very efficient
            if(!std::binary_search(baseTrajectories->begin(), baseTrajectories->end(), curTraj, TrajParser::CompareTrajectories)){
                std::cout << "NOT FOUND : " << trajNames[i] << "\n";//remove if not debugging
                query = "SELECT * FROM TRAJSEG WHERE TRAJECTORYNAME IS " + trajNames[i] + ";";// ORDER BY DATETIME ASC;";
                TrajParser curTrajDB(shader);
                curTrajDB.trajName = trajNames[i];
                rc = sqlite3_exec(db, query.c_str(),trajSegCallback, &curTrajDB, &zErrMsg);
                trajectories.push_back(curTrajDB);
            }
            else{//remove if not debugging
                std::cout << "ALREADY FOUND : " << trajNames[i] << "\n";
            }
        }
    }
    sqlite3_close_v2(db);
    std::transform(trajectories.begin(), trajectories.end(), trajectories.begin(),
                   [](TrajParser &curTraj)
                   {
                       curTraj.SetAverageSpeed();
                       return curTraj;
                   });
    return trajectories;
}

//gonna have a load quad here

//need to encapsulate this stuff
std::vector<TrajParser> TrajParser::LoadColumn(GLSLShader &shader, int column,std::vector<TrajParser>* baseTrajectories)
{
    std::vector<TrajParser> trajectories;
    sqlite3 *db;
    std::string query;
    char *zErrMsg = 0;
    
    
    int rc = sqlite3_open("trajectories.db", &db);
    
    std::vector<glm::vec2> corners;//2 cornerss
    corners = Map::ColumnCorners(column);
    
    //this doesnt work if we are between zones
    std::string minLat = std::to_string(corners[1].x);//should map lat to y and lon to x
    std::string maxLat = std::to_string(corners[0].x);
    
    std::string minLon = std::to_string(corners[0].y);
    std::string maxLon = std::to_string(corners[1].y);

    
    query = "SELECT DISTINCT TRAJECTORYNAME FROM TRAJSEG WHERE LATITUDE BETWEEN " + minLat + " AND " + maxLat + " AND LONGITUDE BETWEEN "
    + minLon + " AND " + maxLon + ";";
    
    std::vector<std::string> trajNames;
    //have to remember that sqlite exec will step over all the rows
    rc = sqlite3_exec(db, query.c_str(),trajListCallback, &trajNames, &zErrMsg);
    
    //TODO - extract as a function
    if(trajNames.size() > 100){
        //TODO - accumulate for strings might be O(n^2), have to look into it.
        std::string query2 = "(";
        query2 = std::accumulate(baseTrajectories->begin(), baseTrajectories->end(), query2,
                                 [&](std::string partialQuery, TrajParser& traj)
                                 {
                                     return partialQuery + traj.trajName + ",";
                                 });
        query2.pop_back();
        query2 += ")";
        
        //could replace this with coordinates I think
        std::string query3 = "(";
        query3 = std::accumulate(trajNames.begin(), trajNames.end(), query3,
                                 [&](std::string partialQuery, std::string trajName)
                                 {
                                     return partialQuery + trajName + ",";
                                 });
        query3.pop_back();
        query3 += ")";
        
        query = "SELECT * FROM TRAJSEG WHERE TRAJECTORYNAME IN " + query3 + " AND TRAJECTORYNAME NOT IN " + query2 + " ORDER BY TRAJECTORYNAME;";
        dummyShaderTParser = &shader;
        rc = sqlite3_exec(db, query.c_str(),trajSegListCallback, &trajectories, &zErrMsg);
        
    }
    else{
        //iterate over trajname to find if trajectories are already loaded, if not add to query and make one query for all
        for(int i = 0; i < trajNames.size(); i++){
            //iterate over the trajectory names and get their segments
            TrajParser curTraj{shader};
            curTraj.trajName = trajNames[i];//doing this to allow binary searching, but not very efficient
            if(!std::binary_search(baseTrajectories->begin(), baseTrajectories->end(), curTraj, TrajParser::CompareTrajectories)){
                std::cout << "NOT FOUND : " << trajNames[i] << "\n";//remove if not debugging
                query = "SELECT * FROM TRAJSEG WHERE TRAJECTORYNAME IS " + trajNames[i] + ";";// ORDER BY DATETIME ASC;";
                TrajParser curTrajDB(shader);
                curTrajDB.trajName = trajNames[i];
                rc = sqlite3_exec(db, query.c_str(),trajSegCallback, &curTrajDB, &zErrMsg);
                trajectories.push_back(curTrajDB);
            }
            else{//remove if not debugging
                std::cout << "ALREADY FOUND : " << trajNames[i] << "\n";
            }
        }
    }
    sqlite3_close_v2(db);
    std::transform(trajectories.begin(), trajectories.end(), trajectories.begin(),
                   [](TrajParser &curTraj)
                   {
                       curTraj.SetAverageSpeed();
                       return curTraj;
                   });
    return trajectories;
}

std::vector<TrajParser> TrajParser::LoadZoom(GLSLShader &shader,std::vector<TrajParser>* baseTrajectories)
{
    std::vector<TrajParser> trajectories;
    //could this be leaking memory?
    sqlite3 *db;
    std::string query;
    char *zErrMsg = 0;
    
    //this minLat maxLat stuff will be based on the limits of the map, at the current zoom level, at the current map size
    float latDelta = 1.0;//this will be more complex later
    float lonDelta = 1.0;
    
    int rc = sqlite3_open("trajectories.db", &db);
    
    std::vector<glm::vec2> corners;//2 cornerss
    GeoPosition location{Map::lat,Map::lon};//dont need to assign the string and vec2 versions, not used here and have default values
    corners = Map::Corners(location);
    
    //this doesnt work if we are between zones
    std::string minLat = std::to_string(corners[1].x);//should map lat to y and lon to x
    std::string maxLat = std::to_string(corners[0].x);
    
    std::string minLon = std::to_string(corners[0].y);
    std::string maxLon = std::to_string(corners[1].y);
    
    //std::string minLat = std::to_string(location.lat-latDelta);
    //std::string maxLat = std::to_string(location.lat+latDelta);
    //std::string minLon = std::to_string(location.lon-lonDelta);
    //std::string maxLon = std::to_string(location.lon+lonDelta);
    
    //TODO - does it make sense to do this query with the same area currently shown on screen? maybe change to be more useful
    //I think I was using the zoom as a way to "catch" trajectories that might've been missed in a previous row/column load
    //but this is a bad way of fixing a different problem
    query = "SELECT DISTINCT TRAJECTORYNAME FROM TRAJSEG WHERE LATITUDE BETWEEN " + minLat + " AND " + maxLat + " AND LONGITUDE BETWEEN "
    + minLon + " AND " + maxLon + ";";
    
    
    std::vector<std::string> trajNames;
    //have to remember that sqlite exec will step over all the rows
    rc = sqlite3_exec(db, query.c_str(),trajListCallback, &trajNames, &zErrMsg);
    
    //problem with zoom is we cant rely on database to find missing trajectories
    //TODO - test performance of both loading methods
    if(trajNames.size() > 100){
        //TODO - accumulate for strings might be O(n^2), have to look into it.
        std::string query2 = "(";
        query2 = std::accumulate(baseTrajectories->begin(), baseTrajectories->end(), query2,
                        [&](std::string partialQuery, TrajParser& traj)
                        {
                            return partialQuery + traj.trajName + ",";
                        });
        query2.pop_back();
        query2 += ")";
        
        //could replace this with coordinates I think
        std::string query3 = "(";
        query3 = std::accumulate(trajNames.begin(), trajNames.end(), query3,
                                 [&](std::string partialQuery, std::string trajName)
                                 {
                                     return partialQuery + trajName + ",";
                                 });
        query3.pop_back();
        query3 += ")";
        
        query = "SELECT * FROM TRAJSEG WHERE TRAJECTORYNAME IN " + query3 + " AND TRAJECTORYNAME NOT IN " + query2 + " ORDER BY TRAJECTORYNAME;";
        dummyShaderTParser = &shader;
        rc = sqlite3_exec(db, query.c_str(),trajSegListCallback, &trajectories, &zErrMsg);
        
    }
    else{
        for(int i = 0; i < trajNames.size(); i++){
            //iterate over the trajectory names and get their segments
            TrajParser curTraj{shader};
            curTraj.trajName = trajNames[i];//doing this to allow binary searching, but not very efficient
            if(!std::binary_search(baseTrajectories->begin(), baseTrajectories->end(), curTraj, TrajParser::CompareTrajectories)){
                //std::cout << "NOT FOUND : " << trajNames[i] << "\n";
                query = "SELECT * FROM TRAJSEG WHERE TRAJECTORYNAME IS " + trajNames[i] + ";";// ORDER BY DATETIME ASC;";
                TrajParser curTrajDB(shader);
                curTrajDB.trajName = trajNames[i];
                rc = sqlite3_exec(db, query.c_str(),trajSegCallback, &curTrajDB, &zErrMsg);
                trajectories.push_back(curTrajDB);
            }
            else{//remove if not debugging
                //std::cout << "ALREADY FOUND : " << trajNames[i] << "\n";
            }
        }
    }
    
    sqlite3_close_v2(db);
    std::transform(trajectories.begin(), trajectories.end(), trajectories.begin(),
                   [](TrajParser &curTraj)
                   {
                       curTraj.SetAverageSpeed();
                       return curTraj;
                   });
    return trajectories;
}

void TrajParser::UnloadRow(int row)
{
    
}

void TrajParser::UnloadColumn(int column)
{
    
}

//will change this here so everything gets in the same buffer, not just the vertex data

TrajParser TrajParser::LoadTrajectory(std::string name, GLSLShader shader)
{
    sqlite3 *db;
    char *zErrMsg = 0;
    std::string query;
    
    int rc = sqlite3_open("trajectories.db", &db);
    query = "SELECT * FROM TRAJSEG WHERE TRAJECTORYNAME IS " + name + ";";// ORDER BY DATETIME ASC;";
    TrajParser trajectory(shader);
    trajectory.trajName = name;
    rc = sqlite3_exec(db, query.c_str(),trajSegCallback, &trajectory, &zErrMsg);
    //trajectories.push_back(curTrajDB);
     sqlite3_close_v2(db);
    return trajectory;
}

void TrajParser::Copy(const TrajParser &copy)
{
    vertexBufferObject = 0;
    weatherBufferObject = 0;
    vertexArrayObject = 0;
    speedArrayObject = 0;

    positions = copy.positions;
    tempColors = copy.tempColors;
    segList = copy.segList;
    clusterList = copy.clusterList;
    
    averageSpeed = copy.averageSpeed;
    trajName = copy.trajName;
    cluster = copy.cluster;
}

//TrajParser::~TrajParser(){
//    std::cout << "Hello there" << "\n";
//    
//    std::vector<glm::vec3>().swap(tempColors);
//    tempColors.shrink_to_fit();
//    std::vector<glm::vec3>().swap(positions);
//    positions.shrink_to_fit();
//    std::vector<TrajSeg>().swap(segList);
//    segList.shrink_to_fit();
//    
//}

std::vector<TrajParser> TrajParser::LoadLocalTrajectories(GeoPosition location, GLSLShader &shader)
{
    std::vector<TrajParser> trajectories;
    //could this be leaking memory?
    sqlite3 *db;
    std::string query;
    char *zErrMsg = 0;
    
    //this minLat maxLat stuff will be based on the limits of the map, at the current zoom level, at the current map size
    float latDelta = 1.0;//this will be more complex later
    float lonDelta = 1.0;
    
    int rc = sqlite3_open("trajectories.db", &db);

    std::vector<glm::vec2> corners;//2 cornerss
    
    corners = Map::Corners(location);
    
    //this doesnt work if we are between zones
    std::string minLat = std::to_string(corners[1].x);//should map lat to y and lon to x
    std::string maxLat = std::to_string(corners[0].x);
    
    std::string minLon = std::to_string(corners[0].y);
    std::string maxLon = std::to_string(corners[1].y);

    TrajParser::basePosition = TrajParser::latLonToMeters(location.lat, location.lon, Map::zoom);

    int x = Map::long2tilex(location.lon,Map::zoom);
    int y = Map::lat2tiley(location.lat, Map::zoom);
    double returnedLat = Map::tiley2lat(y, Map::zoom);
    double returnedLon = Map::tilex2long(x, Map::zoom);
    
    int posX = Map::long2tilex(returnedLon, Map::zoom);
    int posY = Map::lat2tiley(returnedLat, Map::zoom);
    
    TrajParser::SetScale(posX, posY, Map::zoom);
    
    
    //std::string minLat = std::to_string(location.lat-latDelta);
    //std::string maxLat = std::to_string(location.lat+latDelta);
    //std::string minLon = std::to_string(location.lon-lonDelta);
    //std::string maxLon = std::to_string(location.lon+lonDelta);
    
    //also need to change this I guess
    query = "SELECT DISTINCT TRAJECTORYNAME FROM TRAJSEG WHERE LATITUDE BETWEEN " + minLat + " AND " + maxLat + " AND LONGITUDE BETWEEN "
    + minLon + " AND " + maxLon + ";";
    //ALREADY FOUND : 20090424094722
    //ALREADY FOUND : 20090426061534

    std::vector<std::string> trajNames;
    //have to remember that sqlite exec will step over all the rows
    rc = sqlite3_exec(db, query.c_str(),trajListCallback, &trajNames, &zErrMsg);
    
    for(int i = 0; i < trajNames.size(); i++){
        //iterate over the trajectory names and get their segments
        query = "SELECT * FROM TRAJSEG WHERE TRAJECTORYNAME IS " + trajNames[i] + ";";// ORDER BY DATETIME ASC;";
        TrajParser curTrajDB(shader);
        curTrajDB.trajName = trajNames[i];
        rc = sqlite3_exec(db, query.c_str(),trajSegCallback, &curTrajDB, &zErrMsg);
        curTrajDB.SetupData();
        trajectories.push_back(curTrajDB);
    }
    sqlite3_close_v2(db);
    return trajectories;
}

//this should probably be refactored into a loadTrajectory function
//should I derive this class for the geolife stuff?
//TrajParser::TrajParser(std::string file,GLSLShader &shader) : myShader(shader)

//Now using this function to load new trajectories from files
void TrajParser::loadTrajectory(std::string file)
{
    std::ifstream trajFile;
    std::string line;
    std::stringstream lineStream;
    std::string token;
    
    TrajSeg auxSeg;
    glm::vec3 auxPosition;
    //glm::vec3 auxRefPoint;
    
    trajFile.open(file,std::ifstream::in);
    
    if(basePosition.x == 0 && basePosition.y == 0 && basePosition.z == 0){
        //????
    }
    int lineCount = 0;
    if(trajFile.is_open()){
        while (trajFile.good()) {
            getline(trajFile,line);
            //TODO - check if empty line
            //if(line[0] != '#'){ //changed for geolife
            if(lineCount > 5){
                //making changes to process geolife trajectories - also csv files, so not a lot of difference from what've been doing.
                //but first, gonna change here to load weather data - also gonna change the struct to hold weather data
                //also also, the weather data can be passed to the gpu as a vertex buffer object
                lineStream << line;
                lineStream >> auxSeg;
                
                if(basePosition.x == 0 && basePosition.y == 0 && basePosition.z == 0){
                    //base position is in mercator meters without scale
                    //set scale is based on tile position, based on lat/lon
                    
//                    basePosition = convertLatLon(auxSeg,glm::vec3(0,auxSeg.elevation,0));
//                    int posX = Map::long2tilex(auxSeg.lon, Map::zoom);
//                    int posY = Map::lat2tiley(auxSeg.lat, Map::zoom);
//                    SetScale(posX, posY, Map::zoom);

                    int x = Map::long2tilex(auxSeg.lon,Map::zoom);
                    int y = Map::lat2tiley(auxSeg.lat, Map::zoom);
                    
                    double returnedLat = Map::tiley2lat(y, Map::zoom);
                    double returnedLon = Map::tilex2long(x, Map::zoom);
                    
                    //TrajParser::basePosition = traj->latLonToMeters(returnedLat, returnedLon, 17);
                    TrajParser::basePosition = convertLatLon(auxSeg,glm::vec3(0,auxSeg.elevation,0));
                    
                    int posX = Map::long2tilex(returnedLon, Map::zoom);
                    int posY = Map::lat2tiley(returnedLat, Map::zoom);
                    
                    SetScale(posX, posY, Map::zoom);

                }
                
                //I mean, not really a position in xyz space, as we are dealing with gps coordinates
                //auxPosition = glm::vec3(auxSeg.lon,auxSeg.lat,auxSeg.elevation);
//                if(positions.size() == 0){
//                    auxRefPoint = convertLatLon(auxSeg,glm::vec3(0,auxSeg.elevation,0));
//                    auxPosition = convertLatLon(auxSeg,auxRefPoint);
//                }
//                else
//                    auxPosition = convertLatLon(auxSeg,auxRefPoint);
                
                auxPosition = convertLatLon(auxSeg,basePosition);
                
                positions.push_back(auxPosition);
                
                segList.push_back(auxSeg);
                lineStream.clear();
            }
            lineCount++;
        }
        
        SetSpeed();
        SetAverageSpeed();
        GetTrajWeatherData();
        SetupData();//TODO remove this
    }
}

//moved line processing into this overloaded input operator but I'm still not sure I fully understand what is going on here
//was having some weird stuff happening before when the function was in the main class, and the operator was working on a istream
std::stringstream &operator >> (std::stringstream &lineStream, TrajSeg &auxSeg)
{
    std::string token;
    std::string dateTime;
    int count = 0;
    
    //changing here for geolife
    while(getline(lineStream, token, ',')){
        switch (count) { //assuming order is always the same
            case 0:
                auxSeg.lat = atof(token.c_str());
                break;
            case 1:
                auxSeg.lon = atof(token.c_str());
                break;
            case 2: break;//always zero
            case 3:
                auxSeg.elevation = atof(token.c_str());
                break;
            case 4: break;// date in days, not using here
            case 5:
                auxSeg.timeStamp = token;
                break;
            case 6:
                token.pop_back();
                auxSeg.timeStamp = auxSeg.timeStamp + "T" + token + "Z";
            default:
                break;
        }
//        switch (count) { //assuming order is always the same
//            case 0:
//                auxSeg.elevation = atof(token.c_str());
//                break;
//            case 1:
//                auxSeg.timeStamp = token;
//                break;
//            case 2:
//                auxSeg.lat = atof(token.c_str());
//                break;
//            case 3:
//                auxSeg.lon = atof(token.c_str());
//                break;
//            default:
//                break;
//        }
        count++;
        
        
    }
    
    return lineStream;
}

//void GeolifeTrajectory::loadTrajectory(std::string file)
//{
//    std::ifstream trajFile;
//    std::string line;
//    std::stringstream lineStream;
//    std::string token;
//
//    TrajSeg auxSeg;
//    glm::vec3 auxPosition;
//    glm::vec3 auxRefPoint;
//
//    trajFile.open(file,std::ifstream::in);
//
//    if(basePosition.x == 0 && basePosition.y == 0 && basePosition.z == 0){
//        //????
//    }
//    int lineCount = 0;
//    if(trajFile.is_open()){
//        while (trajFile.good()) {
//            getline(trajFile,line);
//            if(lineCount > 5){
//                lineStream << line;
//                lineStream >> auxSeg;
//
//                if(basePosition.x == 0 && basePosition.y == 0 && basePosition.z == 0){
//                    basePosition = convertLatLon(auxSeg,glm::vec3(0,auxSeg.elevation,0));
//                }
//
//
//                auxPosition = convertLatLon(auxSeg,basePosition);
//
//                positions.push_back(auxPosition);
//
//                segList.push_back(auxSeg);
//                lineStream.clear();
//            }
//
//            lineCount++;
//        }
//
//        GetTrajWeatherData();
//        SetupData();
//
//    }
//}

//gonna copy the stuff from mapbox here
//08/07 - this is working, but not very robust? dependent on stuff like scale, ref position etc ? need to look into this again
glm::vec3 TrajParser::convertLatLon(TrajSeg &segment,glm::vec3 refPoint)
{
    //float scale = 1;
    float scale = TrajParser::relativeScale;
    float posx = segment.lon * originShift / 180;
    
    float posy = log(tan((90 + segment.lat) * M_PI / 360)) / (M_PI / 180);
    
    posy = posy * originShift /180;
    
    //return glm::vec3((posx -refPoint.x) * scale, (posy -refPoint.y) * scale, segment.elevation);//probably should not use elevation?
    
    //return glm::vec3((posx -refPoint.x) * scale, segment.elevation, (posy -refPoint.z) * scale);//probably should not use elevation?
    
    //wonder if I could not multiply by scale here and use a scale matrix
    return glm::vec3((posx -refPoint.x) * scale, 0.0f, (posy -refPoint.z) * scale);//probably should not use elevation?
}
//TODO - not using zoom here, remove it
glm::vec3 TrajParser::latLonToMeters(float lat, float lon, int zoom)
{
    
    float posx = lon * originShift /180;
    
    float posy = log(tan((90 + lat) * M_PI / 360)) / (M_PI / 180);
    posy = posy * originShift / 180;
    
    return glm::vec3(posx,0.0f,posy);
}

float TrajParser::resolution(int zoom)
{
    return initialResolution / pow(2, zoom);
}

glm::vec2 TrajParser::pixelsToMeters(glm::vec2 p, int zoom)
{
    float res = resolution(zoom);
    
    glm::vec2 met = glm::vec2(p.x * res - originShift,  -(p.y * res - originShift));
    
    return met;
}

//this sets how many meters does one world unit currently represents
void TrajParser::SetScale(int x, int y, int z)
{
    //reference tile size
    
    glm::vec2 min = pixelsToMeters(glm::vec2(x*512,y*512), z);
    glm::vec2 max = pixelsToMeters(glm::vec2((x+1)*512,(y+1)*512), z);
    
    glm::vec2 size = abs(max-min);
    TrajParser::relativeScale = 200/size.x;
}

//TODO - this function should be moved out of here
void TrajParser::ResetScale(double lat, double lon, std::vector<TrajParser> *trajectories)
{
    int x = Map::long2tilex(lon,Map::zoom);
    int y = Map::lat2tiley(lat, Map::zoom);

    double returnedLat = Map::tiley2lat(y, Map::zoom);
    double returnedLon = Map::tilex2long(x, Map::zoom);

    int posX = Map::long2tilex(returnedLon, Map::zoom);
    int posY = Map::lat2tiley(returnedLat, Map::zoom);

    SetScale(posX, posY, Map::zoom);

    //need to rebind buffer data - not gonna be fast
    for(auto &curTraj : *trajectories){
        for(int i = 0; i < curTraj.positions.size(); i++){
            curTraj.positions[i] = TrajParser::convertLatLon(curTraj.segList[i], TrajParser::basePosition);
        }
    }
}

void TrajParser::ResetPositions(double lat, double lon, std::vector<TrajParser> *trajectories)
{
    //gonna need to load and unload trajectories by tile
    //just kinda assume that it wont be null or empty

    //this is not using the trajectory as a base position, just a dumb way of using the method on a static function
    TrajParser::basePosition = trajectories->at(0).latLonToMeters(lat, lon, Map::zoom);
    
    //TODO - replace with std algorithm like transform
    for(auto &curTraj : *trajectories){
        for(int i = 0; i < curTraj.positions.size(); i++){
            curTraj.positions[i] = TrajParser::convertLatLon(curTraj.segList[i], TrajParser::basePosition);
        }
    }

}

float TrajParser::simpleDistance(glm::vec2 pos1, glm::vec2 pos2)
{
	//this first calculation is not very precise, so will use the other one
    //double distlat = 12430 * (abs(pos1.y - pos2.y)/180);
    //double distlon = 24901 * (abs(pos1.x - pos2.x)/360) * cos((pos1.y+pos2.y)/2 * (M_PI/180));
    //double distance = sqrt(pow(distlat, 2) + pow(distlon,2));
	//double distInMeters = firstDistance * 1000;

	//changing distance calculation to something a bit different for a test
	//basically the same thing as the one from mapbox described on their blog

    //need to document better this sort of thing
	double cos1 = cos(pos1.y * M_PI / 180);
	double cos2 = 2 * cos1 * cos1 - 1;
	double cos3 = 2 * cos1 * cos2 - cos1;
	double cos4 = 2 * cos1 * cos3 - cos2;
	double cos5 = 2 * cos1 * cos4 - cos3;

	//this assumes km but we are returning in meters
	
	double K1 = (111.13209 - 0.56605 * cos2 + 0.0012 * cos4);
	double K2 = (111.41513 * cos1 - 0.09455 * cos3 + 0.00012 * cos5);

	double distance = sqrt( pow(K1*(pos1.y - pos2.y), 2) + pow(K2*(pos1.x - pos2.x), 2) );

	double distInMeters = distance * 1000;
	
	return (float)distInMeters;
	//return (float)distance;
}

float TrajParser::timeDelta(const TrajSeg &pos1, const TrajSeg &pos2)
{
	std::time_t time1;
	std::time_t time2;

	std::tm date1;
    std::tm date2;
    
	std::istringstream ss1(pos1.timeStamp);
	//ss1 >> std::get_time(&date1, "%Y-%m-%dT%T%z");
    ss1 >> std::get_time(&date1, "%Y-%m-%dT%H:%M:%S%z");
    date1.tm_isdst = 0;
	time1 = mktime(&date1);

	std::istringstream ss2(pos2.timeStamp);
	//ss2 >> std::get_time(&date2, "%Y-%m-%dT%T%z");
    //ss2 >> std::get_time(&date2, "%Y-%m-%dT%T%z");
    ss2 >> std::get_time(&date2, "%Y-%m-%dT%H:%M:%S%z");
    date2.tm_isdst = 0;
	time2 = mktime(&date2);

	double timeSeconds = difftime(time2, time1);// does order matter? 
	return (float)timeSeconds; 
}

float TrajParser::getInstantSpeed(const TrajSeg &seg1, const TrajSeg &seg2)
{
	//we get the distance in meters from both points AND the time it took for the distance to be covered
	//should be pretty simple. speed will then be in meters per second. can convert to kmh.

	glm::vec2 pos1 = glm::vec2(seg1.lon, seg1.lat);
	glm::vec2 pos2 = glm::vec2(seg2.lon, seg2.lat);
	float distanceInMeters = TrajParser::simpleDistance(pos1, pos2);

	float timeDifference = TrajParser::timeDelta(seg1, seg2);
    
    if(fabs(timeDifference-0) <= FLT_EPSILON){//cant have infinity speed
        return 0.0;
    }
    
	float speedInMetersPerSecond = distanceInMeters / timeDifference;

	float speedInKMH = speedInMetersPerSecond * 3.6;

	return speedInKMH;
}

//tile bounds starts at tileid * tilesize and goes to tileid+1 * tilesize
//the part used is the size, which is
//WHY ARE WE CALLING THIS???
void TrajParser::SetupData()
{
    //this works but we should look into abstracting away from the trajectory code the opengl stuff
    
    //when changing the buffer data should probably not regen
    if(!glIsBuffer(this->vertexBufferObject))
        glGenBuffers(1, &this->vertexBufferObject);
    
    glBindBuffer(GL_ARRAY_BUFFER, this->vertexBufferObject);
    //not sure if should use glm data pointer or vector data pointer
    glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(glm::vec3), NULL, GL_DYNAMIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(glm::vec3), (float *)glm::value_ptr(positions.front()), GL_DYNAMIC_DRAW);
    
    if(!glIsBuffer(weatherBufferObject))
        glGenBuffers(1, &weatherBufferObject);
    
    glBindBuffer(GL_ARRAY_BUFFER, weatherBufferObject);
    glBufferData(GL_ARRAY_BUFFER, tempColors.size() * sizeof(glm::vec3), NULL, GL_DYNAMIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, tempColors.size() * sizeof(glm::vec3), (float *)glm::value_ptr(tempColors.front()), GL_DYNAMIC_DRAW);

    //Cant believe theres no better way to do this in c++
    std::vector<float> speeds;
    for(auto &seg : segList){
        speeds.push_back(seg.speed);
    }
    
    if(!glIsBuffer(speedArrayObject))
        glGenBuffers(1, &speedArrayObject);
    
    glBindBuffer(GL_ARRAY_BUFFER, speedArrayObject);
    glBufferData(GL_ARRAY_BUFFER, speeds.size() * sizeof(float), NULL, GL_DYNAMIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, speeds.size() * sizeof(float), speeds.data(), GL_DYNAMIC_DRAW);
    
    if(!glIsVertexArray(vertexArrayObject))
        glGenVertexArrays(1, &vertexArrayObject);
    
    glBindVertexArray(vertexArrayObject);
    
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0 , 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, weatherBufferObject);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, speedArrayObject);
    glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, 0);
    
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
}

void TrajParser::GetTrajWeatherData()
{
    glm::vec3 curPoint = glm::vec3(1.0,0.0,0.0);
    
    //only getting the data from the first point for now -- see stuf below
    segList[0].segWeather = Weather::getWeather(&segList[0]);
    curPoint = Weather::getWeatherColor(segList[0].segWeather.temperature);
    
    for(int i = 0; i < segList.size(); i++){
        tempColors.push_back(curPoint);
        segList[i].segWeather = segList[0].segWeather;
    }
    
}

void TrajParser::SetSpeed()
{
    for(int i = 0; i < segList.size(); i++){
        if(i == segList.size() -1){
            segList[i].speed = 0.0;
        }
        else{
            segList[i].speed = TrajParser::getInstantSpeed(segList[i], segList[i+1]);
        }
    }
}

//could do this inside the other function
void TrajParser::SetAverageSpeed()
{
    averageSpeed = 0;
    for(auto &seg : segList){
        averageSpeed += seg.speed;
    }
    
    averageSpeed /= segList.size();
}

//in time do a binary search of the trajectory
//get first point
//get last point
//if difference is greater than x
//divide and do it again recursively

//wont actually need this I think
float * TrajParser::getWeatherVector()
{
    float * weatherArray = new float[segList.size()];
    
    for(int i = 0; i < segList.size(); i++){
        weatherArray[i] = segList[i].segWeather.temperature;
    }
    
    return weatherArray;
}

glm::mat4 TrajParser::SetTrajMatrix(float lat,float lon)
{
    float posX = Map::long2tilexpx(lat, Map::zoom);
    float posY = Map::lat2tileypx(lon, Map::zoom);
    
    float translatedX = (posX *200) -100;
    float translatedY = (posY *200) -100;

    glm::mat4 trajMatrix = glm::mat4(1.0);
    
    trajMatrix = glm::translate(trajMatrix, glm::vec3(translatedX,5,translatedY));
    trajMatrix = glm::rotate<float>(trajMatrix, -M_PI, glm::vec3(1.0,0.0,0.0));
    
    return trajMatrix;
}

std::vector<TrajParser> TrajParser::FilterTrajectories(std::string attribute, std::string minValue, std::string maxValue,GLSLShader &shader)
{
    std::vector<TrajParser> trajectories;
    sqlite3 *db;
    std::string query;
    char *zErrMsg = 0;
    
    //this minLat maxLat stuff will be based on the limits of the map, at the current zoom level, at the current map size
    float latDelta = 1.0;//this will be more complex later
    float lonDelta = 1.0;
    
    int rc = sqlite3_open("trajectories.db", &db);
    
    std::vector<glm::vec2> corners;//2 cornerss
    GeoPosition location{Map::lat,Map::lon};
    corners = Map::Corners(location);
    
    //this doesnt work if we are between zones
    std::string minLat = std::to_string(corners[1].x);//should map lat to y and lon to x
    std::string maxLat = std::to_string(corners[0].x);
    
    std::string minLon = std::to_string(corners[0].y);
    std::string maxLon = std::to_string(corners[1].y);
    
    
    query = "SELECT DISTINCT TRAJECTORYNAME FROM TRAJSEG WHERE LATITUDE BETWEEN " + minLat + " AND " + maxLat + " AND LONGITUDE BETWEEN "
    + minLon + " AND " + maxLon + + " AND " + attribute + " BETWEEN " + minValue + " AND " + maxValue + ";";
    
    std::vector<std::string> trajNames;
    //have to remember that sqlite exec will step over all the rows
    rc = sqlite3_exec(db, query.c_str(),trajListCallback, &trajNames, &zErrMsg);
    
    for(int i = 0; i < trajNames.size(); i++){
        //iterate over the trajectory names and get their segments
        query = "SELECT * FROM TRAJSEG WHERE TRAJECTORYNAME IS " + trajNames[i] + ";";// ORDER BY DATETIME ASC;";
        TrajParser curTrajDB(shader);
        rc = sqlite3_exec(db, query.c_str(),trajSegCallback, &curTrajDB, &zErrMsg);
        curTrajDB.SetupData();
        trajectories.push_back(curTrajDB);
    }
    sqlite3_close_v2(db);
    return trajectories;
}

//refactor in having most things on the same function and the function calls just build the query
std::vector<TrajParser> TrajParser::FilterByTime(std::string minValue, std::string maxValue,GLSLShader &shader)
{
    //time filtering is a bit different
    
    std::vector<TrajParser> trajectories;
    sqlite3 *db;
    std::string query;
    char *zErrMsg = 0;
    
    //this minLat maxLat stuff will be based on the limits of the map, at the current zoom level, at the current map size
    float latDelta = 1.0;//this will be more complex later
    float lonDelta = 1.0;
    
    int rc = sqlite3_open("trajectories.db", &db);
    
    std::vector<glm::vec2> corners;//2 cornerss
    GeoPosition location{Map::lat,Map::lon};
    corners = Map::Corners(location);
    
    //this doesnt work if we are between zones
    std::string minLat = std::to_string(corners[1].x);//should map lat to y and lon to x
    std::string maxLat = std::to_string(corners[0].x);
    
    std::string minLon = std::to_string(corners[0].y);
    std::string maxLon = std::to_string(corners[1].y);
    
    //select trajectoryname,substr(datetime,12,2) as time from trajseg where time = "00";
    //select count(*) from (select trajectoryname,substr(datetime,12,2) as time from trajseg where time between "00" and "05");
    query = "SELECT DISTINCT TRAJECTORYNAME FROM (SELECT TRAJECTORYNAME,SUBSTR(DATETIME,12,2) AS TIME FROM TRAJSEG WHERE TIME BETWEEN";
    query += "\"" + minValue  + "\" AND  \"" + maxValue + "\"";
    query += " AND LATITUDE BETWEEN " + minLat + " AND " + maxLat + " AND LONGITUDE BETWEEN " + minLon + " AND " + maxLon + ");";
    
    std::vector<std::string> trajNames;
    //have to remember that sqlite exec will step over all the rows
    rc = sqlite3_exec(db, query.c_str(),trajListCallback, &trajNames, &zErrMsg);
    
    for(int i = 0; i < trajNames.size(); i++){
        //iterate over the trajectory names and get their segments
        query = "SELECT * FROM TRAJSEG WHERE TRAJECTORYNAME IS " + trajNames[i] + ";";// ORDER BY DATETIME ASC;";
        TrajParser curTrajDB(shader);
        rc = sqlite3_exec(db, query.c_str(),trajSegCallback, &curTrajDB, &zErrMsg);
        curTrajDB.SetupData();
        trajectories.push_back(curTrajDB);
    }
    sqlite3_close_v2(db);
    return trajectories;
}

//again, should refactor this
std::vector<TrajParser> TrajParser::FilterByDate(std::string minValue, std::string maxValue,GLSLShader &shader)
{
    std::vector<TrajParser> trajectories;
    sqlite3 *db;
    std::string query;
    char *zErrMsg = 0;
    
    //this minLat maxLat stuff will be based on the limits of the map, at the current zoom level, at the current map size
    float latDelta = 1.0;//this will be more complex later
    float lonDelta = 1.0;
    
    int rc = sqlite3_open("trajectories.db", &db);
    
    std::vector<glm::vec2> corners;//2 cornerss
    GeoPosition location{Map::lat,Map::lon};
    corners = Map::Corners(location);
    
    //this doesnt work if we are between zones
    std::string minLat = std::to_string(corners[1].x);//should map lat to y and lon to x
    std::string maxLat = std::to_string(corners[0].x);
    
    std::string minLon = std::to_string(corners[0].y);
    std::string maxLon = std::to_string(corners[1].y);
    
    //select distinct trajectoryname from (select trajectoryname,substr(datetime,0,5)||substr(datetime,6,2)||substr(datetime,9,2) as date from trajseg) where date >="20001225" and date <= "2010010101";

    query = "SELECT DISTINCT TRAJECTORYNAME FROM (SELECT TRAJECTORYNAME,substr(datetime,0,5)||substr(datetime,6,2)||substr(datetime,9,2) as date from trajseg WHERE LATITUDE BETWEEN " +minLat+ " AND " +maxLat+ " AND LONGITUDE BETWEEN " +minLon+ " AND " +maxLon+ ")" ;
    query += " WHERE DATE >= \"" + minValue + "\" AND DATE <= \""  + maxValue + "\";";
    
    std::vector<std::string> trajNames;
    //have to remember that sqlite exec will step over all the rows
    rc = sqlite3_exec(db, query.c_str(),trajListCallback, &trajNames, &zErrMsg);
    
    for(int i = 0; i < trajNames.size(); i++){
        //iterate over the trajectory names and get their segments
        query = "SELECT * FROM TRAJSEG WHERE TRAJECTORYNAME IS " + trajNames[i] + ";";// ORDER BY DATETIME ASC;";
        TrajParser curTrajDB(shader);
        rc = sqlite3_exec(db, query.c_str(),trajSegCallback, &curTrajDB, &zErrMsg);
        curTrajDB.SetupData();
        trajectories.push_back(curTrajDB);
    }
    sqlite3_close_v2(db);
    return trajectories;
}
//just playing around with some stuff
void TrajParser::Render()
{
    
}

void TrajParser::initializeShader()
{
    
}
