#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
//now also using glew on mac, but I'm not sure about declaring it static? will clean up this later
#ifdef __APPLE__
    #define GL_SILENCE_DEPRECATION
    //#include <OpenGL/gl3.h>
    //#include <OpenGL/gl3ext.h>
    #define GLEW_STATIC
    #include <GL/glew.h>
#else// include static GLEW and new version of GL on Windows
    #define GLEW_STATIC
    #include <GL/glew.h>
#endif
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "gl_utils.h"
#include <GLFW/glfw3.h> // GLFW helper library
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <stdarg.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include "Camera.h"
#include "TrajParser.h"
#include "Weather.h"
#include <random>
#include "time.h"
#include <iostream>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "GLSLShader.h"
#include <string>
#include <curl/curl.h>
#include <sqlite3.h>
#include "Map.hpp"
#include "Renderer.h"
#include "UI.h"
#include "TrajectoryManager.h"
#include "Framebuffer.h"
#include "trajcomp/trajcomp.hpp"
#include "ClusterManager.hpp"

#define GL_LOG_FILE "gl.log"
#define NUM_WAVES 5

#define FILTER_TEMPERATURE 1
#define FILTER_SPEED 2
#define FILTER_TIME 4
#define FILTER_DATE 8
#define FILTER_CLUSTER 16
#define FILTER_MODE 32
#define TRANSPORT_MODE 8191 //all transport modes start selected
// keep track of window size for things like the viewport and the mouse cursor
int g_gl_width = 1280;
int g_gl_height = 720;

GLFWwindow* g_window = NULL;

Camera camera;

GLfloat lastx = g_gl_width/2, lasty = g_gl_height/2;
bool firstMouse = true;

float Tile::tileScale;
bool TrajectoryManager::renderRepresent = false;

float maxDistance = 5.0;

struct shader_traj_point
{
    float lat;
    float lon;
    float ele;
    float temp;
};


//HOW I CAN SEE THIS WHOLE THING COMING TOGETHER AS A COESIVE MODEL
//LOAD EACH TRAJECTORY FROM FILE (CSV, GPX, ETC)
//LOAD TRAJ DATA INTO TRAJ STRUCT
//LOOK UP AUX DATA - IE WEATHER INFO
//ADD IT TO TRAJ STRUCTURE ON A POINT OR WHOLE TRAJ BASE
//THE STRUCT CONTAINS THE SHADER
//ON RENDERING, ONE TRAJECTORY EQUALS ONE DRAW CALL
//NOT VERY EFFICIENT
//BUT MORE FLEXIBLE THAN ONE HUGE BUFFER
//SCREEN SPACE DEFERRED STUFF MIGHT BE BETTER FOR OPTIMIZATION
//ALSO NEED TO FIGURE OUT MAP RENDERING/MATCHING


//should separate input and camera modification stuff into classes
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{

    //what is happening is that when we use mouse look the camerafront vector is changed and that changes the camera position
    //in ways that make it not be in what we assumed to be world space anymore
    //need to store some world position or the view matrix
    if (firstMouse)
    {
        lastx = xpos;
        lasty = ypos;
        firstMouse = false;
    }
    
    GLfloat xoffset = xpos - lastx;
    GLfloat yoffset = lasty - ypos; // Reversed since y-coordinates range from bottom to top
    lastx = xpos;
    lasty = ypos;
    
    GLfloat sensitivity = 0.05f;
    xoffset *= sensitivity;
    yoffset *= sensitivity;
    
    camera.pitch += yoffset;
    camera.yaw += xoffset; //trying to change the way the camera works without a lot of changes - might not be good
    //forcing the yaw stops rotating, for now thats what we want -
    //but dont know if something weird might happen when we have trajectories being multiplied by the view matrix
    //so keeping this for now, so we can rotate in weird ways
    
    if (camera.pitch > 89.0f)
        camera.pitch = 89.0f;
    if (camera.pitch < -89.0f)
        camera.pitch = -89.0f;
    
    glm::vec3 front;
    front.x = cos(glm::radians(camera.pitch)) * cos(glm::radians(camera.yaw));
    front.y = sin(glm::radians(camera.pitch));
    front.z = cos(glm::radians(camera.pitch)) * sin(glm::radians(camera.yaw));
    camera.cameraFront = glm::normalize(front);
    
    //updating up camera - should keep right vector in camera class
    glm::vec3 right = glm::normalize(glm::cross(camera.cameraFront,glm::vec3(0.0,1.0,0.0)));
    camera.cameraUp = glm::normalize(glm::cross(right,camera.cameraFront));
}

//this is not being used
void Zoom(int direction, Map *map, GLSLShader &mapShader, std::vector<TrajParser> *trajectories)
{
    Map::zoom += direction;
    map->curZoom = Map::zoom;
    
    map->FillMapTiles();
    
    mapShader.Use();
    glUniform1f(mapShader("curZoom"), Map::zoom);
    mapShader.UnUse();
    
    TrajParser::ResetScale(Map::lat, Map::lon, trajectories);
    
    //really need to put this in a function
    //float posX = Map::long2tilexpx(startPos.x, Map::zoom);
    //float posY = Map::lat2tileypx(startPos.z, Map::zoom);
    
    //will this even make sense?
    //float posX = Map::long2tilexpx(camera.cameraPosition.x, Map::zoom);
    //float posY = Map::lat2tileypx(camera.cameraPosition.z, Map::zoom);
    
//    float translatedX = (posX *200) -100;
//    float translatedY = (posY *200) -100;
    
    //trajMatrix = glm::mat4(1.0);
    //trajMatrix = glm::translate(trajMatrix, glm::vec3(translatedX,ytrans,translatedY));
    //trajMatrix = glm::rotate<float>(trajMatrix, -M_PI, glm::vec3(1.0,0.0,0.0));
    
    //trajMatrix = TrajParser::SetTrajMatrix(Map::lat, Map::lon);
    Tile::tileScale = Tile::recalculateScale(Map::lat, Map::zoom);
}

void ZoomIn(Camera *cam)
{
    cam->cameraPosition.y -= 100;
//    Zoom(+1);
}

void ZoomOut(Camera *cam)
{
    cam->cameraPosition.y += 100;
//    Zoom(-1);
}

void Pan(Direction panDirection,Camera *cam,Map *curMap, std::vector<TrajParser> *trajectories, glm::mat4 &TrajMatrix, ClusterManager *clustManager)
{
    glm::vec3 directionLateral = glm::normalize(glm::cross(glm::vec3(0.0,-1.0,0.0), glm::vec3(0.0,0.0,-1.0)));
    glm::vec3 directionVertical = glm::normalize(glm::cross(glm::vec3(0.0,-1.0,0.0), glm::vec3(-1.0,0.0,0.0)));
    
//we are not going to use the cameraPosition for panning the map, but keeping it here for compability
    switch (panDirection) {
        case Direction::East:
            //cam->cameraPosition -= directionLateral * cam->cameraSpeed;
            cam->cameraPosition.x -= 200;
            curMap->LoadEast();
            break;
        case Direction::West:
            //cam->cameraPosition -= directionLateral * cam->cameraSpeed;
            cam->cameraPosition.x += 200;
            curMap->LoadWest();
            break;
        case Direction::North:
            //cam->cameraPosition += directionVertical * cam->cameraSpeed;
            cam->cameraPosition.z -= 200;
            curMap->LoadNorth();
            break;
        case Direction::South:
            //cam->cameraPosition -= directionVertical * cam->cameraSpeed;
            cam->cameraPosition.z += 200;
            curMap->LoadSouth();
            break;
        default:
            break;
    }
    
    //float *mat = glm::value_ptr(*trajMat);
    
    
    //first we remove the baseposition offset- this is NOT the best way of doing this
    clustManager->RemoveBasePosition(TrajParser::basePosition,TrajParser::relativeScale);
    
    if(trajectories->size() >0)
        TrajParser::ResetPositions(Map::lat, Map::lon,trajectories);
    
    //now we add the new base position offset, because baseposition was changed in ResetPositions
    clustManager->AddBasePosition(TrajParser::basePosition, TrajParser::relativeScale);
    
    glm::mat4 trajmat = TrajParser::SetTrajMatrix(Map::lon, Map::lat);
    
    TrajMatrix = trajmat;
    //float *mat = glm::value_ptr(newTrajMat);
    
    //trajMat = mat;
    //trajMat->
}

//need to look again into the camera system
void processs_keyboard(GLFWwindow *window, Camera *cam,Map *map, TrajectoryManager *trajManager, glm::mat4 &trajMatrix, GLSLShader &shader, ClusterManager *clustManager)
{
    if (GLFW_PRESS == glfwGetKey (window, GLFW_KEY_ESCAPE)) {
        glfwSetWindowShouldClose (window, 1);
    }
    
    if (GLFW_PRESS == glfwGetKey (window, GLFW_KEY_1)) {
        maxDistance -= 2;
        maxDistance < 0 ? maxDistance = 0 : maxDistance = maxDistance;
    }
    if (GLFW_PRESS == glfwGetKey (window, GLFW_KEY_2)) {
        maxDistance += 2;
    }
    
    //multiplying by the front vector changes the position in weird ways
    //this is a perfectly fine camera movement system but I think for sliding along the map I might need something else
    
    //glm::vec3 directionLateral = glm::normalize(glm::cross(glm::vec3(0.0,-1.0,0.0), glm::vec3(0.0,0.0,-1.0)));
    //glm::vec3 directionVertical = glm::normalize(glm::cross(glm::vec3(0.0,-1.0,0.0), glm::vec3(-1.0,0.0,0.0)));
    
    //glm::vec3 right = glm::normalize(glm::cross(cam->cameraFront, cam->cameraUp));
    //keeping old camera movement commented out - also added the direction vectors, so that should be a bit faster
    std::vector<TrajParser> *trajectories = &trajManager->trajlist;
    std::vector<TrajParser> * tempVector = new std::vector<TrajParser>;
    
    //calling setupdata eveywere is kinda ugly
    //TODO - it should be possible to avoid calling setup data every time we move by moving calculations to the shader
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_W)){
        //cam->cameraPosition += cam->cameraSpeed * cam->cameraFront;
        //cam->cameraPosition += directionVertical * cam->cameraSpeed;
       
        *tempVector = TrajParser::LoadRow(shader,1,trajectories);
        trajectories->insert(trajectories->end(),tempVector->begin(),tempVector->end() );
        if(tempVector != NULL && tempVector->size() >0){
            std::sort(trajectories->begin(), trajectories->end(),TrajParser::CompareTrajectories);
        }
         Pan(Direction::North, cam,map, trajectories,trajMatrix,clustManager);
        trajManager->SetupData();
        clustManager->SetupClusterData();
    }
    
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_S)){
        //cam->cameraPosition -= cam->cameraSpeed * cam->cameraFront;
        //cam->cameraPosition -= directionVertical * cam->cameraSpeed;
        
        *tempVector = TrajParser::LoadRow(shader,-1,trajectories);
        trajectories->insert(trajectories->end(),tempVector->begin(),tempVector->end() );
        if(tempVector != NULL && tempVector->size() >0){
            std::sort(trajectories->begin(), trajectories->end(),TrajParser::CompareTrajectories);
        }
        Pan(Direction::South, cam,map, trajectories,trajMatrix,clustManager);
        trajManager->SetupData();
        clustManager->SetupClusterData();
    }
    
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_A)){
        //cam->cameraPosition -= directionLateral * cam->cameraSpeed;
        
        *tempVector = TrajParser::LoadColumn(shader,-1,trajectories);
        trajectories->insert(trajectories->end(),tempVector->begin(),tempVector->end() );
        if(tempVector != NULL && tempVector->size() >0){
            std::sort(trajectories->begin(), trajectories->end(),TrajParser::CompareTrajectories);
        }
        Pan(Direction::West, cam,map, trajectories,trajMatrix,clustManager);
        trajManager->SetupData();
        clustManager->SetupClusterData();
    }
    
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_D)){
        //cam->cameraPosition += directionLateral * cam->cameraSpeed;
        
        *tempVector = TrajParser::LoadColumn(shader,1,trajectories);
        trajectories->insert(trajectories->end(),tempVector->begin(),tempVector->end() );
        if(tempVector != NULL && tempVector->size() >0){
            std::sort(trajectories->begin(), trajectories->end(),TrajParser::CompareTrajectories);
        }
        Pan(Direction::East, cam,map, trajectories,trajMatrix,clustManager);
        trajManager->SetupData();
        clustManager->SetupClusterData();
    }
    
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_Z)){
        //TODO - technically when we zoom in we should not need this, only added to "catch" missing trajectories
        *tempVector = TrajParser::LoadZoom(shader,trajectories);
        trajectories->insert(trajectories->end(),tempVector->begin(),tempVector->end() );
        if(tempVector != NULL && tempVector->size() >0){
            std::sort(trajectories->begin(), trajectories->end(),TrajParser::CompareTrajectories);
        }
         ZoomIn(cam);
    }
    
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_X)){
        
        *tempVector = TrajParser::LoadZoom(shader,trajectories);
        trajectories->insert(trajectories->end(),tempVector->begin(),tempVector->end() );
        if(tempVector != NULL && tempVector->size() >0){
            std::sort(trajectories->begin(), trajectories->end(),TrajParser::CompareTrajectories);
        }
        ZoomOut(cam);
    }
    
    delete tempVector;
}

static void FilterBySelection(std::string attribute,std::string minValue,std::string maxValue,GLSLShader &shader,std::vector<TrajParser> *trajectories)
{
    trajectories->clear();
    *trajectories = TrajParser::FilterTrajectories(attribute, minValue, maxValue, shader);
}

static void FilterByTime(std::string minValue,std::string maxValue,GLSLShader &shader,std::vector<TrajParser> *trajectories)
{
    trajectories->clear();
    *trajectories = TrajParser::FilterByTime(minValue, maxValue, shader);
}

static void FilterByDate(std::string minValue,std::string maxValue,GLSLShader &shader,std::vector<TrajParser> *trajectories)
{
    trajectories->clear();
    *trajectories = TrajParser::FilterByDate(minValue, maxValue, shader);
}

float cameraDistance(Camera *cam)
{
    //just vertical distance for zoom
    return abs(cam->cameraPosition.y - TrajParser::basePosition.y);
    
}

static void SetZoomLevel(TrajectoryManager *trajManager, ClusterManager *clustManager, float curDistance, GLSLShader &mapShader, Map &myMap, float &ratio, const glm::vec3 &startPos, glm::mat4 &trajMatrix, int &zoom) {
    
    //TODO - since we changed the vertical movement to be discrete all this ratio calculation is unnecessary, change to something simpler
    ratio = 1-(round(curDistance)/1000);
    int newZoom = int(round((10*ratio) + 9));//was using floor here, not sure why, caused precision error so changed to round
    newZoom > 19 ? newZoom = 19 : newZoom = newZoom; //changing here for a max of 15 until we deal with the height issue
    newZoom < 0 ? newZoom = 0 : newZoom = newZoom;
    
    //zoom = (int)floor(5000 * ratio);
    if(newZoom != zoom){
        //myMap.GetMapData(-30.057637, -51.171501,newZoom);
        zoom = newZoom;
        myMap.curZoom = zoom;
        Map::zoom = zoom;
        myMap.FillMapTiles();
        mapShader.Use();
        glUniform1f(mapShader("curZoom"), Map::zoom);
        mapShader.UnUse();
        
        clustManager->RemoveBasePosition(TrajParser::basePosition, TrajParser::relativeScale);
        
        std::vector<TrajParser> &TrajList = trajManager->trajlist;
        TrajParser::ResetScale(Map::lat, Map::lon, &TrajList);
        trajManager->SetupData();
        
        clustManager->AddBasePosition(TrajParser::basePosition, TrajParser::relativeScale);
        clustManager->SetupClusterData();
        
        //really need to put this in a function
        float posX = Map::long2tilexpx(startPos.x, Map::zoom);
        float posY = Map::lat2tileypx(startPos.z, Map::zoom);
        
        //will this even make sense?
        //float posX = Map::long2tilexpx(camera.cameraPosition.x, Map::zoom);
        //float posY = Map::lat2tileypx(camera.cameraPosition.z, Map::zoom);
        
        float translatedX = (posX *200) -100;
        float translatedY = (posY *200) -100;
        
        //trajMatrix = glm::mat4(1.0);
        //trajMatrix = glm::translate(trajMatrix, glm::vec3(translatedX,ytrans,translatedY));
        //trajMatrix = glm::rotate<float>(trajMatrix, -M_PI, glm::vec3(1.0,0.0,0.0));
        
        trajMatrix = TrajParser::SetTrajMatrix(Map::lon, Map::lat);
        Tile::tileScale = Tile::recalculateScale(Map::lat, Map::zoom);
        
        //TrajParser::ResetPositions(startPos.z, startPos.x, &TrajList);
    }
}

//pipeline (in a way) for loading (and re loading trajectories and the map)
//get the center -- get number of tiles -- get upper left, upper right, lower left, lower right corners (have to see what is needed)
//adjust search query accordingly
//changed the center - get new center -- change trajectory model matrix

void ClusterUI(TrajectoryManager &trajManager, ClusterManager &clusterManager,float (*minMaxCurrentFilters)[6][2],bool &filterClustering, int epsilon,int minLines,int scale,unsigned int flag, unsigned int modeFlag)
{
    ImGui::Checkbox("Use filter attributes \nas clustering parameters", &filterClustering);//\n gets recognized even without space
    ImGui::NewLine();
    if(ImGui::Button("Cluster") && (!trajManager.isClustered || trajManager.canClusterAgain)){
        std::cout << "Starting Clustering \n";
        std::map<int,std::vector<std::vector<double>>> newClusters;//is this going to copy?

        if(filterClustering){
            //prepare parameters
            //right now we get EVERYTHING that is loaded
            //even things that are outside (way outside) the screen
            //this filter option does not directly adresses that
            //also I think its better to get the trajs from what is already
            //loaded instead of going to the db once again
            //BUT who knows, it may be faster
            //think we need to figure out how to pass the area as well
             newClusters = trajManager.ClusterTrajectories(minMaxCurrentFilters,epsilon,minLines,scale,flag,modeFlag);
            
        }else{
            newClusters = trajManager.ClusterTrajectories(nullptr,epsilon,minLines,scale);
        }
        
        if(trajManager.isClustered){
            clusterManager.representativeTrajectories.insert(newClusters.begin(),newClusters.end());
        }
        else{
            clusterManager.representativeTrajectories = newClusters;
        }
        clusterManager.SetupClusterData();
    }
}

int main (int argc, char** argv) {
	assert (restart_gl_log ());
/*------------------------------start GL context------------------------------*/
	assert (start_gl ());

    
    //-f and filename for loading new trajectories from PLT files
    //-l and filename for loading the trasportation type labels
    
    int c;
    char *argument;
    while ((c = getopt(argc, argv, "f:l:")) != -1) {
        switch(c){
            case 'f' : {
                GLSLShader dummyShader;
                //TODO Refactor LoadTrajDescription to not need shader and not return vector, move function to TrajManager
                std::vector<TrajParser> TrajList = TrajParser::LoadTrajDescription(optarg,dummyShader);
                return 0;
            }
            case 'l' :
                TrajectoryManager::ParseGeolifeLabels(optarg);
                return 0;
        }
    }
    
	camera.cameraPosition = TrajParser::basePosition;
    //camera.cameraPosition.z = 100;
    
    //a couple of things that need to be done regarding the camera
    //we need to map the camera position to the position of the map in lat/lon
    //this is relatively easy, but always centering the map on the exact point is harder
    //and the map and the trajectories need to be in the same system
    
    //camera.cameraPosition = glm::vec3(0.0,0.0,0.0);
    
    camera.cameraPosition.y = 1000;
    camera.cameraFront = glm::vec3(0.0, 0.0, -1.0);
    camera.cameraUp = glm::vec3(0.0, 1.0, 0.0);    
    camera.cameraSpeed = 500;
    camera.pitch = 0.0;
    camera.yaw = -90.0;
    
    //for now using the first pass shader as the trajectory rendering one
    
    //GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER
    
    UI newUi;

    //for this deferred rendering test we will render the map to texture and then access the texture data on the trajectory shader
    //but only use it as a background for rendering
    //actually just remembered that this doesnt even make 
    //one thing I think could be interesting is adding to the shader class some form of reference to the textures used by that shader?
    
    GLSLShader trajectoryShader;
    trajectoryShader.LoadFromFile(GL_VERTEX_SHADER, "vert.glsl");
    trajectoryShader.LoadFromFile(GL_FRAGMENT_SHADER, "frag.glsl");
    trajectoryShader.LoadFromFile(GL_GEOMETRY_SHADER, "geom.glsl");
    trajectoryShader.CreateAndLinkProgram();
    trajectoryShader.Use();
    trajectoryShader.AddUniform("view_mat");
    trajectoryShader.AddUniform("projection_mat");
    trajectoryShader.AddUniform("model_mat");
    trajectoryShader.AddUniform("averageSpeed");
    trajectoryShader.AddUniform("mode");
    trajectoryShader.AddUniform("windowSize");
    trajectoryShader.AddUniform("minMaxCurrent");
    trajectoryShader.AddUniform("currentSelection");
    trajectoryShader.AddUniform("minColor");
    trajectoryShader.AddUniform("maxColor");
    trajectoryShader.AddUniform("minWidth");
    trajectoryShader.AddUniform("maxWidth");
    trajectoryShader.AddUniform("minMaxCurrentFilter");
    trajectoryShader.AddUniform("selected");
    trajectoryShader.AddUniform("mapTexture");
    trajectoryShader.AddUniform("numberOfClusters");
    trajectoryShader.AddUniform("filterFlags");
    trajectoryShader.AddUniform("maxDistance");
    trajectoryShader.UnUse();
    
    GLSLShader mapShader;
    mapShader.LoadFromFile(GL_VERTEX_SHADER, "map_vs.glsl");
    mapShader.LoadFromFile(GL_FRAGMENT_SHADER, "map_fs.glsl");
    mapShader.LoadFromFile(GL_TESS_CONTROL_SHADER, "map_ts_control.glsl");
    mapShader.LoadFromFile(GL_TESS_EVALUATION_SHADER, "map_ts_eval.glsl");
    mapShader.CreateAndLinkProgram();
    mapShader.Use();
    mapShader.AddUniform("view_mat");
    mapShader.AddUniform("projection_mat");
    mapShader.AddUniform("model_mat");
    mapShader.AddUniform("curTexture");
    mapShader.AddUniform("heightMapTex");
    mapShader.AddUniform("elevationScale");
    mapShader.AddUniform("curZoom");
    mapShader.AddUniform("tileID");//by ID I mean the same style we are using for the files : x-y-zoom, but no
    mapShader.UnUse();

    GLSLShader fullScreenShader;
    fullScreenShader.LoadFromFile(GL_VERTEX_SHADER, "full_screen_vs.glsl");
    fullScreenShader.LoadFromFile(GL_FRAGMENT_SHADER, "full_screen_fs.glsl");
    fullScreenShader.CreateAndLinkProgram();
    fullScreenShader.Use();
    fullScreenShader.AddUniform("mapTexture");
    
    //this shader code initialization is a mess, but should not go inside the renderables, because they are being called here
    
    glewInit();
    glfwGetFramebufferSize(g_window, &g_gl_width, &g_gl_height);
    Framebuffer mapBuffer = Framebuffer(mapShader, fullScreenShader,g_gl_width,g_gl_height);
    Framebuffer trajBuffer = Framebuffer(trajectoryShader,fullScreenShader,g_gl_width,g_gl_height);
    glfwSetCursorPosCallback(g_window, mouse_callback);
    //glfwSetInputMode(g_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO(); (void)io;
    
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(g_window, true);
    ImGui_ImplOpenGL3_Init("#version 410");
    
    bool show_demo_window = true;
    bool show_another_window = false;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
    
    //need to change here to get all trajectory files on dir -- but will have to figure something to get files on demand
    
    TrajParser::basePosition = glm::vec3(0,0,0);
    
    //Have to change things to also be able to load the geolife trajectory type
    //yeah I need to change it
    
    //would make sense for each trajectory to be connected to a shader
    //should I have a shader object pointer/reference in the trajectory?
    //for now will just iterate over the list
    
    //TrajParser trajetory("trajectories/20080506020147.plt",firstPassShader);
    //TrajParser trajetory2("trajectories/20080505124345.plt",firstPassShader);
    //TrajParser trajetory3("trajectories/20080509004801.plt",firstPassShader);
    
//    TrajParser trajetory("trajectories/walk_11.csv",firstPassShader);
//    TrajParser trajetory2("trajectories/walk_16.csv",firstPassShader);
//    TrajParser trajetory3("trajectories/walk_17.csv",firstPassShader);
//    TrajParser trajetory4("trajectories/walk_20.csv",firstPassShader);
    Map::zoom = 9;
    
    //loading trajectories based on starting position
    //std::string location = Map::GetLocation();
    //std::string location = "-30.057637,-51.171501"; //mocking location
    //std::vector<TrajParser> TrajList = TrajParser::LoadTrajDescription(location,firstPassShader);
    //mocking position, also can I only implemented the version of the method using the GeoPosition struct, not strings
    //GeoPosition start {"40,116",40,116,glm::vec2(40,116)};//around Beijing
    GeoPosition start;
    start = Map::GetLocation(true);//added the option to mock the location or not
    
    TrajectoryManager trajManager{trajectoryShader};
    ClusterManager clusterManager;
    //std::vector<TrajParser> TrajList = TrajParser::LoadLocalTrajectories(start, trajectoryShader);
    
    //old way still availiable
    //std::vector<TrajParser> TrajList = TrajParser::LoadTrajDescription("trajectories/trajectories3.txt",trajectoryShader);
    
    int mode = 0;
    
//    TrajList.push_back(trajetory);
//    TrajList.push_back(trajetory2);
//    TrajList.push_back(trajetory3);
//    TrajList.push_back(trajetory4);

    //camera.cameraPosition.y = trajetory.positions[0].y;
    glm::mat4 perspectiveMatrix = glm::perspective(glm::radians(45.0f), (float)g_gl_width / g_gl_height, 0.1f, 10000.0f);
    
    //keeping this for compatibility, but... not a good name for this case
    //glm::mat4 perspectiveMatrix = glm::ortho(0, (float)g_gl_width, (float)g_gl_height, 0, 0.1, -10000.0); //not working, look how I built this before
    //glm::mat4 perspectiveMatrix = glm::orthoLH(0, g_gl_width, g_gl_height, 0, 0, -10000);
    
    //this kinda works for ortographic perspective (but movement and viewing volume are not ok)
    //glm::mat4 perspectiveMatrix = glm::ortho<float>(0.0f, (float)g_gl_width,-(float)g_gl_height,(float)g_gl_height, -1000.f, +1000.0f);
    
	glm::mat4 model_mat = glm::mat4(1.0f);
    //model_mat = glm::scale(model_mat, glm::vec3(0.2,0.2,0.2));
	
    GLfloat deltaTime = 0.0f;
    GLfloat lastFrame = 0.0f;
    
/*------------------------------rendering loop--------------------------------*/
	/* some rendering defaults */
    
    //IMPORTANT - I disabled depth testing here to render both the fullscreen texture and the trajectory after it,
    //but not sure if there could be consequences.
    //at least when doing things in screen space, I think the usual thing to do is disable it
    //this still doesnt solve blending, which I should look into doing
    //but in a way this is good I guess? just realized that lowering the alpha on the map would make the whole thing transparent maybe?
    //also could just render the trajectory to another full screen texture and blend the two in a final pass
    //this is not very relevant because rendering the trajectories in screen space would solve these issues
	//glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
//    glEnable (GL_CULL_FACE); // cull face
//    glCullFace(GL_BACK); // cull back face
	glFrontFace (GL_CCW); // GL_CCW for counter clock-wise
	
    glViewport(0, 0, g_gl_width,  g_gl_height);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDepthMask(GL_TRUE);

    trajectoryShader.Use();
    glUniformMatrix4fv(trajectoryShader("projection_mat"), 1, GL_FALSE, glm::value_ptr(perspectiveMatrix));
    glUniform1i(trajectoryShader("mode"), mode);
    glUniform2fv(trajectoryShader("windowSize"), 1, glm::value_ptr(glm::vec2(g_gl_width,g_gl_height)));
    glUniform1i(trajectoryShader("currentSelection"),0);
    //glUniform3fv(trajectoryShader("minMaxCurrent"), 1, glm::value_ptr(glm::vec3(-50,50,TrajList[0].segList[0].segWeather.temperature)));
    
    GLSLShader clusterShader;
    clusterShader.LoadFromFile(GL_VERTEX_SHADER, "cluster_vert.glsl");
    clusterShader.LoadFromFile(GL_FRAGMENT_SHADER, "cluster_frag.glsl");
    clusterShader.LoadFromFile(GL_GEOMETRY_SHADER, "cluster_geom.glsl");
    clusterShader.CreateAndLinkProgram();
    clusterShader.Use();
    clusterShader.AddUniform("view_mat");
    clusterShader.AddUniform("projection_mat");
    clusterShader.AddUniform("model_mat");
    clusterShader.AddUniform("windowSize");
    clusterShader.AddUniform("maxDistance");
    clusterShader.AddUniform("numberOfClusters");
    clusterShader.AddUniform("minMaxCurrentFilter");
    glUniformMatrix4fv(clusterShader("projection_mat"), 1, GL_FALSE, glm::value_ptr(perspectiveMatrix));
    glUniform2fv(clusterShader("windowSize"), 1, glm::value_ptr(glm::vec2(g_gl_width,g_gl_height)));
    glUniform1f(clusterShader("maxDistance"),maxDistance);
    clusterShader.UnUse();
    
    mapShader.Use();
    glUniformMatrix4fv(mapShader("projection_mat"), 1, GL_FALSE, glm::value_ptr(perspectiveMatrix));
    //-30.057637, -51.171501
    
    glUniform1i(mapShader("curTexture"), 0);
    glUniform1i(mapShader("heightMapTex"), 1);
    
    float distance = cameraDistance(&camera);
    float ratio; //= 1 / distance;
    //10*(1-(00/1000))
    
    ratio = 1-(distance/1000);
    //int zoom = (int)floor(5000 * ratio);
    
    
    int zoom = (10*ratio) + 9;
    
    Map::zoom = zoom; //wont use both for long
    
    glUniform1f(mapShader("curZoom"), Map::zoom);
    
    //this is still static
    //will need to use the calculation from lat/lon to our coordinate system and then update map when camera moves
    //also would make sense to treat the map sort of a 1 plane skybox
    //but the textures change based on position/distance - it just stays static with relation to the camera
    
    //should move this stuff to a init function
    glm::vec3 startPos;
//    if(TrajList.size() >0){
//        startPos = glm::vec3(TrajList[0].segList[0].lon,0.0,TrajList[0].segList[0].lat);
//    }
//    else{//would make sense to always use this I guess
        startPos = glm::vec3(start.lon,0.0,start.lat);
//    }
    
    float posX = Map::long2tilexpx(startPos.x, Map::zoom);
    float posY = Map::lat2tileypx(startPos.z, Map::zoom);
    
    float translatedX = (posX *200) -100;
    float translatedY = (posY *200) -100;
    
    //int x = Map::long2tilex(startPos.x,zoom);
    //int y = Map::lat2tiley(startPos.z, zoom);
    //double returnedLat = Map::tiley2lat(y, zoom);
    //double returnedLon = Map::tilex2long(x, zoom);
    
    Tile::tileScale = Tile::recalculateScale(startPos.z, Map::zoom);

    
    //TrajList[0].SetScale(x, y, zoom);
    
    //-30.035939, -51.213876
    //Map myMap(-30.057637, -51.171501,zoom,mapShader);
    //Map myMap(-30.035939, -51.213876,zoom,mapShader);
    
    //should change to start on current location - but this is a big implementation thing
    Map myMap(startPos.z, startPos.x,Map::zoom,mapShader);
    
    //glm::mat4 trajMatrix = myMap.tileMap[4][4].modelMatrix;
    
    //trajmatrix should be a class member
    float ytrans = 5;
    glm::mat4 trajMatrix = TrajParser::SetTrajMatrix(startPos.x, startPos.z);//glm::mat4(1.0);
    //trajMatrix = glm::translate(trajMatrix,glm::vec3(-(1)*200,-99,-TILEMAP_SIZE-1*200));
    
    //trajMatrix = glm::translate(trajMatrix,glm::vec3(-200,00,-400));
    
    //trajMatrix = glm::scale(trajMatrix, glm::vec3(0.2,1.0,0.2));
    
    //trajMatrix = glm::scale(trajMatrix, glm::vec3(TrajParser::relativeScale,1.0,TrajParser::relativeScale));
    //trajMatrix = glm::rotate<float>(trajMatrix, -M_PI/2, glm::vec3(0.0,1.0,0.0));
    
//    int R = myMap.tileMap[TILEMAP_SIZE/2][TILEMAP_SIZE/2].height_data[1];
//    int G = myMap.tileMap[TILEMAP_SIZE/2][TILEMAP_SIZE/2].height_data[2];
//    int B = myMap.tileMap[TILEMAP_SIZE/2][TILEMAP_SIZE/2].height_data[3];
//    float height = (-1000 + (R * 256 * 256 + G * 256 + B) * 0.1);
    
    
    //dont think this is needed anymore
//    float xtrans = 0.0;
//    float ytrans = 5.0;
//
//
//    trajMatrix = glm::translate(trajMatrix, glm::vec3(translatedX,ytrans,translatedY));
//    trajMatrix = glm::rotate<float>(trajMatrix, -M_PI, glm::vec3(1.0,0.0,0.0));
    
    
    //
    
    
    
    //trajMatrix = glm::translate(trajMatrix, glm::vec3(0.0,-99.0,0.0));
    
    //myMap.GetMapData(-30.057637, -51.171501,zoom);
    //myMap.GetLocation();
    
    //if 1000 is default distance
    
    float rotation = 0.0;
    
    //imgui variables
    static bool picker = false;
    static float minValueColor = -50;
    static float maxValueColor = 50;

    static float minColor[3] = { 0.0f,0.0f,0.0f};
    static float maxColor[3] = { 1.0f,1.0f,1.0f};
    
    static float minWidth = 1;
    static float maxWidth = 5;
    
    static float minFilter = -50;
    static float maxFilter = 50;
    static int filter = 0;
    static int selected = 0;
    
    static char minDate[11] = "2000-01-01";
    static char maxDate[11] = "2000-01-01";

    static int colorAttribute = 0;
    static int shapeAttribute = 0;
    
    static int epsilon = 30;
    static int minLines = 5;
    static int scale = 10;
    
    static int minMaxCluster[1][2]{{0,10}};//just a starting default, wont know until actually clustered
    
    static bool filterClustering = false;
    
    static int numClusters = 0;//TODO - gonna use for multiple clustering passes, dont commit for now
    uint flag =0;
    static float minMaxCurrentFilters[6][2]{{-50,50},{0,100},{0,23},{0,0},{0,0},{0,0}};
    uint modeFlag = TRANSPORT_MODE;
    
    trajectoryShader.Use();
    //trajManager.ClusterTrajectories();
    //trajManager.CreateRTree();
    
	while (!glfwWindowShouldClose (g_window)) {
		
        //start imgui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
    
        
        //should see how messing around with the framebuffers (cleaning, writing etc) affects performance)
        //glViewport(0, 0, g_gl_width, g_gl_height); //also calling this every frame? thats how it was in the ray tracer but seems weird
        //but the call seems necessary to properly align the framebuffer texture with the viewport
        
        
        mapBuffer.Use();
        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(0.0, 0.0, 0.0, 1.0);
        
        glEnable(GL_DEPTH_TEST);

        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        camera.cameraSpeed = 500.0f * deltaTime;
        
        //glm::mat4 viewMatrix = glm::lookAt(camera.cameraPosition, camera.cameraPosition + camera.cameraFront, camera.cameraUp);
        glm::mat4 viewMatrix = glm::lookAt(camera.cameraStaticPosition, camera.cameraStaticPosition + camera.cameraStaticFront, camera.cameraStaticUp);
        
		_update_fps_counter (g_window);
		
//        glFinish();
//
//        GLuint query;
//        GLuint64 elapsed_time;
//
//        glGenQueries(1, &query);
//        glBeginQuery(GL_TIME_ELAPSED, query);
      
        mapShader.Use();
        glUniformMatrix4fv(mapShader("view_mat"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
        myMap.Render();
       
        //the way I named this makes it a bit confusing, but unuse means we are no longer drawing to a texture
        mapBuffer.UnUse();
        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//        glDisable(GL_DEPTH_TEST);
//
        fullScreenShader.Use();

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, mapBuffer.frameBufferTexture);
        glBindVertexArray(mapBuffer.vertexArrayObject);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        fullScreenShader.UnUse();
        //glBindVertexArray(myMap.vertexArrayObject);
        //glDrawArrays(GL_TRIANGLES, 0, 6);
        
        /*-----------------------------Trajectory Rendering-------------------------------*/
        if(!TrajectoryManager::renderRepresent){
            trajBuffer.Use();
            glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glDisable(GL_DEPTH_TEST);
            trajectoryShader.Use();

            //need to move this into the render function
            glUniformMatrix4fv(trajectoryShader("view_mat"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
            glUniformMatrix4fv(trajectoryShader("model_mat"), 1, GL_FALSE, glm::value_ptr(trajMatrix));
            glUniform3fv(trajectoryShader("minMaxCurrent"),1,glm::value_ptr(glm::vec3(minValueColor,maxValueColor,0)));//not using z
            glUniform2fv(trajectoryShader("minMaxCurrentFilter"),6,*minMaxCurrentFilters);
            glUniform3fv(trajectoryShader("minColor"),1,minColor);
            glUniform3fv(trajectoryShader("maxColor"),1,maxColor);
            glUniform1f(trajectoryShader("minWidth"),minWidth);
            glUniform1f(trajectoryShader("maxWidth"),maxWidth);
            glUniform1i(trajectoryShader("currentSelection"),colorAttribute);
            glUniform1i(trajectoryShader("mode"),shapeAttribute);
            glUniform1ui(trajectoryShader("filterFlags"),flag);
            glUniform1i(trajectoryShader("selected"),selected);
            glUniform1i(trajectoryShader("numberOfClusters"),trajManager.numberOfClusters);//dont need to update this every frame
            glUniform1f(trajectoryShader("maxDistance"),maxDistance);
            
            glBindVertexArray (trajManager.vertexArrayObject);
            glDrawArrays (GL_LINE_STRIP_ADJACENCY, 0,trajManager.pointCount);
        
            trajBuffer.UnUse();
            //glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            //glDisable(GL_DEPTH_TEST);
            fullScreenShader.Use();
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, trajBuffer.frameBufferTexture);
            glBindVertexArray(trajBuffer.vertexArrayObject);
            glDrawArrays(GL_TRIANGLES, 0, 6);
            fullScreenShader.UnUse();
        }
        /*-----------------------------End Trajectory Rendering---------------------------*/
        /*-----------------------------Represent. Rendering-------------------------------*/
        else{
            glDisable(GL_DEPTH_TEST);
            clusterShader.Use();
            glUniformMatrix4fv(clusterShader("view_mat"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
            glUniformMatrix4fv(clusterShader("model_mat"), 1, GL_FALSE, glm::value_ptr(trajMatrix));
            glUniform1f(clusterShader("maxDistance"),maxDistance);
            glUniform1i(clusterShader("numberOfClusters"),trajManager.numberOfClusters);//dont need to update this every frame
            
            glUniform2iv(clusterShader("minMaxCurrentFilter"),1,*minMaxCluster);
            glBindVertexArray(clusterManager.vertexArrayObject);
            glDrawArrays(GL_LINE_STRIP_ADJACENCY, 0, clusterManager.pointCount);
        }
        /*-----------------------------End Represent. Rendering---------------------------*/
        
        bool my_tool_active;
//        ImGui::Begin("Test");
        ImGui::Begin("Filter Attributes", &my_tool_active, ImGuiWindowFlags_MenuBar);

//        if(ImGui::Button("Show color picker")){
//            picker = !picker;
//            std::cout << color[0] << " " << color[1] << " " << color[2] << " " << color[3] << "\n";
//        }
//
//        if(picker){
//            ImGui::ColorEdit3("Select Color", color);
//        }
        

        ImGui::RadioButton("Temperature", &selected, 0); ImGui::SameLine();
        ImGui::RadioButton("Speed", &selected, 1); ImGui::SameLine();
        ImGui::RadioButton("Time", &selected, 2); ImGui::SameLine();
        ImGui::RadioButton("Date", &selected, 3); ImGui::SameLine();
        ImGui::RadioButton("Mode", &selected, 5);
        if(trajManager.isClustered){
            ImGui::SameLine();
            ImGui::RadioButton("Cluster", &selected, 4);
        }
        
        //can make a thing here to have an "old selected value", so when the selected is changed
        //we restart the range of the new selected
        //should have created an enum for all these values
        if(selected == 0 || selected == 1){
//            static char min[4] = "0";
//            static char max[4] = "0";
//            ImGui::InputText("Min Value", min, IM_ARRAYSIZE(min));
//            ImGui::InputText("Max Value", max, IM_ARRAYSIZE(max));
            
            if(selected == 0){
                ImGui::CheckboxFlags("Keep Filter", &flag, FILTER_TEMPERATURE);
                ImGui::SliderFloat("Min Temp", &minMaxCurrentFilters[0][0], -50, 50);
                ImGui::SliderFloat("Max Temp", &minMaxCurrentFilters[0][1], -50, 50 );
            }
            if(selected == 1){
                ImGui::CheckboxFlags("Keep Filter", &flag, FILTER_SPEED);
                ImGui::SliderFloat("Min Speed", &minMaxCurrentFilters[1][0], 0, 100);
                ImGui::SliderFloat("Max Speed", &minMaxCurrentFilters[1][1], 0, 100 );
            }
//            if(ImGui::Button("Filter")){
//                std::string attribute;
//                switch (selected) {
//                    case 0:
//                        attribute = "TEMPERATURE";
//                        break;
//                    case 1:
//                        attribute = "AVERAGESPEED";
//                        break;
//                    default:
//                        break;
//                }
//                //FilterBySelection("TEMPERATURE", min, max, trajectoryShader, &TrajList);
//            }
            
        }
        else{
            //2009-10-02T03:24:28Z datetime format

            if(selected == 2){
//                static char min[4] = "0";
//                static char max[4] = "0";
//
//                ImGui::InputText("Start Time", min, IM_ARRAYSIZE(min));
//                ImGui::InputText("End Time", max, IM_ARRAYSIZE(max));
//                if(ImGui::Button("Filter")){
//                    FilterByTime(min, max, trajectoryShader, &TrajList);
//                }
                ImGui::CheckboxFlags("Keep Filter", &flag, FILTER_TIME);
                ImGui::SliderFloat("Start Time", &minMaxCurrentFilters[2][0], 0, 23);
                ImGui::SliderFloat("End Time", &minMaxCurrentFilters[2][1], 0, 23 );
            }
            if(selected == 3){
                ImGui::CheckboxFlags("Keep Filter", &flag, FILTER_DATE);
                //should look into only doing this when there is change
                ImGui::InputText("Start Date", minDate, IM_ARRAYSIZE(minDate));
                ImGui::InputText("End Date", maxDate, IM_ARRAYSIZE(maxDate));
                std::string min = minDate;
                std::string max = maxDate;
                
                
                minMaxCurrentFilters[3][0] = std::stof(min.substr(0,4) + min.substr(5,2) + min.substr(8,2));
                minMaxCurrentFilters[3][1] = std::stof(max.substr(0,4) + max.substr(5,2) + max.substr(8,2));
                
                //if(ImGui::Button("Filter")){
                    //FilterByDate(min, max, trajectoryShader, &TrajList);
                //}
            }
            
            if(selected == 4){
                ImGui::CheckboxFlags("Keep Filter", &flag, FILTER_CLUSTER);
                //TODO - change cluster shader data back to int, probably wont need all this casting
                int min = static_cast<int>(minMaxCurrentFilters[4][0]);
                int max = static_cast<int>(minMaxCurrentFilters[4][1]);
                //TODO - dont remember exactly where I'm setting anything to -6, and why is numClust+1. Look into this.
                ImGui::SliderInt("Min Cluster", &min, -6, trajManager.numberOfClusters+1);
                ImGui::SliderInt("Max Cluster", &max, -6, trajManager.numberOfClusters+1);
                minMaxCurrentFilters[4][0] = static_cast<float>(min);
                minMaxCurrentFilters[4][1] = static_cast<float>(max);
            }
            
            if(selected == 5){
                ImGui::CheckboxFlags("Keep Filter", &flag, FILTER_MODE);
                ImGui::NewLine();
                ImGui::CheckboxFlags("Undefined",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Undefined)); ImGui::SameLine();
                ImGui::CheckboxFlags("Walk",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Walk)); ImGui::SameLine();
                ImGui::CheckboxFlags("Bike",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Bike));ImGui::SameLine();
                ImGui::CheckboxFlags("Bus",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Bus));
                
                ImGui::CheckboxFlags("Car",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Car)); ImGui::SameLine();
                ImGui::CheckboxFlags("Taxi",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Taxi)); ImGui::SameLine();
                ImGui::CheckboxFlags("Airplane",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Airplane)); ImGui::SameLine();
                ImGui::CheckboxFlags("Other",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Other));
                
                ImGui::CheckboxFlags("Boat",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Boat));   ImGui::SameLine();
                ImGui::CheckboxFlags("Subway",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Subway)); ImGui::SameLine();
                ImGui::CheckboxFlags("Train",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Train));  ImGui::SameLine();
                ImGui::CheckboxFlags("Run",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Run));
                
                ImGui::CheckboxFlags("Motorcycle",&modeFlag,1<<TrajSeg::GetTransportModeValue(TransportMode::Motorcycle));
                
                minMaxCurrentFilters[5][0] = static_cast<float>(modeFlag);
            }
        }

        

        //keeping this here just for
//        char buf[200];
//        ImGui::InputText("Type query", buf, IM_ARRAYSIZE(buf));
//        
//        std::cout << buf << "\n";

//        if (ImGui::BeginMenuBar())
//        {
//            if (ImGui::BeginMenu("File"))
//            {
//                if (ImGui::MenuItem("Open..", "Ctrl+O")) { /* Do stuff */ }
//                if (ImGui::MenuItem("Save", "Ctrl+S"))   { /* Do stuff */ }
//                //if (ImGui::MenuItem("Close", "Ctrl+W"))  { my_tool_active = false; }
//                ImGui::EndMenu();
//            }
//            ImGui::EndMenuBar();
//        }

        ImGui::End();
        
        ImGui::Begin("Map Attributes");

        
        const char* items[] = { "Temperature", "Speed", "Time of Day", "Transport Mode", "Cluster"};
        //const char* items[] = { "Temperature", "Speed", "Time of Day", "Cluster"};
        
//        if(trajManager.isClustered)
//            items.push_back("Cluster");
        
        //if temperature then only can change the min max values
        //speed both the values and the range of colors
        //time of day only the range of colors
//        std::string test = items[0];
        static const char* item_current = items[0];
        static ImGuiComboFlags flags = 0;
        ImGui::BeginGroup();
            ImGui::Text("Color");
            if (ImGui::BeginCombo("Attribute", item_current, flags)){ // The second parameter is the label previewed before opening the combo.
                int limit=0;
                if(trajManager.isClustered){
                    limit = IM_ARRAYSIZE(items);
                }else{
                    limit = IM_ARRAYSIZE(items)-1;
                }
                for (int n = 0; n < limit; n++){
                    bool is_selected = (item_current == items[n]);
                    if (ImGui::Selectable(items[n], is_selected)){
                        item_current = items[n];
                        std::cout << item_current;
                        //we are setting the uniform directly here, but should probably do it in a specific place
                        
                        colorAttribute = n;
                    }
                    if (is_selected){
                        ImGui::SetItemDefaultFocus();   // Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)

                    }
                }
                ImGui::EndCombo();
            }
        if(strcmp(item_current, "Temperature") == 0 ){//in this case we dont need
            ImGui::SliderFloat("Min Temperature", &minValueColor, -50, 50);
            ImGui::SliderFloat("Max Temperature", &maxValueColor, -50, 50);
        }
        if(strcmp(item_current, "Speed") == 0 ){//in this case we dont need
            ImGui::SliderFloat("Min Speed", &minValueColor, 0, 100);
            ImGui::SliderFloat("Max Speed", &maxValueColor, 1, 100);

            ImGui::ColorEdit3("Min Color", minColor);
            ImGui::ColorEdit3("Max Color", maxColor);
        }
        if(strcmp(item_current, "Time of Day") == 0 ){//in this case we dont need
            //ImGui::SliderFloat("Min Speed", &minValueColor, 0, 100);
            //ImGui::SliderFloat("Max Speed", &maxValueColor, 1, 100);
            ImGui::SliderFloat("Start Time", &minValueColor, 0, 23);
            ImGui::SliderFloat("End Time", &maxValueColor, 0, 23);

            ImGui::ColorEdit3("Min Color", minColor);
            ImGui::ColorEdit3("Max Color", maxColor);
        }
        
        //TODO - map colors as discrete values for each mode
        if(strcmp(item_current, "Transport Mode") == 0 ){//in this case we dont need
            ImGui::SliderFloat("Start Mode", &minValueColor, 0, 12);
            ImGui::SliderFloat("End Mode", &maxValueColor, 0, 12);
            
            ImGui::ColorEdit3("Min Color", minColor);
            ImGui::ColorEdit3("Max Color", maxColor);
        }
        
        if(strcmp(item_current, "Cluster") == 0 ){//in this case we dont need
            ImGui::SliderFloat("Start Cluster", &minValueColor, 0, trajManager.numberOfClusters);
            ImGui::SliderFloat("End Cluster", &maxValueColor, 0, trajManager.numberOfClusters);
            
            ImGui::ColorEdit3("Min Color", minColor);
            ImGui::ColorEdit3("Max Color", maxColor);
        }
        
        ImGui::EndGroup();
        ImGui::End();
        
        //start another group for shape
        
        ImGui::Begin("Map Attributes");
        const char* itemsShape[] = { "Temperature", "Speed", "Time of Day"};//, "CCCC", "DDDD", "EEEE", "FFFF", "GGGG", "HHHH", "IIII", "JJJJ", "KKKK", "LLLLLLL", "MMMM", "OOOOOOO" };
        
        //if temperature then only can change the min max values
        //speed both the values and the range of colors
        //time of day only the range of colors
        static const char* item_current_shape = itemsShape[0];
        static ImGuiComboFlags flagsShape = 0;
        ImGui::BeginGroup();
        ImGui::Text("Shape");
        if (ImGui::BeginCombo("Attribute2", item_current_shape, flagsShape)){ // The second parameter is the label previewed before opening the combo.
            for (int n = 0; n < IM_ARRAYSIZE(itemsShape); n++){
                bool is_selected_shape = (item_current_shape == itemsShape[n]);
                if (ImGui::Selectable(itemsShape[n], is_selected_shape)){
                    item_current_shape = itemsShape[n];
                    //std::cout << item_current;
                    //we are setting the uniform directly here, but should probably do it in a specific place
                    
                    shapeAttribute = n+1;
                }
                if (is_selected_shape){
                    ImGui::SetItemDefaultFocus();   // Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
                    
                }
            }
            ImGui::EndCombo();
        }
        if(strcmp(item_current_shape, "Temperature") == 0 ){//in this case we dont need
            ImGui::SliderFloat("Min width", &minWidth, 1, 10);
            ImGui::SliderFloat("Max width", &maxWidth, 2, 10);
            
            ImGui::SliderFloat("Start value", &minValueColor, -50, 50);
            ImGui::SliderFloat("End value", &maxValueColor, -50, 50);
        }
        if(strcmp(item_current_shape, "Speed") == 0 ){
            ImGui::SliderFloat("Min width", &minWidth, 1, 10);
            ImGui::SliderFloat("Max width", &maxWidth, 2, 10);
            
            ImGui::SliderFloat("Min Speed", &minValueColor, 0, 100);
            ImGui::SliderFloat("Max Speed", &maxValueColor, 1, 100);
        }
        if(strcmp(item_current_shape, "Time of Day") == 0 ){//in this case we dont need
            //ImGui::SliderFloat("Min Speed", &minValueColor, 0, 100);
            //ImGui::SliderFloat("Max Speed", &maxValueColor, 1, 100);
            ImGui::SliderFloat("Start Time", &minValueColor, 0, 23);
            ImGui::SliderFloat("End Time", &maxValueColor, 0, 23);
            
            ImGui::ColorEdit3("Min Color", minColor);
            ImGui::ColorEdit3("Max Color", maxColor);
        }
        
        //ImGui::SameLine();
//        static float minColor[3] = { 0.0f,0.0f,0.0f};
//        static float maxColor[3] = { 1.0f,1.0f,1.0f};
//        static int minValue = 0;
//        static int maxValue = 50;

//        if(ImGui::Button("PickColor"))
//            ImGui::ColorPicker3("Range", minColor, ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_NoSmallPreview);
        
//        ImGui::ColorEdit3("Start Color", minColor);
//        //ImGui::SameLine();
//        //ImGui::SliderInt("Temperature Min", &temp, minTemp, maxTemp);
//        ImGui::InputInt("Value Min", &minValue);
//
//        ImGui::ColorEdit3("End Color", maxColor);
//        ImGui::InputInt("Value Max", &maxValue);
        
        
        ImGui::EndGroup();
//        ImGui::SameLine();
//        ImGui::BeginGroup();
//        ImGui::Text("Shape");
//        if (ImGui::BeginCombo("Attribute", item_current, flags)){ // The second parameter is the label previewed before opening the combo.
//            for (int n = 0; n < IM_ARRAYSIZE(items); n++){
//                bool is_selected = (item_current == items[n]);
//                if (ImGui::Selectable(items[n], is_selected)){
//                    item_current = items[n];
//                    std::cout << item_current;
//                }
//                if (is_selected){
//                    ImGui::SetItemDefaultFocus();   // Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
//
//                }
//            }
//            ImGui::EndCombo();
//        }
//        ImGui::EndGroup();
        
//
//        static float minColor[4] = { 0.0f,0.0f,0.0f,0.0f };
//        ImGui::ColorPicker3("Low Value", minColor);
//
//        static float maxColor[4] = { 0.0f,0.0f,0.0f,0.0f };
//        ImGui::ColorPicker3("Max Value", maxColor);

        
        ImGui::End();
        
        ImGui::Begin("Clustering");
            ImGui::Text("Set clustering options");
            ImGui::InputInt("Epsilon", &epsilon);
            ImGui::InputInt("Min Lines ", &minLines);
            ImGui::InputInt("Scale Factor ", &scale);
            ImGui::NewLine();
            ClusterUI(trajManager, clusterManager, &minMaxCurrentFilters, filterClustering, epsilon, minLines, scale, flag, modeFlag);
            //moved clustering to own function, maybe do with stuff below?
            //think everything here is specific to each cluster process, maybe not rendering
            //think there should be a global cluster id system
            if(trajManager.isClustered){
                //maybe the post clustering UI should go in a function as well
                if(ImGui::Button("Render Represent. Trajs.")){
                    TrajectoryManager::renderRepresent = !TrajectoryManager::renderRepresent;
                }
                if(ImGui::Button("Clear Clustering")){
                    //TODO
                    //Should encapsulate this
                    trajManager.SetupClearClusterData();
                    trajManager.isClustered = false;
                    trajManager.numberOfClusters = 0;
                    selected = 0;
                    colorAttribute = 0;
                    clusterManager.representativeTrajectories.clear();
                    trajManager.renderRepresent = false;//think this should be on clustermanager
                }
                ImGui::NewLine();
                ImGui::SliderInt("Min Cluster", &minMaxCluster[0][0], 0, trajManager.numberOfClusters);
                ImGui::SliderInt("Max Cluster", &minMaxCluster[0][1], 0, trajManager.numberOfClusters);
                
                ImGui::NewLine();
                if(ImGui::Button("+")){
                    trajManager.canClusterAgain = true;
                }
                //is there any reason to use new buttons?
                //for(int i =0; i < numClusters; i++){
                    //ClusterUI(trajManager, clusterManager, &minMaxCurrentFilters, filterClustering, epsilon, minLines, scale, flag, modeFlag);
                    //ImGui::NewLine();
                //}
            }
            //if a trajectory already has been clustered, allow it to go again? I think it makes sense not to.
            //TODO - use this as base for creating multiple cluster cases (have to think of a better name for this process)
//
//            for(int i =0; i < numClusters; i++){
//                std::string label = "Label " + std::to_string(i);
//                ImGui::RadioButton(label.c_str(), false);
//                ImGui::NewLine();
//            }
        ImGui::End();
        
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		// update other events like input handling 
		glfwPollEvents ();

		
        //all of this movement code comes from the old old old "Camera Virtual + GLM e Model Matrix" camera project I think?
        //thats probably why there is a camera movement part and a model movement part I think. need to clean thisss
/*-----------------------------move camera here-------------------------------*/
		// control keys
        processs_keyboard(g_window, &camera,&myMap, &trajManager, trajMatrix,trajectoryShader,&clusterManager);
        //this should be every key pressed now
        //TODO - is there a reason to perform this comparisson every frame?
        float curDistance = cameraDistance(&camera);
        if(abs(distance-curDistance) >= 100){
            //ratio = 1/curDistance;
            SetZoomLevel(&trajManager,&clusterManager, curDistance, mapShader, myMap, ratio, startPos, trajMatrix, zoom);
            distance = curDistance;
        }
        
        //should move this stuff into the keyboard function
        //changing this mapping to the gui
//        if(GLFW_PRESS == glfwGetKey(g_window,GLFW_KEY_1))
//        {
//            mode = 1;
//        }
//
//        if(GLFW_PRESS == glfwGetKey(g_window,GLFW_KEY_2))
//        {
//            mode = 2;
//        }
        
        if(GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_G)){
            ytrans += 10.0;
            //trajMatrix = glm::mat4(1.0);
            trajMatrix = glm::translate(trajMatrix, glm::vec3(0.0,ytrans,0.0));
            trajMatrix = glm::rotate<float>(trajMatrix, -M_PI, glm::vec3(1.0,0.0,0.0));
        }
        if(GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_H)){
            ytrans -= 10.0;
            //trajMatrix = glm::mat4(1.0);
            trajMatrix = glm::translate(trajMatrix, glm::vec3(0.0,ytrans,0.0));
            trajMatrix = glm::rotate<float>(trajMatrix, -M_PI, glm::vec3(1.0,0.0,0.0));
        }


        //for testing the position
//        std::cout << std::to_string(cameramatrix[3][0]) << " " << std::to_string(cameramatrix[3][1])<< " " << std::to_string(cameramatrix[3][2]) << "\n";
        //std::cout << camera.cameraPosition.x << " " << camera.cameraPosition.z << " " << camera.cameraPosition.y << "\n";
        //std::cout << camera.cameraUp.x << " " << camera.cameraUp.y << " " << camera.cameraUp.z << "\n";
        
//
//        if (GLFW_PRESS == glfwGetKey (g_window, GLFW_KEY_ESCAPE)) {
//            glfwSetWindowShouldClose (g_window, 1);
//        }
//
//        if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_W))
//            camera.cameraPosition += camera.cameraSpeed * camera.cameraFront;
//
//        if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_S))
//            camera.cameraPosition -= camera.cameraSpeed * camera.cameraFront;
//
//        if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_A))
//            camera.cameraPosition -= glm::normalize(glm::cross(camera.cameraFront, camera.cameraUp)) * camera.cameraSpeed;
//
//        if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_D))
//            camera.cameraPosition += glm::normalize(glm::cross(camera.cameraFront, camera.cameraUp)) * camera.cameraSpeed;
//
//        if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_U))
//            camera.cameraPosition.y += camera.cameraSpeed;// glm::normalize(glm::cross(camera.cameraFront, camera.cameraUp)) * camera.cameraSpeed;
//
//        if (GLFW_PRESS == glfwGetKey(g_window, GLFW_KEY_J))
//            camera.cameraPosition.y -= camera.cameraSpeed;
        
		// put the stuff we've been drawing onto the display
		glfwSwapBuffers (g_window);

        glFinish();
		//in this case we are measuring the total rendering time- should separate by map and trajectories, 
		//especially to measure things like tesselation, geometry generation and number of trajectories.
//        glFinish();
//
//        glEndQuery(GL_TIME_ELAPSED);
//        glGetQueryObjectui64v(query, GL_QUERY_RESULT, &elapsed_time);
//        printf("%f ms\n", (elapsed_time) / 1000000.0);
	}
	
	// close GL context and any other GLFW resources
	glfwTerminate();
	return 0;
}
