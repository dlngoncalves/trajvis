//
//  Framebuffer.h
//  Waves
//
//  Created by Diego Gonçalves on 23/11/18.
//  Copyright © 2018 Diego Gonçalves. All rights reserved.
//

#ifndef Framebuffer_h
#define Framebuffer_h

#define GL_SILENCE_DEPRECATION
#ifdef __APPLE__
#include <OpenGL/gl3.h>
#include <OpenGL/gl3ext.h>
#else
#include <GL/glew.h> // include GLEW and new version of GL on Windows
#endif
#include "GLSLShader.h"

enum class FrameBufferType{
    Screen,
    Texture
};


class Framebuffer {

    //need to make things not public again I guess
    //this should go into the code_cleanup feature later
public:

    FrameBufferType type;
    GLuint frameBuffer;
    GLuint frameBufferTexture;
    
    //I dont remember if in the ray tracer we actually had
    //these because we needed them or something didnt work
    //I think they are needed for depth sorting?
    GLuint depthTexture;
    GLuint depthrenderbuffer;

    //full screen quad(and its buffers) are members of the framebuffer class
    GLuint vertexBufferObject;
    GLuint vertexArrayObject;
    GLuint cube_texture;
    GLuint tex_cube;

    GLfloat points[18] = {
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        -1.0f,  1.0f, 0.0f,
        -1.0f,  1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        1.0f,  1.0f, 0.0f,
        
    };
    
    //just redeclaring this here to shut up the compiler but if using need to pass real ones
    int width;
    int height;

    GLSLShader &inShader;
    GLSLShader &outShader;
    
    void Use();
    void UnUse();
    void SetupData(); //OpenGL data
    
    void CreateFrameBuffer(int width, int height);
    Framebuffer(GLSLShader &inShader, GLSLShader &outShader, int width=1280, int height=720);
    ~Framebuffer(){};
};
#endif /* Framebuffer_h */
