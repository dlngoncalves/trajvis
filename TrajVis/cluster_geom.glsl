#version 410

layout (lines_adjacency) in;
layout (triangle_strip, max_vertices = 5) out;

in vec3 vertColor_g[];
in int cluster_g[];
in vec4 screenPosition[];

out vec3 vertColor;

uniform mat4 projection_mat, view_mat, model_mat;
uniform vec2 windowSize;
uniform float maxDistance;
uniform ivec2 minMaxCurrentFilter;

vec2 ToScreenSpace(vec4 vertex)
{
    return vec2( vertex.xy / vertex.w ) * windowSize;
    
}

bool FilterOut(int min, int max, int current)
{
    if(min > max)
        return false;
    
    if(current < min)
        return true;
    if(current > max)
        return true;
    
    return false;
}

bool isOutside()
{
    vec2 screenCoords1 = ToScreenSpace(screenPosition[1]);
    vec2 screenCoords2 = ToScreenSpace(screenPosition[2]);

    vec2 area = windowSize;
    area.x *= 0.66667; //this is valid for the current projection/viewport

    if(screenCoords1.x < -area.x || screenCoords1.x > area.x)
        return true;
    if(screenCoords2.x < -area.x || screenCoords2.x > area.x)
        return true;
    if(screenCoords1.y < -area.y || screenCoords1.y > area.y)
        return true;
    if(screenCoords2.y < -area.y || screenCoords2.y > area.y)
        return true;

    return false;
}

void main() {
    
    float thickness1;
    float thickness2;
    
    vec3 color;
    color = vertColor_g[0];
    
    if(isOutside())
        return;
    
    if(FilterOut(minMaxCurrentFilter[0],minMaxCurrentFilter[1],cluster_g[1]))
        return;
    
    if(distance(gl_in[1].gl_Position,gl_in[2].gl_Position) > maxDistance)
        return;

    //not using thickness for now in the cluster trajectories, but keeping this here in case this changes, so the calculation below is the same
    thickness1 = 1;
    thickness2 = 1;

    //need to work with elevation data here
    float constantHeight = gl_in[1].gl_Position.y;

    //line segments - previous, current, next
    vec2 seg1 = normalize(vec2(gl_in[1].gl_Position.xz - gl_in[0].gl_Position.xz));
    vec2 seg2 = normalize(vec2(gl_in[2].gl_Position.xz - gl_in[1].gl_Position.xz));
    vec2 seg3 = normalize(vec2(gl_in[3].gl_Position.xz - gl_in[2].gl_Position.xz));

    //normals of line segments
    //TODO - why is this here? dx and dz are not being used
    float dx = gl_in[1].gl_Position.x - gl_in[0].gl_Position.x;
    float dz = gl_in[1].gl_Position.z - gl_in[0].gl_Position.z;
    vec2 normal1 = vec2(-dz,dx);
    normal1 = vec2(-seg1.y,seg1.x);

    dx = gl_in[2].gl_Position.x - gl_in[1].gl_Position.x;
    dz = gl_in[2].gl_Position.z - gl_in[1].gl_Position.z;
    vec2 normal2 = vec2(-dz,dx);
    normal2 = vec2(-seg2.y,seg2.x);

    dx = gl_in[3].gl_Position.x - gl_in[2].gl_Position.x;
    dz = gl_in[3].gl_Position.z - gl_in[2].gl_Position.z;
    vec2 normal3 = vec2(-dz,dx);
    normal3 = vec2(-seg3.y,seg3.x);

    normal1 = normalize(normal1);
    normal2 = normalize(normal2);
    normal3 = normalize(normal3);

    vec2 miter1 = normalize(normal2 + normal1);
    vec2 miter2 = normalize(normal3 + normal2);

    //trying to avoid weirdness at end points and some strange cases
    float length1 = clamp(thickness1/dot(miter1,normal2),0.01,1.0);
    float length2 = clamp(thickness2/dot(miter2,normal2),0.01,1.0);

    vec2 offset1;
    vec2 offset2;
    vec2 offset3;

    if(dot(seg1,normal2) > 0){
        offset1 = -(length1 * miter1);
        offset2 = thickness1 * normal2;
    }
    else{
        offset1 = -(thickness1 * normal2);
        offset2 = length1 * miter1;
    }

    vec4 newPos = gl_in[1].gl_Position;
    newPos.xz += offset1;
    newPos.y = constantHeight;
    gl_Position = projection_mat * view_mat * model_mat * newPos;
    vertColor = color;
    EmitVertex();

    newPos = gl_in[1].gl_Position;
    newPos.xz += offset2;
    newPos.y = constantHeight;
    gl_Position = projection_mat * view_mat * model_mat * newPos;
    vertColor = color;
    EmitVertex();

    if(dot(seg3,normal2) < 0){
        offset1 = -(length2 * miter2);
        offset2 = thickness2 * normal2;
        offset3 = thickness2 * normal3;
    }
    else{
        offset1 = -(thickness2 * normal2);
        offset2 = length2 * miter2;
        offset3 = -(thickness2 * normal3);
    }

    newPos = gl_in[2].gl_Position;
    newPos.xz += offset1;
    newPos.y = constantHeight;
    gl_Position = projection_mat * view_mat * model_mat * newPos;
    vertColor = color;
    EmitVertex();

    newPos = gl_in[2].gl_Position;
    newPos.xz += offset2;
    newPos.y = constantHeight;
    gl_Position = projection_mat * view_mat * model_mat * newPos;
    vertColor = color;
    EmitVertex();

    newPos = gl_in[2].gl_Position;
    newPos.xz += offset3;
    newPos.y = constantHeight;
    gl_Position = projection_mat * view_mat * model_mat * newPos;
    vertColor = color;
    EmitVertex();

    EndPrimitive();
}
