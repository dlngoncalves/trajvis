//
//  Trajectory.h
//  TrajVis
//
//  Created by Diego Gonçalves on 15/08/19.
//  Copyright © 2019 Diego Gonçalves. All rights reserved.
//

#ifndef TrajectoryManager_h
#define TrajectoryManager_h

#include <stdio.h>
#include <vector>
#include "TrajParser.h"
#include "Renderable.h"

struct VertexData{
    float vertex[3];
    float weather[3];
    float speed[3];
};

//created this class because I wanted to migrate some stuff away from the trajparser class but I think I'm just gonna refactor that one
class TrajectoryManager : public Renderable {
    
public:

    //questioning if we are even going to use it
    std::vector<TrajParser> trajlist; //this vector is currently on the TrajParser class itself, need to bring it here
    //should have a large buffer for all traj data that is submitted to the gpu - separate from the trajparser vector
    //more like a vec3 vector
    //but ideally that should be a mapped buffer instead
    std::vector<VertexData> vertexBuffer;
    
    int pointCount;
    
    std::vector<TrajParser> LoadTrajectories(GeoPosition location,GLSLShader shader);
    static void ParseGeolifeLabels(std::string file);
    static std::map<std::string,int> GetTransportTypes();
    std::vector<TrajParser> FastLoadTrajectories(GeoPosition location,GLSLShader shader);
    
    TrajectoryManager(GLSLShader &_shader);
    ~TrajectoryManager();
    
    bool FilterOut(float (*minMaxCurrentFilters)[6][2],unsigned int flags, unsigned int modeFlags,TrajParser &trajectory, TrajSeg &segment);
    bool FilterCurrent(float min, float max, float current);
    bool FilterTransport(int filterValues,int current);
    
    std::vector<std::vector<std::vector<double>>> PrepareForClustering(std::map<int,int> &indexMapping);
    std::vector<std::vector<std::vector<double>>> PrepareForClustering(float (*minMaxCurrentFilters)[6][2],unsigned int flags,unsigned int modeFlags,std::map<int,int> &indexMapping);
    
    std::map<int,std::vector<std::vector<double>>> ResetRepresentPositions(std::map<int,std::vector<std::vector<double>>> &represTraj);
    std::map<int,std::vector<std::vector<double>>> ClusterTrajectories(float (*minMaxCurrentFilters)[6][2],int epsilon=30,int minLines=5,int scale=10,unsigned int flags=0,unsigned int modeFlags=0);
    void SetupClusterData();
    void SetupClearClusterData();
    bool isClustered = false;
    bool canClusterAgain = true;
    static bool renderRepresent;
    int numberOfClusters = 0;
    static int scaleFactor;
    
    void CreateRTree();
    
    void Render();
    void initializeShader();
    void SetupData();
};

#endif /* TrajectoryManager_h */
