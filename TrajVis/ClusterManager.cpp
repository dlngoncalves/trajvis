//
//  ClusterManager.cpp
//  TrajVis
//
//  Created by Diego Gonçalves on 05/01/21.
//  Copyright © 2021 Diego Gonçalves. All rights reserved.
//

#include "ClusterManager.hpp"
#include "Map.hpp"
#include "TrajParser.h"

int ClusterManager::clusterID = 0;

void ClusterManager::SetupClusterData()
{
    std::vector<glm::vec3> positions;
    std::vector<int> clusterID;
    
    for(auto &curCluster : representativeTrajectories){
        std::transform(curCluster.second.begin(), curCluster.second.end(), std::back_inserter(positions),
                       [](std::vector<double> &curPoint)
                       {
                           return glm::vec3(curPoint[0],0.0,curPoint[1]);
                       });
        //it would make sense for clusterID to be an uniform, but since we are using one big buffer for all trajectories we can't.
        clusterID.insert(clusterID.end(), curCluster.second.size(),curCluster.first);
    }
    
    pointCount = (int)positions.size();
    
    if(!glIsBuffer(this->vertexBufferObject))
        glGenBuffers(1, &this->vertexBufferObject);
    
    glBindBuffer(GL_ARRAY_BUFFER, this->vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(glm::vec3), NULL, GL_DYNAMIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(glm::vec3), (float *)glm::value_ptr(positions.front()), GL_DYNAMIC_DRAW);

    if(!glIsBuffer(this->clusterBufferObject))
        glGenBuffers(1, &this->clusterBufferObject);
    
    glBindBuffer(GL_ARRAY_BUFFER, this->clusterBufferObject);
    glBufferData(GL_ARRAY_BUFFER, clusterID.size() * sizeof(int), NULL, GL_DYNAMIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, clusterID.size() * sizeof(int), clusterID.data(), GL_DYNAMIC_DRAW);

    if(!glIsVertexArray(vertexArrayObject))
        glGenVertexArrays(1, &vertexArrayObject);

    glBindVertexArray(vertexArrayObject);
    
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0 , 0);
    
    glBindBuffer(GL_ARRAY_BUFFER,clusterBufferObject);
    glVertexAttribIPointer(1,1,GL_INT,0,0);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
}

void ClusterManager::RemoveBasePosition(glm::vec3 refPoint,float scale)
{
    for(auto &curCluster : representativeTrajectories){
        std::transform(curCluster.second.begin(),curCluster.second.end(),curCluster.second.begin(),
                       [&](std::vector<double> &curPosition)
                       {
                           double x = curPosition[0];
                           double y = curPosition[1];
                           x /= scale;
                           y /= scale;
                           x += refPoint.x;
                           y += refPoint.z;
                           return std::vector<double>{x,y};
                       });
    }
}

void ClusterManager::AddBasePosition(glm::vec3 refPoint,float scale)
{
    for(auto &curCluster : representativeTrajectories){
        std::transform(curCluster.second.begin(),curCluster.second.end(),curCluster.second.begin(),
                       [&](std::vector<double> &curPosition)
                       {
                           double x = curPosition[0];
                           double y = curPosition[1];
                           x -= refPoint.x;
                           y -= refPoint.z;
                           x *= scale;
                           y *= scale;
                           return std::vector<double>{x,y};
                       });
    }
}

//Empty definitions of inherited virtual members
void ClusterManager::initializeShader()
{
    
}

void ClusterManager::Render()
{
    
}
