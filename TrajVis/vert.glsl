#version 410
const float pi = 3.14159;

layout (location = 0) in vec3 vertex_position;
layout (location = 1) in vec3 vertex_temp;
layout (location = 2) in vec2 speed; //x is imediate,y is average
layout (location = 3) in float temperature;
layout (location = 4) in vec2 date_time;//x is date, y is time
layout (location = 5) in int transport;
layout (location = 6) in float cluster;

uniform mat4 projection_mat, view_mat, model_mat;

//uniform float temperature;
uniform vec3 minMaxCurrent;//packs the minimun of the range, the maximum and the current on a vec3 - current now is temp

uniform vec3 minColor; //in modes where we can lerp between two colors, these are the values
uniform vec3 maxColor; 

uniform int currentSelection;
uniform int selection;

uniform int numberOfClusters;

out vec4 screenPosition;
out vec3 position_eye;
//out vec3 vertColorTemp;
out vec3 vertColorTemp_g;

out vec3 minMaxCurrent_g;

out float cluster_g;
out vec2 speed_g;
out float temperature_g;
out vec2 date_time_g;
out int transport_g;

//, normal_eye; //not using normals for anything now

//should create a color uniform to use if the color of the entire trajectory is constant

//would make sense to pass all the base data - such as temperature - to the vertex shader, with additional uniforms for color and temp range
//then we perform the mapping here instead

vec3 getColor()
{
    //adapted from https://stackoverflow.com/questions/20792445/calculate-rgb-value-for-a-range-of-values-to-create-heat-map
    
    //float ratio = 2 * (temperature - minMaxCurrent.x) / (minMaxCurrent.y - minMaxCurrent.x);
    float ratio = 2 * (temperature - minMaxCurrent.x) / (minMaxCurrent.y - minMaxCurrent.x);
    
    float b = 255*(1 - ratio);
    float r = 255*(ratio - 1);
    
    b = (b > 0) ? b : 0;
    r = (r > 0) ? r : 0;
    
    float g = 255 - b - r;
    
    r = r / 255;
    g = g / 255;
    b = b / 255;
    
    return vec3(r,g,b);
}

vec3 getClusterColor()
{
//    if(cluster == -3){
//        return vec3(0.0,0.0,0.0);
//    }
//    if(cluster == -2){
//        return vec3(0.0,0.0,0.0);
//    }
//    else{
            //float intensity = cluster/255;
            //vertColorTemp_g = vec3(1.0,0.0,0.0);
        //was using as int
        ///float clusterFloat = float(cluster);
        float numberOfClustersFloat = float(numberOfClusters);
    
        float ratio = 2 * (cluster - 0) / (numberOfClustersFloat - 0);
        
        float b = 255*(1 - ratio);
        float r = 255*(ratio - 1);
        
        b = (b > 0) ? b : 0;
        r = (r > 0) ? r : 0;
        
        float g = 255 - b - r;
        
        r = r / 255;
        g = g / 255;
        b = b / 255;
        
        return vec3(r,g,b);
    //}
}

void main () {
	
    
    //probably wont need this also?
    //position_eye = vec3 (view_mat * model_mat * vertex_position);
    //normal_eye = vec3 (view_mat * model_mat * vec4 (vertex_normal, 0.0));
    
    //for now just using the color as a intensity of red
    //vertColorTemp = vec3(vertex_temp.r,vertex_temp.g,vertex_temp.b);
    //vertColorTemp = vec3(1.0,1.0,0.0);
    
//    vertColorTemp = vertex_temp;
    
    //vertColorTemp_g = vertex_temp;
    
    //TODO - switch maybe?
    
    if(currentSelection == 0)
        vertColorTemp_g = getColor();
    if(currentSelection == 1){
        
        //float intensity = clamp(mix(minMaxCurrent.x/minMaxCurrent.y,minMaxCurrent.y/minMaxCurrent.y,averageSpeed/minMaxCurrent.y),0.0,1.0);
        float intensity = smoothstep(minMaxCurrent.x,minMaxCurrent.y,speed.y);
        vec3 intensityColor = vec3(intensity,intensity,intensity);
        //vertColorTemp_g = vec3(intensity,0.0,0.0);
        vertColorTemp_g = mix(minColor,maxColor,intensityColor);
    }
    if(currentSelection == 2){
        float timePercentage = smoothstep(minMaxCurrent.x,minMaxCurrent.y,date_time.y);
        vec3 intensityColor = vec3(timePercentage,timePercentage,timePercentage);
        vertColorTemp_g = mix(minColor,maxColor,intensityColor);
    }
    if(currentSelection == 4){
        if(cluster < 0){//some of those colors like white and black are not displaying properly?
            switch (int(cluster)) {//could use a mix or smoothstep here?
                case -1:
                    vertColorTemp_g = vec3(0.9,0.9,0.9);
                    break;
                case -2:
                    vertColorTemp_g = vec3(0.25,0.25,0.25);
                    break;
                case -3:
                    vertColorTemp_g = vec3(0.5,0.5,0.5);
                    break;
                case -4:
                    vertColorTemp_g = vec3(0.75,0.75,0.75);
                    break;
                case -5:
                    vertColorTemp_g = vec3(0.1,0.1,0.1);
                    break;
                default:
                    break;
            }
            
            
//        }else if(cluster >= 0 && cluster <= 16){
//            vertColorTemp_g = vec3(1.0,0.0,0.0); //getClusterColor();
//        }else if(cluster > 16 && cluster <= 32){
//            vertColorTemp_g = vec3(0.0,1.0,0.0);
//        }else if(cluster > 32 && cluster <= 48){
//            vertColorTemp_g = vec3(0.0,0.0,1.0);
//        }else if(cluster > 48 && cluster <= 64){
//            vertColorTemp_g = vec3(1.0,1.0,0.0);
//        }else if(cluster > 64 && cluster <= 80){
//            vertColorTemp_g = vec3(1.0,0.0,1.0);
//        }else if(cluster > 80 && cluster <= 96){
//            vertColorTemp_g = vec3(0.0,1.0,1.0);
//        }
    
        }else{
            //float clusterFloat = float(cluster);
            //float numberOfClustersFloat = float(numberOfClusters);
            vertColorTemp_g = getClusterColor();
//            float intensity = smoothstep(minMaxCurrent.x,minMaxCurrent.y,cluster);
//            vec3 intensityColor = vec3(intensity,intensity,intensity);
//            vertColorTemp_g = mix(minColor,maxColor,intensityColor);
        }
        

    }
    
    if(currentSelection == 3){//TODO - find better way to map color to transport mode ( a discrete fixed amout of types)
        float transportPercentage = smoothstep(minMaxCurrent.x,minMaxCurrent.y,transport);
        vec3 intensityColor = vec3(transportPercentage,transportPercentage,transportPercentage);
        vertColorTemp_g = mix(minColor,maxColor,intensityColor);
    }
    
    minMaxCurrent_g = minMaxCurrent;
    
    speed_g = speed;
    temperature_g = temperature;
    date_time_g = date_time;
    cluster_g = cluster;
    transport_g = transport;
    
    screenPosition = projection_mat * view_mat * model_mat * vec4(vertex_position,1.0);
//    gl_Position = projection_mat * view_mat * model_mat * vec4(vertex_position,1.0);
    gl_Position = vec4(vertex_position,1.0);
}
