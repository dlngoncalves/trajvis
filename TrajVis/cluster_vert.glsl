#version 410

layout (location = 0) in vec3 vertex_position;
layout (location = 1) in int cluster;

uniform mat4 projection_mat, view_mat, model_mat;
uniform int numberOfClusters;

out vec4 screenPosition;
out vec3 vertColor_g;
out int cluster_g;

//TODO - These functions are kinda ugly and not generic/performant enough. Fix it
vec3 GetColor(int curCluster,int numClusters)
{
    float numberOfClustersFloat = float(numClusters);
    float curClusterFloat = float(curCluster);
    float ratio = 2 * (curClusterFloat - 0) / (numberOfClustersFloat - 0);
    
    float b = 255*(1 - ratio);
    float r = 255*(ratio - 1);
    
    b = (b > 0) ? b : 0;
    r = (r > 0) ? r : 0;
    
    float g = 255 - b - r;
    
    r = r / 255;
    g = g / 255;
    b = b / 255;
    
    return vec3(r,g,b);
}

vec3 GetClusterColor(int curCluster,int numClusters)
{
    //is this part even needed?
    if(curCluster < 0){//some of those colors like white and black are not displaying properly?
        switch (int(curCluster)) {//could use a mix or smoothstep here?
            case -1:
                return vec3(0.9,0.9,0.9);
            case -2:
                return vec3(0.25,0.25,0.25);
            case -3:
                return vec3(0.5,0.5,0.5);
            case -4:
                return vec3(0.75,0.75,0.75);
            case -5:
                return vec3(0.1,0.1,0.1);
            default:
                break;
        }
    }else{
        return GetColor(curCluster,numClusters);
    }
}

void main () {
    
    //TODO - need more ways of mapping color to cluster trajs.
    vertColor_g = GetClusterColor(cluster,numberOfClusters);
    cluster_g = cluster;
    
    screenPosition = projection_mat * view_mat * model_mat * vec4(vertex_position,1.0);
    gl_Position = vec4(vertex_position,1.0);
}
