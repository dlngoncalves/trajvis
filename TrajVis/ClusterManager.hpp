//
//  ClusterManager.hpp
//  TrajVis
//
//  Created by Diego Gonçalves on 05/01/21.
//  Copyright © 2021 Diego Gonçalves. All rights reserved.
//

#ifndef ClusterManager_hpp
#define ClusterManager_hpp

#include <stdio.h>
#include <vector>
#include <utility>
#include "Renderable.h"
#include <glm/gtc/type_ptr.hpp>

struct Cluster
{
    int clusterID;
    std::vector<std::vector<double>> representativeTrajectory;
};

class ClusterManager: public Renderable {
    
    
public:

    std::map<int,std::vector<std::vector<double>>> representativeTrajectories;
    int pointCount;
    static int clusterID;
    void CreateClusters();
    void SetupClusterData();
    void ClearClusters();
    void ResetClusterData();
    void RemoveBasePosition(glm::vec3 refPoint,float scale);
    void AddBasePosition(glm::vec3 refPoint,float scale);
    //TODO - added these but we are not using them, if this continues to be the case delete it. 
    void ResetClusterScale(double lat,double lon,std::map<int,std::vector<std::vector<double>>> * representativeTrajectories);
    void ResetClusterPositions(double lat,double lon,std::map<int,std::vector<std::vector<double>>> * representativeTrajectories);
    std::vector<double> ReZoomPosition(glm::vec3 position, glm::vec3 refPoint, float scale);
    
    void Render();
    void initializeShader();

    ClusterManager(){};//empty constructor
    ClusterManager(const std::map<int,std::vector<std::vector<double>>> &_clusterList) : representativeTrajectories(std::move(_clusterList)){};
    ClusterManager(std::map<int,std::vector<std::vector<double>>> &&_clusterList) : representativeTrajectories(_clusterList){};

    ~ClusterManager(){};//empty destructor
};
#endif /* ClusterManager_hpp */
